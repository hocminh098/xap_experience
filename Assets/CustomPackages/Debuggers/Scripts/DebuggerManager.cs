using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebuggerManager : MonoBehaviour
{
    public GameObject debuggerPanel;
    // Start is called before the first frame update
    void Start()
    {
        InitEvents();
    }

    /// <summary>
    /// Author: sonvdh
    /// Purpose: Add event triggers for UI
    /// </summary>
    void InitEvents()
    {
        this.GetComponent<Button>().onClick.AddListener(ToggleDebugMode);
    }

    /// <summary>
    /// Author: sonvdh
    /// Purpose: Toggle debug mode
    /// </summary>
    void ToggleDebugMode()
    {
        debuggerPanel.SetActive(!debuggerPanel.activeSelf);
    }
}
