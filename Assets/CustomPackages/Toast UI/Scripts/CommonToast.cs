using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using EasyUI.Toast;

/* -------------------------------
   Created by : Hamza Herbou
   hamza95herbou@gmail.com
---------------------------------- */

namespace EasyUI.Helpers
{

    public class CommonToast : MonoBehaviour
    {
        [Header("UI References :")]

        public Sprite successSprite;
        public Sprite errorSprite;
        public Sprite warningSprite;
        private string successBackgroundColor = "#00884F";
        private string errorBackgroundColor = "#C7002B";
        private string warningBackgroundColor = "#C65312";
        // private Dictionary<int , string> notific
        private string sucessTitle = "Successfully!";
        private string errorTitle = "Error!";
        private string warnignTitle = "Warning!";
        private float defaultDuration = 4f;
        public Image backgroundImage;
        public Image notificationTypeImage;
        public Text titleText;
        public Text contentText;
        public Button closeBtn;
        private Coroutine showNotificationPanel;
        [SerializeField] private CanvasGroup uiCanvasGroup;
        [SerializeField] private RectTransform uiRectTransform;
        [SerializeField] private HorizontalLayoutGroup uiContentHorizontalLayoutGroup;
        [SerializeField] private VerticalLayoutGroup uiContentVerticalLayoutGroup;

        [Header("Toast Fade In/Out Duration :")]
        [Range(.1f, .8f)]
        [SerializeField] private float fadeDuration = .3f;
        private Touch touch;

        void Awake()
        {
            uiCanvasGroup.alpha = 0f;
        }

        public void CloseNotification()
        {
            StopCoroutine(showNotificationPanel);
            uiCanvasGroup.alpha = 0f;
        }

        public void Init(string content, int code)
        {
            contentText.text = content;
            string title = errorTitle;
            string background = errorBackgroundColor;
            Sprite sprite = errorSprite;

            if (code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
            {
                title = sucessTitle;
                background = successBackgroundColor;
                sprite = successSprite;
            }
            else if (code == APIUrlConfig.SYSTEM_ERROR_CODE)
            {
                title = warnignTitle;
                background = warningBackgroundColor;
                sprite = warningSprite;
            }
            titleText.text = title;
            Color color;
            if (ColorUtility.TryParseHtmlString(background, out color))
            {
                backgroundImage.color = color;
            }
            notificationTypeImage.sprite = sprite;
            ShowCommonToast();
        }

        private void ShowCommonToast()
        {
            Dismiss();
            showNotificationPanel = StartCoroutine(FadeInOut(defaultDuration, fadeDuration));
        }

        private IEnumerator FadeInOut(float toastDuration, float fadeDuration)
        {
            yield return null;
            Helper.RefreshLayoutGroupsImmediateAndRecursive(gameObject);
            yield return new WaitForFixedUpdate();
            // Anim start
            yield return Fade(uiCanvasGroup, 0f, 1f, fadeDuration);
            yield return new WaitForSeconds(toastDuration);
            yield return Fade(uiCanvasGroup, 1f, 0f, fadeDuration);
            // Anim end
        }

        private IEnumerator Fade(CanvasGroup cGroup, float startAlpha, float endAlpha, float fadeDuration)
        {
            float startTime = Time.time;
            float alpha = startAlpha;

            if (fadeDuration > 0f)
            {
                //Anim start
                while (alpha != endAlpha)
                {
                    alpha = Mathf.Lerp(startAlpha, endAlpha, (Time.time - startTime) / fadeDuration);
                    cGroup.alpha = alpha;

                    yield return null;
                }
            }

            cGroup.alpha = endAlpha;
        }

        public void Dismiss()
        {
            StopAllCoroutines();
            uiCanvasGroup.alpha = 0f;
        }

        private void OnDestroy()
        {
            EasyUI.Toast.Toast.isLoaded = false;
        }
    }
}
