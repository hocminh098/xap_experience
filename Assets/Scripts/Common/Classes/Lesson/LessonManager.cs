using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public static class LessonManager
{
    public static int lessonId;
    public static void InitLesson(int _lessonId)
    { 
        lessonId = _lessonId;
    }
}
