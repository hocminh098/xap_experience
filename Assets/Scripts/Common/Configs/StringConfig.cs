using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StringConfig
{
    public static string TAG_CAMERA_MAIN = "MainCamera";
    public static string DOT_SHORT_STRING = "...";
    public static string audioPreText = "Audio ";
    public static string videoPreText = "Video ";
    public static string levelParentObjectAppend = "0-";
    public static string letterAppend = "-";
    public static string levelParentObject = "0";
    public static string message_error_enterlabel = "Please fill in required fields!";
    public static string labelId = "labelId";
    public static string labelName = "labelName";
}
