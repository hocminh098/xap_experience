using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExperienceConfig
{
    public static Vector3 ScaleOriginLabel = new Vector3(0.009f, 0.009f, 0.009f);
    public static Vector3 ScaleOriginPointLabelMediumObject = new Vector3(0.004f, 0.004f, 0.004f);
    public static Vector3 ScaleOriginPointLabelBigObject = new Vector3(0.0075f, 0.0075f, 0.0075f);
    public static Vector3 ScaleOriginPointLabelModeAR = new Vector3(0.0018f, 0.0018f, 0.0019f);
    public static Vector3 ScaleOriginPointLabelMin = new Vector3(0.0045f, 0.0045f, 0.0045f);
    public static Vector3 ScaleOriginLabelMin = new Vector3(0.003f, 0.003f, 0.003f);
    public static Vector3 ScaleOriginLabelMax = new Vector3(0.2f, 0.2f, 0.2f);

}
