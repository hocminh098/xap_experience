using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeDetectorHome : MonoBehaviour
{
    private Vector2 fingerDown;
    private Vector2 fingerUp;
    public bool detectSwipeOnlyAfterRelease = false;
    public float SWIPE_THRESHOLD = 20f;
    [SerializeField]
    Animator statusAnimator;

    void Update()
    {
        // Debug.Log("Swipe: ");
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                fingerUp = touch.position;
                fingerDown = touch.position;
            }

            // Detects swipe after finger is released
            if (touch.phase == TouchPhase.Ended)
            {
                fingerDown = touch.position;
                checkSwipe();
            }
        }
    }

    void checkSwipe()
    {
        // Check if Vertical swipe
        if (verticalMove() > SWIPE_THRESHOLD && verticalMove() > horizontalValMove())
        {
            // Debug.Log("Vertical");
            if (fingerDown.y - fingerUp.y > 0) // up swipe
            {
                OnSwipeUp();
            }
            else if (fingerDown.y - fingerUp.y < 0) // Down swipe
            {
                OnSwipeDown();
            }
            fingerUp = fingerDown;
        }

        // Check if Horizontal swipe
        else if (horizontalValMove() > SWIPE_THRESHOLD && horizontalValMove() > verticalMove())
        {
            //Debug.Log("Horizontal");
            if (fingerDown.x - fingerUp.x > 0 && fingerUp.x <= 60f) // Right swipe
            {
                Debug.Log("Swipe fingerUp.x " + fingerUp.x);
                Debug.Log("Swipe fingerDown.x " + fingerDown.x);
                OnSwipeRight();
            }
            else if (fingerDown.x - fingerUp.x < 0) // Left swipe
            {
                Debug.Log("Swipe fingerDown.x " + fingerDown.x);
                OnSwipeLeft();
            }
            fingerUp = fingerDown;
        }
        else
        {
            // Debug.Log("No Swipe!");
        }
    }

    float verticalMove()
    {
        return Mathf.Abs(fingerDown.y - fingerUp.y);
    }

    float horizontalValMove()
    {
        return Mathf.Abs(fingerDown.x - fingerUp.x);
    }

    void OnSwipeUp()
    {
        Debug.Log("Swipe UP");
    }

    void OnSwipeDown()
    {
        Debug.Log("Swipe Down");
    }

    void OnSwipeLeft()
    {
        Debug.Log("Swipe Left");
        statusAnimator.SetBool(AnimatorConfig.showLeftPanel, false);
    }

    void OnSwipeRight()
    {
        Debug.Log("Swipe Right");
        // Enable the pannel
        statusAnimator.SetBool(AnimatorConfig.showLeftPanel, true);
    }
}
