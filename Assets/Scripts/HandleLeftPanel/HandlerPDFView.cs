using System.Net.Mime;
using System.Security;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.XR.ARFoundation;
using UnityEngine.Networking;
using Newtonsoft.Json;
using System.Net;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TriLibCore;
using System.Linq;
using System.IO;
using Photon.Pun;
using System.Threading.Tasks;
using EasyUI.Toast;
using System.Net.Http;
using System.Threading;
using TriLibCore.Utils;
using TriLibCore.Mappers;
using TriLibCore.General;

public class HandlerPDFView : MonoBehaviour
{
    public Button btnDownload;
    public GameObject panelDownload;
    public Button btnExit;
    public Button btnBack;
    
    void Start()
    {
        InitEvent();
        InitScreen();
    }

    public void InitScreen()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        StatusBarManager.statusBarState = StatusBarManager.States.TranslucentOverContent;
        StatusBarManager.navigationBarState = StatusBarManager.States.Hidden;
    }

    public void InitEvent()
    {
        btnDownload.onClick.AddListener(DownloadDocument);
        btnBack.onClick.AddListener(BackToPreviousScene);
        btnExit.onClick.AddListener(HiddenPanelDownload);
    }

    public async void DownloadDocument()
    {
        panelDownload.SetActive(true);
    }

    public void BackToPreviousScene()
    {
        StopAllCoroutines();
        BackOrLeaveApp.Instance.BackToPreviousScene(SceneManager.GetActiveScene().name);
    }

    public void HiddenPanelDownload()
    {
        panelDownload.SetActive(false);
    }
}
