using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement; 

public class OpenUserGuide : MonoBehaviour
{
    public Button btnUserGuide;
    
    void Start()
    {
        btnUserGuide.onClick.AddListener(HandleUserGuide);
    }

    void Update()
    {
        
    }

    public void HandleUserGuide()
    {
        BackOrLeaveApp.Instance.AddPreviousScene(SceneManager.GetActiveScene().name, SceneConfig.pdfViewer);
        SceneManager.LoadScene(SceneConfig.pdfViewer);
    }
}
