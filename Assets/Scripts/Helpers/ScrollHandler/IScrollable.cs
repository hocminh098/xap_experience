public interface IScrollable
{
    bool Dragging { get; }
}