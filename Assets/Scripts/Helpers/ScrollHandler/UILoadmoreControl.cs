using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UILoadmoreControl : MonoBehaviour
{
    [Serializable] public class LoadmoreControlEvent : UnityEvent {}

    [SerializeField] private CustomScrollRect m_ScrollRect;
    private float m_SwipeUpDistanceRequiredLoadingmore = 0;
    [SerializeField] LoadmoreControlEvent m_OnLoadmore = new LoadmoreControlEvent();

    private bool m_IsLoadingmore = false;
    private bool m_IsSwipeUp = false;
    private IScrollable m_ScrollView;

    /// <summary>
    /// Refreshing?
    /// </summary>
    public bool IsRefreshing
    {
        get { return m_IsLoadingmore; }
    }

    /// <summary>
    /// Callback executed when refresh started.
    /// </summary>
    public LoadmoreControlEvent OnLoadmore
    {
        get { return m_OnLoadmore; }
        set { m_OnLoadmore = value; }
    }

        /// <summary>
    /// Call When Refresh is End.
    /// </summary>
    public void EndLoadingmore()
    {
        m_IsLoadingmore = false;
        m_IsSwipeUp = false;
    }

    private void Start()
    {
        m_ScrollView = m_ScrollRect.GetComponent<IScrollable>();
        m_ScrollRect.onValueChanged.AddListener(OnScroll);
    }

    private void OnScroll(Vector2 normalizedPosition)
    {
        if (m_IsLoadingmore) return;
        if (m_IsSwipeUp && m_ScrollView.Dragging) return;
        if (m_ScrollView.Dragging)
        {
            m_IsSwipeUp = true;
        }
        if (!m_ScrollView.Dragging && m_IsSwipeUp && m_ScrollRect.verticalNormalizedPosition < m_SwipeUpDistanceRequiredLoadingmore)
        {
            m_IsLoadingmore = true;
            m_OnLoadmore.Invoke();
        }
    }
}
