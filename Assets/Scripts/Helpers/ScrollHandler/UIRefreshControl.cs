﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIRefreshControl : MonoBehaviour
{
    [Serializable] public class RefreshControlEvent : UnityEvent {}
    [SerializeField] private CustomScrollRect m_ScrollRect;
    private float m_PullDistanceRequiredRefresh = 200f;
    [SerializeField] RefreshControlEvent m_OnRefresh = new RefreshControlEvent();
    private string releaseMessage = "Release to refresh ...";
    private string pullMessage = "Pull down to refresh ...";
    private float m_InitialPosition;
    private float m_Progress;
    private bool m_IsPulled;
    private bool m_IsRefreshing;
    private Vector2 m_PositionStop;
    private IScrollable m_ScrollView;
    public Text refreshText;

    /// <summary>
    /// Progress until refreshing begins. (0-1)
    /// </summary>
    public float Progress
    {
        get { return m_Progress; }
    }

    /// <summary>
    /// Refreshing?
    /// </summary>
    public bool IsRefreshing
    {
        get { return m_IsRefreshing; }
    }

    /// <summary>
    /// Callback executed when refresh started.
    /// </summary>
    public RefreshControlEvent OnRefresh
    {
        get { return m_OnRefresh; }
        set { m_OnRefresh = value; }
    }

        /// <summary>
    /// Call When Refresh is End.
    /// </summary>
    public void EndRefreshing()
    {
        m_IsPulled = false;
        m_IsRefreshing = false;
        SetActiveRefreshText(true);
    }

    private void Start()
    {
        m_InitialPosition = GetContentAnchoredPosition();
        m_PositionStop = new Vector2(m_ScrollRect.content.anchoredPosition.x, m_InitialPosition - m_PullDistanceRequiredRefresh);
        m_ScrollView = m_ScrollRect.GetComponent<IScrollable>();
        m_ScrollRect.onValueChanged.AddListener(OnScroll);
    }

    private void LateUpdate()
    {
        if (!m_IsPulled)
        {
            return;
        }

        if (!m_IsRefreshing)
        {
            return;
        }

        m_ScrollRect.content.anchoredPosition = m_PositionStop;
    }

    private void OnScroll(Vector2 normalizedPosition)
    {
        var distance = m_InitialPosition - GetContentAnchoredPosition();

        if (distance < 0f)
        {
            SetRefreshText(pullMessage);
            return;
        }

        OnPull(distance);
    }

    private void OnPull(float distance)
    {
        if (m_IsRefreshing && Math.Abs(distance) < 1f)
        {
            m_IsRefreshing = false;
        }
        m_Progress = distance / m_PullDistanceRequiredRefresh;

        if (m_IsPulled && m_ScrollView.Dragging)
        {
            if (m_Progress < 1)
            {
                m_IsPulled = false;
                SetRefreshText(pullMessage);
            }
            return;
        }

        if (m_Progress < 1f)
        {
            m_IsPulled = false;
            SetRefreshText(pullMessage);
            return;
        }

        // Start animation when you reach the required distance while dragging.
        if (m_ScrollView.Dragging)
        {
            m_IsPulled = true;
            SetRefreshText(releaseMessage);
        }

        if (m_IsPulled && !m_ScrollView.Dragging)
        {
            m_IsRefreshing = true;
            m_ScrollRect.verticalNormalizedPosition = 1f;
            // m_ScrollRect.content.anchoredPosition = Vector2.zero;
            OnRefresh.Invoke();
            SetActiveRefreshText(false);
        }
        
        m_Progress = 0f;
    }

    private float GetContentAnchoredPosition()
    {
        return m_ScrollRect.content.anchoredPosition.y;
    }

    void SetRefreshText(string text)
    {
        if (refreshText != null)
        {
            refreshText.text = text;
        }
    }

    void SetActiveRefreshText(bool isActive)
    {
        if (refreshText != null)
        {
            refreshText.transform.parent.gameObject.SetActive(isActive);
        }
    }
}
