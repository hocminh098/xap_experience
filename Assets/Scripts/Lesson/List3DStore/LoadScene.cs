using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using System; 
using System.Text.RegularExpressions;
using UnityEngine.EventSystems;
using UnityEngine.Networking; 
using System.Threading.Tasks;
using System.Linq; 
using EasyUI.Toast;

namespace List3DStore
{
    public class LoadScene : MonoBehaviour
    {
        private int timeDelayForSearch = 400;
        private char[] charsToTrim = { '*', '.', ' '};
        public int calculatedSize = 26; 
        private string searchValueString; 
        private int offset = 0;
        private int limit = 12;
        public GameObject modelPanelObject;
        public GameObject modelObjectResource;
        public InputField searchInputField; 
        public GameObject resetSearchBoxBtn; 
        public CustomScrollRect modelPanelScrollRect;
        public GameObject loadingItem;
        private int type = -1;  // Initial value
        public GameObject noDataComponent;
        [SerializeField] private UIRefreshControl m_UIRefreshControl;
        [SerializeField] private UILoadmoreControl m_UILoadmoreControl;
        private bool isLoadmoreActive = true;

        void OnEnable()
        {
            SearchOnInit();
            resetSearchBoxBtn.transform.GetComponent<Button>().onClick.AddListener(ResetSearchBox); 
            searchInputField.onValueChanged.AddListener(SearchModels); 
            m_UIRefreshControl.OnRefresh.AddListener(RefreshData);
            m_UILoadmoreControl.OnLoadmore.AddListener(LoadmoreData);
        }

        void OnDisable()
        {
            resetSearchBoxBtn.transform.GetComponent<Button>().onClick.RemoveListener(ResetSearchBox); 
            searchInputField.onValueChanged.RemoveListener(SearchModels); 
            m_UIRefreshControl.OnRefresh.RemoveListener(RefreshData);
            m_UILoadmoreControl.OnLoadmore.RemoveListener(LoadmoreData);
            DestroyAllModels();
        }

        void Start()
        {
            InitScreen();
        }

        async void RefreshData()
        {
            await UpdateModelsData();
            m_UIRefreshControl.EndRefreshing();
        }

        async void LoadmoreData()
        {
            await LoadMoreModels();
            m_UILoadmoreControl.EndLoadingmore();
        }

        void SearchOnInit()
        {
            type = ModelTypeManager.Instance.GetCurrentModeType();
            string searchText = Regex.Replace(searchInputField.text, @"\s+", " ").ToLower().Trim(charsToTrim); 
            if (string.IsNullOrEmpty(searchText))
            {
                searchText = null; // make "" = null;
            }
            if (searchText != searchValueString)
            {
                searchValueString = searchText;
            }
            UpdateModelsData();
        }
        void InitScreen()
        {
            Screen.orientation = ScreenOrientation.Portrait; 
            StatusBarManager.statusBarState = StatusBarManager.States.TranslucentOverContent;
            StatusBarManager.navigationBarState = StatusBarManager.States.Hidden;
            ScenePrevious.scenePrevious = SceneConfig.storeModel;
        }
        void ResetSearchBox()
        {
            searchInputField.text = "";
            searchInputField.Select();
        }

        async void SearchModels(string value)
        {
            searchValueString = Regex.Replace(value, @"\s+", " ").ToLower().Trim(charsToTrim); 
            resetSearchBoxBtn.SetActive(!string.IsNullOrEmpty(searchValueString)); 
            if (string.IsNullOrEmpty(searchValueString))
            {
                await UpdateModelsData();
            }
            else
            {
                await CheckForSearchingModels(value);
            }
        }

        async Task CheckForSearchingModels(string value)
        {
            await Task.Delay(timeDelayForSearch);
            if (value == searchValueString)
            {
                await UpdateModelsData();
            }
        }

        async Task UpdateModelsData()
        {
            isLoadmoreActive = true;
            offset = 0;
            await UpdateModelPanel(true);
        }

        async Task LoadMoreModels()
        {
            offset++;
            await UpdateModelPanel(false);
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: UpdateLessonPanel and call API GET_MODEL_LIST
        /// </summary>
        async Task UpdateModelPanel(bool isRenewModelPanel)
        {
            try
            {
                if (!isRenewModelPanel && !isLoadmoreActive)
                {
                    return;
                }
                if (string.IsNullOrEmpty(searchValueString))
                {
                    searchValueString = null;
                }
                if (isRenewModelPanel)
                {
                    DestroyAllModels();
                }
                ResetLoading(true);
                noDataComponent.SetActive(false);

                string URL = String.Format(APIUrlConfig.GET_MODEL_LIST, searchValueString, offset, limit, type);
                APIResponse<List<List3DModel>> modelResponse = await UnityHttpClient.CallAPI<List<List3DModel>>(URL, UnityWebRequest.kHttpVerbGET);
                noDataComponent.SetActive(false); 
                ResetLoading(false);             

                if (isRenewModelPanel)
                {
                    DestroyAllModels();
                }

                if (modelResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
                {
                    offset = modelResponse.meta.page;
                    isLoadmoreActive = (modelResponse.meta.totalPage > modelResponse.meta.page + 1);
                    StartCoroutine(LoadModelsIntoUI(modelResponse.data, isRenewModelPanel));
                } 
            } 
            catch (Exception e)
            {
                Debug.Log(e.Message);
                ResetLoading(false);           
                noDataComponent.SetActive(false);
            }
        }

        void ResetLoading(bool isActive)
        {
            if (loadingItem != null)
            {
                loadingItem.SetActive(isActive);  
                loadingItem.transform.SetAsLastSibling();
            }
        }

        IEnumerator LoadModelsIntoUI(List<List3DModel> models, bool isRenewModelPanel)
        {
            if (isRenewModelPanel)
            {
                DestroyAllModels();
            }
            if (models.Count < 1)
            {
                if (isRenewModelPanel)
                {
                    noDataComponent.SetActive(true);   
                }
                yield break;
            }
            noDataComponent.SetActive(false);   
            foreach (List3DModel model in models)
            {
                InstantiateModel(model, modelPanelObject.transform);
            }
            ResetLoading(false);   
        }

        void DestroyAllModels()
        {
            foreach (Transform child in modelPanelObject.transform) 
            {
               if (child.gameObject.tag != TagConfig.loadingItemTag)
                {
                    GameObject.Destroy(child.gameObject);
                }
            }
        }

        void InstantiateModel(List3DModel model, Transform parentTransform)
        {
            try
            {
                GameObject modelObject = Instantiate(modelObjectResource) as GameObject; 
                modelObject.transform.SetParent(parentTransform, false);

                string imageURL = APIUrlConfig.DOMAIN_SERVER + model.modelThumbnail;
                RawImage targetImage = modelObject.transform.GetChild(0).GetChild(0).gameObject.GetComponent<RawImage>();
                StartCoroutine(UnityHttpClient.LoadRawImageAsync(imageURL, targetImage, (isSuccess) => {
                    if (isSuccess)
                    {
                        modelObject.transform.GetChild(0).GetChild(1).gameObject.SetActive(false);
                    }
                }));
                modelObject.name = model.modelId.ToString(); 
                modelObject.transform.GetChild(1).gameObject.GetComponent<Text>().text = Helper.FormatString(model.modelName, calculatedSize);
                modelObject.transform.GetChild(0).gameObject.GetComponent<Button>().onClick.AddListener(() => InteractionUI.Instance.onClickItemModel(model.modelId, model.modelName));        
            }
            catch (Exception e)
            {
                Debug.Log($"error InstantiateModel");
            }
        }
    }
}
