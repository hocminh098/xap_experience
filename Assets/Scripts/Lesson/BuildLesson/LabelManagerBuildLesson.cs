using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;
using System.Text;
using UnityEngine.UI;
using EasyUI.Toast;
using System.Threading.Tasks;

namespace BuildLesson 
{
    public class LabelManagerBuildLesson : MonoBehaviour
    {
        private static LabelManagerBuildLesson instance; 
        public static LabelManagerBuildLesson Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<LabelManagerBuildLesson>(); 
                }
                return instance; 
            }
        }
        // private int calculatedSize = 10;
        public GameObject UIComponent;
        const float RADIUS = 17f;
        private string SEPARATE_LEVEL_SYMBOL = "-";
        public GameObject btnLabel;
        public GameObject labelUpdatePanel;
        private bool isShowingLabel = true;
        private bool isListLabel = true;
        private bool isAddingLabel = false;
        private bool isAddLabelError = false;
        public bool isTyping { get; set; } = false;
        public  GameObject labelObjectHidden { get; set;}
        public int indexShowLabel { get; set;} = 0;
        public GameObject addLabelIcon { get; set;} = null;
        public bool IsShowingLabel 
        {    
            get
            {
                return isShowingLabel;
            }
            set
            {
                Debug.Log("LabelManagerBuildLesson isShowingLabel call"); 
                isShowingLabel = value; 
                btnLabel.GetComponent<Image>().sprite = !isShowingLabel ? Resources.Load<Sprite>(PathConfig.LABEL_UNCLICK_IMAGE) : Resources.Load<Sprite>(PathConfig.LABEL_CLICKED_IMAGE);
            }
        }

        /// <summary>
        /// Author: quyennt57
        /// Purpose: Get hit point and display UI enter name label
        /// </summary>
        public void DisplayPanelAddLabel(Touch touch)
        {
            // Get infor hit point
            InforPointByHit inforPointByHit = Helper.GetObjectByTouch(touch.position);
            if (inforPointByHit == null)
                return;

            inforPointByHit.ObjectByHit = inforPointByHit.ObjectByHit != null ? inforPointByHit.ObjectByHit : ObjectManager.Instance.CurrentObject;
            inforPointByHit.hitPoint = inforPointByHit.ObjectByHit.transform.InverseTransformPoint(inforPointByHit.hitPoint);

            // Add UI inputfield to enter name label
            addLabelIcon = Instantiate(Resources.Load(PathConfig.MODEL_ADD_LABEL) as GameObject);
            addLabelIcon.transform.parent = UIComponent.transform;
            addLabelIcon.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
            addLabelIcon.transform.position = touch.position;
            addLabelIcon.transform.GetComponent<Button>().onClick.AddListener(() => DisplayInputFieldLabel(touch.position, addLabelIcon, inforPointByHit));
        }

        
        public void DisplayInputFieldLabel(Vector3 position, GameObject destroyObj, InforPointByHit inforPointByHit)
        {
            addLabelIcon = null;
            GameObject label2D = Instantiate(Resources.Load(PathConfig.MODEL_TAG_CONFIG) as GameObject);
            label2D.tag = "Tag2D";
            label2D.transform.SetParent(UIComponent.transform, false);
            label2D.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
            label2D.transform.position = position;
            label2D.transform.GetChild(0).gameObject.GetComponent<InputField>().onValueChanged.AddListener(ValidateCreatedLabel);
            label2D.transform.GetChild(0).gameObject.GetComponent<InputField>().ActivateInputField();
            label2D.transform.GetChild(0).gameObject.GetComponent<InputField>().Select();
            label2D.transform.GetChild(1).gameObject.GetComponent<Button>().interactable = false;
            label2D.transform.GetChild(1).gameObject.GetComponent<Button>().onClick.AddListener(() => SaveLabel(label2D, inforPointByHit));
            label2D.transform.GetChild(2).gameObject.GetComponent<Button>().onClick.AddListener(() => CancelSaveLabel(label2D));
            Destroy(destroyObj);  

            void ValidateCreatedLabel(string data)
            {
                IsValidCreatedLabel(data);
            }

            bool IsValidCreatedLabel(string data)
            {
                if (string.IsNullOrEmpty(data))
                {
                    Debug.Log("Empty data: ");
                    label2D.transform.GetChild(1).gameObject.GetComponent<Button>().interactable = false;
                    return false;
                }
                else
                {
                    label2D.transform.GetChild(1).gameObject.GetComponent<Button>().interactable = true;
                    return true;
                }  
            }      
        }
        
        public void HidePanelAddLabel()
        {
            Destroy(addLabelIcon);
            addLabelIcon = null;
        }

        void SaveLabel(GameObject destroyedObj, InforPointByHit inforPointByHit)
        {
            try
            {
                // Data save label
                PostModelLabel postModelLabel = new PostModelLabel();
                postModelLabel.lessonId = StaticLesson.LessonId;
                postModelLabel.modelId = StaticLesson.ModelId;
                postModelLabel.labelName = destroyedObj.transform.GetChild(0).gameObject.GetComponent<InputField>().text;
                postModelLabel.coordinates = Coordinate.InitCoordinate(inforPointByHit.hitPoint);
                postModelLabel.level = Helper.GetLevelObjectInLevelParent(ObjectManager.Instance.CurrentObject);
                postModelLabel.subLevel = Helper.GetLevelObjectInLevelParent(inforPointByHit.ObjectByHit);

                SaveCoordinate(postModelLabel);
                if (isAddLabelError)
                {
                    Destroy(destroyedObj);
                    return;
                }
                PlayerPrefs.SetString("labelName", destroyedObj.transform.GetChild(0).gameObject.GetComponent<InputField>().text);
                PlayerPrefs.Save();

                isAddingLabel = true;
                CreateLabel(inforPointByHit.hitPoint, inforPointByHit.ObjectByHit, destroyedObj.transform.GetChild(0).gameObject.GetComponent<InputField>().text, PlayerPrefs.GetString("labelId"), LabelManagerBuildLesson.Instance.IsShowingLabel);
                Destroy(destroyedObj);
            }
            catch (Exception exception)
            {
                Toast.ShowCommonToast(exception.Message, APIUrlConfig.SERVER_ERROR_RESPONSE_CODE);
                return;
            }
        }

        void CancelSaveLabel(GameObject destroyObj)
        {
            Destroy(destroyObj);
        }

        public async Task SaveCoordinate(PostModelLabel postModelLabel)
        {
            try
            {
                APIResponse<DataCoordinate> coordinateResponse = await UnityHttpClient.CallAPI<DataCoordinate>(APIUrlConfig.POST_CREATE_MODEL_LABEL, UnityWebRequest.kHttpVerbPOST, postModelLabel);
                if (coordinateResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
                {
                    PlayerPrefs.SetString(StringConfig.labelId, coordinateResponse.data.labelId.ToString());
                    PlayerPrefs.Save();
                    await GetInforDetaiLesson(StaticLesson.LessonId);
                    isAddLabelError = false;
                }
            }
            catch (Exception exception)
            {
                Toast.ShowCommonToast(exception.Message, APIUrlConfig.SERVER_ERROR_RESPONSE_CODE);
                isAddLabelError = true;
            }
        }
        public async Task ConfirmDeleteLabel()
        {
            try
            { 
                APIResponse<string> deleteLabelResponse = await UnityHttpClient.CallAPI<string>($"{APIUrlConfig.DELETE_LABEL}/{PlayerPrefs.GetString(StringConfig.labelId)}", UnityWebRequest.kHttpVerbDELETE);
                if (deleteLabelResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
                {
                    ClearLabelForUpdate();
                    Toast.ShowCommonToast(deleteLabelResponse.message, APIUrlConfig.SUCCESS_RESPONSE_CODE);
                    await GetInforDetaiLesson(StaticLesson.LessonId);
                }
            }
            catch (Exception e)
            {
                Toast.ShowCommonToast(e.Message, APIUrlConfig.SERVER_ERROR_RESPONSE_CODE);
            }
        }

        public async Task UpdateLabelName(string newLabel)
        {
            try
            {
                UpdateLabel updateLabelData = new UpdateLabel();
                updateLabelData.labelName = newLabel;
                APIResponse<string> updateLabelResponse = await UnityHttpClient.CallAPI<string>($"{APIUrlConfig.PATCH_UPDATE_MODEL_LABEL}/{PlayerPrefs.GetString(StringConfig.labelId)}",  APIUrlConfig.PATCH_METHOD, updateLabelData);
                if (updateLabelResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
                {
                    Toast.ShowCommonToast(updateLabelResponse.message, APIUrlConfig.SUCCESS_RESPONSE_CODE);
                    await GetInforDetaiLesson(StaticLesson.LessonId);
                }
            }
            catch (Exception e)
            {
                Toast.ShowCommonToast(e.Message, APIUrlConfig.SERVER_ERROR_RESPONSE_CODE);
            }
        }

        public async Task GetInforDetaiLesson(int lessonId)
        {
            // Call again API to update variable static lesson 
            try
            {
                APIResponse<LessonDetail[]> lessonDetailResponse = await UnityHttpClient.CallAPI<LessonDetail[]>(String.Format(APIUrlConfig.GET_LESSON_BY_ID, lessonId), UnityWebRequest.kHttpVerbGET);
                if (lessonDetailResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
                {
                    StaticLesson.SetValueForStaticLesson(lessonDetailResponse.data[0]);
                }
                else
                {
                    throw new Exception(lessonDetailResponse.message);
                }
            }
            catch (Exception ex)
            {
                Toast.ShowCommonToast(ex.Message, APIUrlConfig.SERVER_ERROR_RESPONSE_CODE);
            }
        }

        /// <summary>
        /// Author: quyennt57
        /// Purpose: Create label
        /// </summary>
        /// <param name="hitPoint">Position create label on object</param>
        /// <param name="parent">Point is belong to subobject ~ parent</param>
        /// <param name="text">Name label</param>

        void CreateLabel(Vector3 hitPoint, GameObject parent, string text, string _labelId, bool _isListLabel)
        {
            if (!_isListLabel)
                ClearLabelForUpdate();
            GameObject point = Instantiate(Resources.Load(PathConfig.MODEL_POINT) as GameObject, hitPoint, Quaternion.identity);
            point.transform.SetParent(parent.transform, false);
            point.name = _labelId;
            point.transform.localScale = ObjectManager.Instance.OriginScaleLabel / ((ObjectManager.Instance.OriginObject.transform.localScale.x / ObjectManager.Instance.OriginScale.x) * parent.transform.localScale.x );
        
            LineRenderer lineRenderer;
            if(point.GetComponent<LineRenderer>() == null)
            {
                lineRenderer = point.AddComponent<LineRenderer>();
            }
            else
            {
                lineRenderer = point.GetComponent<LineRenderer>();
            }

            lineRenderer.SetVertexCount(2);
            lineRenderer.useWorldSpace = false;
            lineRenderer.SetWidth(0.0018f, 0.0018f);
            lineRenderer.material.color = Color.black;
            Vector3 targetPoint = CirclePosition(ObjectManager.Instance.CurrentObject, parent, point.transform.localPosition); 
            lineRenderer.SetPosition(0, Vector3.zero);
            lineRenderer.SetPosition(1, targetPoint);

            GameObject labelObject = Instantiate(Resources.Load(PathConfig.MODEL_TAG_LABEL) as GameObject);
            labelObject.tag = TagConfig.LABEL_TAG;
            labelObject.transform.localPosition = targetPoint;
            labelObject.transform.SetParent(point.transform, false);


            if (hitPoint.x >= 0)
                indexShowLabel = 1;
            else
                indexShowLabel = 0;
           
            labelObject.transform.GetChild(indexShowLabel).gameObject.SetActive(true);
            labelObject.transform.GetChild(indexShowLabel).GetChild(0).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = text;
            if (!_isListLabel || isAddingLabel)
            {
                if (labelObjectHidden != null && !labelObjectHidden.activeSelf) labelObjectHidden.SetActive(true);
                labelObject.SetActive(false);
                labelObjectHidden = labelObject;
                labelUpdatePanel.transform.position = Camera.main.WorldToScreenPoint(labelObject.transform.position);
                labelUpdatePanel.transform.GetChild(indexShowLabel).gameObject.SetActive(true);
                labelUpdatePanel.transform.GetChild(indexShowLabel).GetChild(0).GetChild(1).GetComponent<TMP_InputField>().text = text;
                labelUpdatePanel.SetActive(true);

                isAddingLabel = false;
            }

            LabelObjectInfo labelObjectInfo = new LabelObjectInfo();
            labelObjectInfo.point = point;
            labelObjectInfo.indexSideDisplay = indexShowLabel;
            labelObjectInfo.labelName = text;
            labelObjectInfo.parentBelongTo = parent;
            labelObjectInfo.level = Helper.GetLevelObjectInLevelParent(ObjectManager.Instance.CurrentObject);
            labelObjectInfo.subLevel = Helper.GetLevelObjectInLevelParent(parent);
            TagHandler.Instance.AddLabel(labelObjectInfo);
        }

        /// <summary>
        /// Author: quyennt57
        /// Purpose: Create point second of line
        /// </summary>
        /// <param name="obj">Current object</param>
        /// <param name="radius">Distance from positon current object to target point</param>
        /// <param name="point">point on object</param>
        public Vector3 CirclePosition(GameObject root, GameObject parent, Vector3 point)
        {
            // Vector3 center = Helper.CalculateBounds(root).center;
            Vector3 center = Helper.CalculateBounds(parent).center;

            center = parent.transform.InverseTransformPoint(center);
            Vector3 v = point - center;
            v = (v.normalized * RADIUS);
            return v;
        }

        public void HandleLabelView(bool currentLabelStatus)
        {
            IsShowingLabel = currentLabelStatus;
            if (IsShowingLabel)
            {
                btnLabel.GetComponent<Button>().interactable = false;
                CreateAllLabel();
                btnLabel.GetComponent<Button>().interactable = true;
            }
            else
            {
                btnLabel.GetComponent<Button>().interactable = false;
                ClearLabelForUpdate();
                btnLabel.GetComponent<Button>().interactable = true;
            }
        }

        public async void CreateAllLabel()
        {
            ClearLabelForUpdate();

            string levelObject = "";
            int subLevel = 0;
            int lastIndex;
            GameObject subObject = null;
            Vector3 point;

            levelObject = Helper.GetLevelObjectInLevelParent(ObjectManager.Instance.CurrentObject);
            foreach (Label itemInforLabel in StaticLesson.ListLabel)
            {
                if (itemInforLabel.level == levelObject)
                {
                    if (itemInforLabel.level == itemInforLabel.subLevel)
                    {
                        subObject = ObjectManager.Instance.CurrentObject;
                    }
                    else 
                    {
                        lastIndex = itemInforLabel.subLevel.LastIndexOf(SEPARATE_LEVEL_SYMBOL, StringComparison.Ordinal);
                        subLevel = Convert.ToInt32(itemInforLabel.subLevel.Remove(0, lastIndex + 1));
                        subObject = ObjectManager.Instance.CurrentObject.transform.GetChild(subLevel).gameObject;

                        // subLevel = (int)char.GetNumericValue(itemInforLabel.subLevel[itemInforLabel.subLevel.Length - 1]); 
                        // subObject = ObjectManager.Instance.CurrentObject.transform.GetChild(subLevel).gameObject;
                    }
                    point = new Vector3(itemInforLabel.coordinates.x, itemInforLabel.coordinates.y, itemInforLabel.coordinates.z);
                    isListLabel = true;
                    CreateLabel(point, subObject, itemInforLabel.labelName, itemInforLabel.labelId.ToString(), isListLabel);
                }
            }
        }

        public void ClearLabel()
        {
            if (TagHandler.Instance.listLabelObjects.Count <= 0)
                return;
            foreach (LabelObjectInfo item in TagHandler.Instance.listLabelObjects)
            {
                Destroy(item.point);
            }
            TagHandler.Instance.DeleteLabel();
        }

        public void ClearLabelForUpdate()
        {
            if (LabelManagerBuildLesson.Instance.IsShowingLabel && labelObjectHidden != null)
            {
                Destroy(labelObjectHidden.transform.parent.gameObject);
            }
            else 
            {
                ClearLabel();
            }
            if (labelUpdatePanel.activeSelf)
            {
                labelUpdatePanel.SetActive(false);
            }
        }

        public void OnEndEditLabel(TMP_InputField inputField)
        {
            LabelManagerBuildLesson.Instance.isTyping = false;
            string newNameLabel = inputField.text;
            if (newNameLabel.Length <= 0) 
            {
                Toast.ShowCommonToast(StringConfig.message_error_enterlabel, APIUrlConfig.SERVER_ERROR_RESPONSE_CODE);
                newNameLabel = PlayerPrefs.GetString(StringConfig.labelName);
                return;
            }
            UpdateLabelName(newNameLabel);
            labelUpdatePanel.transform.GetChild(indexShowLabel).GetChild(0).GetChild(1).GetComponent<TMP_InputField>().text = newNameLabel;
            labelObjectHidden.transform.GetChild(indexShowLabel).GetChild(0).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = newNameLabel;
            PlayerPrefs.SetString(StringConfig.labelName, newNameLabel);
            PlayerPrefs.Save();
        }
        
        public void ShowLabelEdit(GameObject labelTouched)
        {
            
            if (labelUpdatePanel.activeSelf)
                labelUpdatePanel.SetActive(false);

            if (labelObjectHidden != null)
                labelObjectHidden.SetActive(true);

            if (labelTouched.transform.GetChild(0).GetChild(0).gameObject.activeSelf)
                indexShowLabel = 0;
            else
                indexShowLabel = 1;

            labelObjectHidden = labelTouched.transform.GetChild(0).gameObject;
            PlayerPrefs.SetString(StringConfig.labelName, labelObjectHidden.transform.GetChild(indexShowLabel).GetChild(0).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text);
            PlayerPrefs.SetString(StringConfig.labelId, labelTouched.name);
            PlayerPrefs.Save();

            labelUpdatePanel.transform.position = Camera.main.WorldToScreenPoint(labelObjectHidden.transform.position);
            labelUpdatePanel.transform.GetChild(indexShowLabel).gameObject.SetActive(true);
            labelUpdatePanel.transform.GetChild(indexShowLabel).GetChild(0).GetChild(1).GetComponent<TMP_InputField>().text = PlayerPrefs.GetString(StringConfig.labelName);
            labelUpdatePanel.SetActive(true);
            labelObjectHidden.SetActive(false);

            TagHandler.Instance.OnMoveLabel();
        }

        public void HideLabelEdit()
        {
            labelUpdatePanel.SetActive(false);
            labelObjectHidden.SetActive(true);
        }

        public bool CheckAvailableLabel(GameObject obj)
        {
            if (StaticLesson.ListLabel.Length <= 0)
                return false;

            string levelObject = Helper.GetLevelObjectInLevelParent(obj);
            foreach (Label item in StaticLesson.ListLabel)
            {
                if (item.level == levelObject)
                    return true;
            }
            return false;
        }

    }
}
