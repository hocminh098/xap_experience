using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.Networking;

namespace BuildLesson
{
    public class TagHandler : MonoBehaviour
    {
        // Tag Handler use to hanlder: - All NormalTag and One 2DTag(with the index)
        private static TagHandler instance;
        public static TagHandler Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<TagHandler>();
                }
                return instance;
            }
        }
        public List<GameObject> addedTags = new List<GameObject>();
        public List<int> labelIds = new List<int>();
        public GameObject labelEditTag;
        public GameObject labelUpdatePanel;
        public int currentEditingIdx = -1; 
        private Vector2 rootLabel2D;
        private Vector3 originLabelScale;
        public List<Vector3> positionOriginLabel = new List<Vector3>();
        public List<LabelObjectInfo> listLabelObjects = new List<LabelObjectInfo>();

        /// <summary>
        /// author: quyennt57
        
        void Update()
        {
            OnMoveLabel();
        }

        public void AddLabel(LabelObjectInfo labelObjectInfor)
        {
            listLabelObjects.Add(labelObjectInfor);
        }

        public void DeleteLabel()
        {
            listLabelObjects.Clear();
        }

        public void OnMoveLabel()
        {
            foreach (LabelObjectInfo item in listLabelObjects)
            {
                if (item.point != null)
                {
                    DenoteLabel(item);
                    MoveLabel(item);
                }
            }
        }

        public void DenoteLabel(LabelObjectInfo labelObjectInfo)
        {
            if (labelObjectInfo.point.transform.position.z > labelObjectInfo.parentBelongTo.transform.position.z)
            {
                labelObjectInfo.point.SetActive(false);
            }
            else
                labelObjectInfo.point.SetActive(true);
         
            if (labelUpdatePanel.activeSelf)
            {
                labelUpdatePanel.transform.position = Camera.main.WorldToScreenPoint(LabelManagerBuildLesson.Instance.labelObjectHidden.transform.position);
            }

            if (labelObjectInfo.point.transform.GetChild(0).GetChild(labelObjectInfo.indexSideDisplay).position.x > labelObjectInfo.parentBelongTo.transform.position.x)
            {   
                labelObjectInfo.indexSideDisplay = 1;
                if (labelUpdatePanel.activeSelf && labelObjectInfo.point.name == LabelManagerBuildLesson.Instance.labelObjectHidden.transform.parent.name)
                {
                    LabelManagerBuildLesson.Instance.indexShowLabel = 1;  
                    
                    labelUpdatePanel.transform.GetChild(1).gameObject.SetActive(true);
                    if (!LabelManagerBuildLesson.Instance.isTyping)
                    {
                        labelUpdatePanel.transform.GetChild(1).GetChild(0).GetChild(1).GetComponent<TMP_InputField>().text = PlayerPrefs.GetString("labelName");
                        labelObjectInfo.labelName = PlayerPrefs.GetString(StringConfig.labelName);
                    }
                    labelUpdatePanel.transform.GetChild(0).gameObject.SetActive(false);

                }
                labelObjectInfo.point.transform.GetChild(0).GetChild(1).gameObject.SetActive(true);
                labelObjectInfo.point.transform.GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = labelObjectInfo.labelName;
                labelObjectInfo.point.transform.GetChild(0).GetChild(0).gameObject.SetActive(false);   
           
            }
            else
            {
                labelObjectInfo.indexSideDisplay = 0;
                if (labelUpdatePanel.activeSelf && labelObjectInfo.point.name == LabelManagerBuildLesson.Instance.labelObjectHidden.transform.parent.name)
                {
                    LabelManagerBuildLesson.Instance.indexShowLabel = 0;  

                    labelUpdatePanel.transform.GetChild(0).gameObject.SetActive(true);
                    if (!LabelManagerBuildLesson.Instance.isTyping)
                    {
                        labelUpdatePanel.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<TMP_InputField>().text = PlayerPrefs.GetString("labelName");
                        labelObjectInfo.labelName = PlayerPrefs.GetString(StringConfig.labelName);
                    }

                    labelUpdatePanel.transform.GetChild(1).gameObject.SetActive(false);
                }
                labelObjectInfo.point.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
                labelObjectInfo.point.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = labelObjectInfo.labelName;
                labelObjectInfo.point.transform.GetChild(0).GetChild(1).gameObject.SetActive(false);
            }
        }

        public void MoveLabel(LabelObjectInfo labelObjectInfo)
        {
            labelObjectInfo.point.transform.GetChild(0).GetChild(labelObjectInfo.indexSideDisplay).transform.LookAt(
                    labelObjectInfo.point.transform.GetChild(0).GetChild(labelObjectInfo.indexSideDisplay).position + Camera.main.transform.rotation * Vector3.forward, 
                    Camera.main.transform.rotation * Vector3.up);
        }
    }
}

