using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Timers;
using System;
using System.Net;
using UnityEngine.Networking; 
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System.IO;
using YoutubePlayer;
using EasyUI.Toast;
using System.Threading.Tasks;

namespace BuildLesson
{
    public class ListItemsManager : MonoBehaviour
    {
        private static ListItemsManager instance;
        public static ListItemsManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<ListItemsManager>();
                }
                return instance;
            }
        }

        public GameObject spinner;
        public Button btnToggleListItem;
        public Animator toggleListItemAnimator;
        public GameObject groupIcon;

        // Panel List Create Lesson 
        public GameObject panelListCreateLesson;
        public Button btnAddAudio; 
        public Button btnAddVideo;
        public Button btnCancelListItem; 
        public GameObject panelAddAudio;
        public Button btnCancelAddAudio;
        public Button btnCancelAudio;

        // Panel Record 
        public GameObject panelRecord; 
        public Button btnCancelAddRecord;
        public Button btnCancelRecord;
        public Button btnRecord; 
        public Button btnUpload;
        public Button btnRedRecord;
        public GameObject timeIndicator;

        // Panel Save Record 
        public GameObject panelSaveRecord;
        public Button btnPlayBack;
        public Button btnCancelSaveRecordTL;
        public Button btnCancelSaveRecordBL;
        public Button btnRecording;

        // Panel Add Video 
        public GameObject panelAddVideo;   
        public Button btnCancelAddVideo;  // X Button 
        public Button btnCancelPasteVideo;  // Cancel Button
        public Button btnSaveAddVideo;
        public GameObject loadingPanelVideo;
        
        public InputField pasteVideo;
        private Regex ytbRegex = new Regex(@"^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube(-nocookie)?\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$"); 
        private string jsonResponse;
        public Text title;
        private int calculatedSize = 25;
        public GameObject videoObj;

        // Panel Upload
        public GameObject panelUploadAudio;
        public Button btnCancelUploadAudio;
        public Button btnCancelUpload;
        public Button btnUploadAudio;

        private bool isRecordAudio = false;

        public static float startTime = 0f;
        private AudioSource audioSource;
        private bool isPlayingAudio = false;
        // add sampling rate 
        private static int samplingRate;

        void Awake()
        {
            // Query the maximum frequency of the default microphone.
            int minSamplingRate; 
            Microphone.GetDeviceCaps(null, out minSamplingRate, out samplingRate);
        }

        void Start()
        {
            spinner.SetActive(false);
            loadingPanelVideo.SetActive(false);
            foreach (var device in Microphone.devices)
            {
                Debug.Log("Record Pannel Name: " + device);
            }
            InitEvents();
            LoadDataListItemPanel.Instance.UpdateLessonInforPannel(SaveLesson.lessonId); 
        }
        
        void Update()
        {
            if (panelRecord.activeSelf && isRecordAudio)
            {
                startTime += Time.deltaTime;
                DisplayTime(startTime, timeIndicator);
            }
            if (panelRecord.activeSelf && !isRecordAudio)
            {
                timeIndicator.GetComponent<Text>().text = "00:00";
            }
            if (pasteVideo.isFocused == false)
            {
                pasteVideo.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldNormal);
            }
        }
    
        void InitEvents()
        {
            btnToggleListItem.onClick.AddListener(ToggleListItem);
            btnAddAudio.onClick.AddListener(HandlerAddAudio);
            btnCancelListItem.onClick.AddListener(CancelListItem); 
            btnCancelAddAudio.onClick.AddListener(CancelAddAudio);
            btnCancelAudio.onClick.AddListener(CancelAudio);
            
            // Record
            btnCancelAddRecord.onClick.AddListener(CancelAddRecord);
            btnCancelRecord.onClick.AddListener(CancelRecord);
            btnRecord.onClick.AddListener(RecordLesson);
            btnUpload.onClick.AddListener(UploadAudioLesson);
            btnRedRecord.onClick.AddListener(HandlerRedRecord);

            // Playback Audio, Save Record
            btnPlayBack.onClick.AddListener(PlayStopAudio);
            btnCancelSaveRecordTL.onClick.AddListener(HandlerCancelSaveRecordTL);
            btnCancelSaveRecordBL.onClick.AddListener(HandlerCancelSaveRecordBL);
            btnRecording.onClick.AddListener(HandlerRecording);

            // Upload
            btnCancelUploadAudio.onClick.AddListener(CancelUploadAudio);
            btnCancelUpload.onClick.AddListener(CancelUpload);
            btnUploadAudio.onClick.AddListener(HandlerUploadAudio);
            
            // Add Video
            btnSaveAddVideo.interactable = false;
            btnAddVideo.onClick.AddListener(HandlerAddVideo);
            btnCancelAddVideo.onClick.AddListener(HandlerCancelAddVideo);
            btnCancelPasteVideo.onClick.AddListener(HandlerCancelPasteVideo);
            btnSaveAddVideo.onClick.AddListener(HandlerSaveAddVideo); 
            pasteVideo.onEndEdit.AddListener(delegate {LockInput(pasteVideo);});
            pasteVideo.keyboardType = TouchScreenKeyboardType.URL;
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: Checks if there is anything entered into the input field.
        /// </summary>
        void LockInput(InputField input)
        {
            if (input.text.Length > 0)
            {
                loadingPanelVideo.SetActive(true);
                if (ytbRegex.IsMatch(input.text.Trim()))
                {
                    InfoLinkVideo info = GetVideoInfo(input.text.Trim());
                    title.gameObject.GetComponent<Text>().text = Helper.FormatString(info.title.ToLower(), calculatedSize);
                    VideoManagerBuildLesson.Instance.ShowVideo(input.text.Trim());
                    btnSaveAddVideo.interactable = true;
                }
                else
                {
                    btnSaveAddVideo.interactable = false;
                    title.gameObject.GetComponent<Text>().text = "Video not available";
                }
            }
            else if (input.text.Length == 0)
            {
                btnSaveAddVideo.interactable = false;
                title.gameObject.GetComponent<Text>().text = "Video not available";
            }
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: Check add video label or video lesson
        /// </summary>
        void HandlerSaveAddVideo()
        {
            if (panelAddVideo.transform.GetChild(0).GetChild(1).GetComponent<Text>().text == "Intro")
            {
                SaveAddVideo(SaveLesson.lessonId, pasteVideo.text);
            }
            else 
            {
                SaveAddVideoLabel(Convert.ToInt32(PlayerPrefs.GetString("labelId")), pasteVideo.text);
            }
        }

        void ToggleListItem()
        {
            toggleListItemAnimator.SetBool(AnimatorConfig.isShowMeetingMemberList, !toggleListItemAnimator.GetBool(AnimatorConfig.isShowMeetingMemberList)); 
            groupIcon.transform.Rotate(0.0f, 0.0f, 180.0f, Space.Self);
        }
        void CancelListItem()
        {
            toggleListItemAnimator.SetBool(AnimatorConfig.isShowMeetingMemberList, false);
        }
        void HandlerAddAudio()
        {
            panelAddAudio.SetActive(true);
            panelAddAudio.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = "Add audio";
        }
        void HandlerAddVideo()
        {
            panelAddVideo.SetActive(true);
            panelAddVideo.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = "Intro";
            panelListCreateLesson.SetActive(false);
        }
       
        void CancelAddAudio()
        {
            panelAddAudio.SetActive(false);
            panelListCreateLesson.SetActive(false);
        }
        void CancelAudio()
        {
            panelAddAudio.SetActive(false);
            panelListCreateLesson.SetActive(false);
        }
        void CancelAddRecord()
        {
            startTime = 0f;
            timeIndicator.GetComponent<Text>().text = "00:00";
            btnRedRecord.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageRecordingIcon);
            panelRecord.SetActive(false);
            panelListCreateLesson.SetActive(false);
        }
        void CancelRecord()
        {
            startTime = 0f;
            timeIndicator.GetComponent<Text>().text = "00:00";
            btnRedRecord.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageRecordingIcon);
            panelRecord.SetActive(false);
            isRecordAudio = false;
            panelListCreateLesson.SetActive(false);
        }
        void RecordLesson()
        {
            panelAddAudio.SetActive(false);
            panelRecord.SetActive(true);
            panelRecord.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = Helper.ShortString(panelAddAudio.transform.GetChild(0).GetChild(1).GetComponent<Text>().text, 10);
        }
        void UploadAudioLesson()
        {
            panelAddAudio.SetActive(false);
        }
        void CancelUploadAudio()
        {
            panelUploadAudio.SetActive(false);
            panelListCreateLesson.SetActive(false);
        }
        void CancelUpload()
        {
            panelUploadAudio.SetActive(false);
            panelListCreateLesson.SetActive(false);
        }
        void HandlerCancelAddVideo()
        {
            pasteVideo.text = "";
            title.gameObject.GetComponent<Text>().text = "";
            videoObj.GetComponent<RawImage>().texture = null;
            videoObj.transform.GetChild(1).GetComponent<Button>().onClick.RemoveAllListeners();
            panelAddVideo.SetActive(false);
            panelListCreateLesson.SetActive(false);
        }
        void HandlerCancelPasteVideo()
        {
            pasteVideo.text = "";
            title.gameObject.GetComponent<Text>().text = "";
            videoObj.GetComponent<RawImage>().texture = null;
            videoObj.transform.GetChild(1).GetComponent<Button>().onClick.RemoveAllListeners();
            panelAddVideo.SetActive(false);
            panelListCreateLesson.SetActive(false);
        }
        void HandlerRecording()
        {
            panelSaveRecord.SetActive(false);
            panelRecord.SetActive(true);
            HandlerRedRecord();
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: Enter save call API AddVideoLesson
        /// </summary>
        public async void SaveAddVideo(int lessonId, string video)
        {
            spinner.SetActive(true);
            try
            {
                AddVideoRequest addVideoRequest = new AddVideoRequest();
                addVideoRequest.Init(lessonId, video);
                APIResponse<string> addVideoResponse = await UnityHttpClient.CallAPI<string>(APIUrlConfig.POST_ADD_VIDEO_LESSON, UnityWebRequest.kHttpVerbPOST, addVideoRequest);
                if (addVideoResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
                {
                    panelAddVideo.SetActive(false);
                    panelListCreateLesson.SetActive(false);
                    pasteVideo.text = "";
                    title.gameObject.GetComponent<Text>().text = "";
                    VideoManagerBuildLesson.Instance.ResetVideo2();
                    spinner.SetActive(false);
                    toggleListItemAnimator.SetBool(AnimatorConfig.isShowMeetingMemberList, true);
                    LoadDataListItemPanel.Instance.UpdateLessonInforPannel(lessonId);
                }
            }
            catch (Exception e)
            {
                spinner.SetActive(false);
            }
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: Enter save call API AddVideoLabel
        /// </summary>
        public async void SaveAddVideoLabel(int labelId, string video)
        {
            spinner.SetActive(true);
            try
            {
                AddVideoLabelRequest addVideoLabelRequest = new AddVideoLabelRequest();
                addVideoLabelRequest.Init(labelId, video);
                APIResponse<string> addVideoLabelResponse = await UnityHttpClient.CallAPI<string>(APIUrlConfig.POST_ADD_VIDEO_LABEL, UnityWebRequest.kHttpVerbPOST, addVideoLabelRequest);
                if (addVideoLabelResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
                {
                    panelAddVideo.SetActive(false);
                    panelListCreateLesson.SetActive(false);
                    pasteVideo.text = "";
                    title.gameObject.GetComponent<Text>().text = "";
                    VideoManagerBuildLesson.Instance.ResetVideo2();
                    spinner.SetActive(false);
                    toggleListItemAnimator.SetBool(AnimatorConfig.isShowMeetingMemberList, true);
                    LoadDataListItemPanel.Instance.UpdateLessonInforPannel(SaveLesson.lessonId);
                }
            }
            catch (Exception e)
            {
                spinner.SetActive(false);
            }
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: Get video info from link Youtube
        /// </summary>
        public InfoLinkVideo GetVideoInfo(string link)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(String.Format(APIUrlConfig.GetLinkVideo, link)); 
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            jsonResponse = reader.ReadToEnd();
            return JsonUtility.FromJson<InfoLinkVideo>(jsonResponse); 
        }

        public string DecodeFromUtf8(string utf8String)
        {
            byte[] utf8Bytes = new byte[utf8String.Length];
            for (int i = 0; i < utf8String.Length; ++i)
            {
                utf8Bytes[i] = (byte)utf8String[i];
            }
            return Encoding.UTF8.GetString(utf8Bytes,0,utf8Bytes.Length);
        }

        void HandlerCancelSaveRecordTL()
        {
            panelSaveRecord.SetActive(false);
            panelListCreateLesson.SetActive(false);
            startTime = 0f;
            timeIndicator.GetComponent<Text>().text = "00:00";
            btnRedRecord.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageRecordingIcon);
        }
        void HandlerCancelSaveRecordBL()
        {
            startTime = 0f;
            timeIndicator.GetComponent<Text>().text = "00:00";
            btnRedRecord.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageRecordingIcon);
            panelSaveRecord.SetActive(false);
            panelListCreateLesson.SetActive(false);
        }

        void HandlerRedRecord()
        {
            isRecordAudio = !isRecordAudio;
            if (isRecordAudio)
            {
                btnRedRecord.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageStopRecordingIcon);
                /// Purpose: Start recording
                audioSource = panelSaveRecord.GetComponent<AudioSource>();
                audioSource.clip = Microphone.Start(null, true, 3599, samplingRate); // Maximum record 1 hour
            }
            else 
            {
                startTime = 0f;
                btnRedRecord.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageRecordingIcon);
                panelRecord.SetActive(false);
                panelSaveRecord.SetActive(true);
                panelSaveRecord.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = Helper.ShortString(panelRecord.transform.GetChild(0).GetChild(1).GetComponent<Text>().text, 10);
                /// Purpose: Stop recording
                audioSource = EndRecording(audioSource);
                AudioManager.Instance.DisplayAudio(true);
            }
        }

        void DisplayTime(float timeToDisplay, GameObject time)
        {
            float minutes = Mathf.FloorToInt(timeToDisplay / 60);  
            float seconds = Mathf.FloorToInt(timeToDisplay % 60);
            time.GetComponent<Text>().text = string.Format("{0:00}:{1:00}", minutes, seconds);
        }

        AudioSource EndRecording (AudioSource audS) 
        {
            // Capture the current clip data
            AudioClip recordedClip = audS.clip;
            var position = Microphone.GetPosition(null);  // Use default microphone 
            var soundData = new float[recordedClip.samples * recordedClip.channels];
            recordedClip.GetData (soundData, 0);
            // Create shortened array for the data that was used for recording
            var newData = new float[position * recordedClip.channels];
            // anonymous Microphone.End (null);
            // Copy the used samples to a new array
            for (int i = 0; i < newData.Length; i++) 
            {
                newData[i] = soundData[i];
            }
            // One does not simply shorten an AudioClip,
            // so we make a new one with the appropriate length
            var newClip = AudioClip.Create(recordedClip.name, position, recordedClip.channels, recordedClip.frequency, false);
            newClip.SetData(newData, 0); // Give it the data from the old clip
            // Replace the old clip
            AudioClip.Destroy(recordedClip);
            audS.clip = newClip;   
            return audS;
        }

        void PlayStopAudio()
        {
            isPlayingAudio = !isPlayingAudio;
            AudioManager.Instance.ControlAudio(isPlayingAudio);
        }

        public void HandlerUploadAudio()
        {
            Debug.Log("Upload audio from local: ");
        }
    }
}
