using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BuildLesson
{
    public class SeparateManagerBuildLesson : MonoBehaviour
    {
        private static SeparateManagerBuildLesson instance;
        public static SeparateManagerBuildLesson Instance
        {
            get
            {
                if (instance == null)
                    instance = FindObjectOfType<SeparateManagerBuildLesson>();
                return instance;
            }
        }

        private float r = 1f;
        private const float MIN = 0.01f;
        private const float RADIUS = 8f;
        private const float DISTANCE_FACTOR = 1.5f;

        // variable
        private int childCount;
        private Vector3 centerPosition;
        private Vector3 centerPosCurrentObject;
        private Vector3 centerPosChildObject;

        private Vector3 targetPosition;
        private float angle;
        public Button btnSeparate;
        private bool isSeparating;
        public bool IsSeparating 
        {
            get
            {
                return isSeparating;
            }
            set
            {
                isSeparating = value;
                btnSeparate.GetComponent<Image>().sprite = isSeparating ? Resources.Load<Sprite>(PathConfig.SEPARATE_CLICKED_IMAGE) : Resources.Load<Sprite>(PathConfig.SEPARATE_UNCLICK_IMAGE);
            }
        }

        public void HandleSeparate(bool isSeparating)
        {
            IsSeparating = isSeparating;
            if (IsSeparating)
            {
                btnSeparate.interactable = false;
                SeparateOrganModel();
                btnSeparate.interactable = true;
            }
            else
            {
                btnSeparate.interactable = false;
                BackToPositionOrgan();
                btnSeparate.interactable = true;
            }
        }

        GameObject InstantiateItem(int n, int i)
        {
            float angle = (360 / n ) * i * Mathf.Deg2Rad;
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.position = new Vector3(r  * Mathf.Sin(angle), r * Mathf.Cos(angle), 0);
            return cube;
        }

        void PreventOverlap(GameObject obj)
        {
            Bounds boundObject = Helper.CalculateBounds(obj);
            float factorX = (boundObject.center.x < 0) ? -1 : 1;
            float factorY = (boundObject.center.y < 0) ? -1 : 1;
            if (isEqualToZero(boundObject.center.x))
            {
                obj.transform.localPosition += new Vector3(0, factorY * boundObject.extents.y, 0) / ObjectManager.Instance.FactorScaleInitial;
            }
            else if (isEqualToZero(boundObject.center.y))
            {
                obj.transform.localPosition += new Vector3(factorX * boundObject.extents.x, 0, 0) / ObjectManager.Instance.FactorScaleInitial;
            }
            else
            {
                obj.transform.localPosition += new Vector3(factorX * boundObject.extents.x, factorY * boundObject.extents.y, 0) / ObjectManager.Instance.FactorScaleInitial;
            }
        }

        bool isEqualToZero(float x)
        {
            return Mathf.Abs(x) < MIN;
        }

        public void SeparateOrganModel()
        {
            
        }

        private Vector3 CalculateCentroid()
        {
            Vector3 centroid = new Vector3(0, 0, 0);

            foreach (Vector3 localPosition in ObjectManager.Instance.ListchildrenOfOriginPosition)
            {
                centroid += localPosition;
            }
            centroid /= ObjectManager.Instance.ListchildrenOfOriginPosition.Count;
            return centroid;
        }

        public Vector3 ComputeTargetPosition(Vector3 origin, Vector3 target)
        {
            Vector3 dir = target - origin;
            return dir.normalized * DISTANCE_FACTOR;
        }

        public IEnumerator MoveObjectWithLocalPosition(GameObject moveObject, Vector3 targetPosition)
        {
            float timeSinceStarted = 0f;
            while (true)
            {
                timeSinceStarted += Time.deltaTime;
                moveObject.transform.localPosition = Vector3.Lerp(moveObject.transform.localPosition, targetPosition, timeSinceStarted);
                if (moveObject.transform.localPosition == targetPosition)
                {
                    yield break;
                }
                yield return null;
            }
        }

        public void BackToPositionOrgan()
        {
            int childCount = ObjectManager.Instance.CurrentObject.transform.childCount;
            if (childCount < 1)
            {
                return;
            }
            for (int i = 0; i < childCount; i++)
            {
                if (ObjectManager.Instance.CurrentObject.transform.GetChild(i).gameObject.tag != TagConfig.LABEL_TAG)
                {
                    targetPosition = ObjectManager.Instance.ListchildrenOfOriginPosition[i];	
                    StartCoroutine(MoveObjectWithLocalPosition(ObjectManager.Instance.CurrentObject.transform.GetChild(i).gameObject, targetPosition));
                }
            }
        }
    }
}
