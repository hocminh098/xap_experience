using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using System; 
using UnityEngine.SceneManagement;
using TMPro; 
using UnityEngine.Networking;
using System.Text;
using EasyUI.Toast;
using System.Threading.Tasks;

namespace BuildLesson
{
    public class TouchHandler : MonoBehaviour
    {
        private static TouchHandler instance; 
        public static TouchHandler Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<TouchHandler>();
                }
                return instance; 
            }
        }
        public static event Action onResetStatusFeature; 
        public static event Action<GameObject> onSelectChildObject; 

        public GameObject UIComponent;
        const float ROTATION_RATE = 0.08f;
        const float LONG_TOUCH_THRESHOLD = 0.8f; 
        const float ROTATION_SPEED = 0.5f; 
        const float MIN_SWIPE_MAGNITUDE = 20.0f;
        float touchDuration = 0.0f; 
        Touch touch; 
        Touch touchZero; 
        Touch touchOne; 
        float originDelta; 
        Vector3 originScale;

        Vector2 swipeStartPosition;

        Vector3 originLabelScale = new Vector3(1f, 1f, 1f);
        Vector3 originLabelTagScale = new Vector3(7f, 1f, 1f);
        Vector3 originLineScale = new Vector3(1f, 1f, 1f); 
        Vector3 originScaleSelected;
        bool isMovingByLongTouch = false; 
        bool isLongTouch = false;
        float currentDelta;
        float scaleFactor;

        private GameObject currentSelectedObject; 
        private GameObject recentSelectedObject;
        private Vector3 centerPosition;
        private Vector3 mOffset; 
        private float mZCoord;
        private string currentSelectedLabelOrganName;
        private Vector3 hitPoint;
        private Vector2 hitPoint2D;

        // Panel DeleteTag
        public GameObject panelPopUpDeleteLabel;
        public Button btnExitPopupDeleteLabel; 
        public Button btnDeleteLabel; 
        public Button btnCancelDeleteLabel;

        // Panel AddActivities: addVideo, addAudio
        public GameObject panelAddActivities;
        public Button btnAddAudioLabel;
        public Button btnAddVideoLabel;
        public Button btnCancelAddActivities;

        private int calculatedSize = 10;
        private bool isRotation = false;

        /// <summary>
        /// author: quyennt57
        /// aim: Detected touch on screen
        /// </summary>
        public void HandleTouchInteraction()    
        {
            if (ObjectManager.Instance.CurrentObject == null)
            {
                return;
            }
            if (Input.touchCount == 3)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Moved && Input.GetTouch(1).phase == TouchPhase.Moved && Input.GetTouch(2).phase == TouchPhase.Moved)
                {
                    HandleSimultaneousThreeTouch(Input.GetTouch(1));
                }
            }
            else if (Input.touchCount == 2)
            {
                touchZero = Input.GetTouch(0);
                touchOne = Input.GetTouch(1);
                HandleSimultaneousTouch(touchZero, touchOne);
            }
            else if (Input.touchCount == 1)
            {
                touch = Input.GetTouch(0);
                if (touch.tapCount == 2)
                {
                    touch = Input.touches[0];
                    if (touch.phase == TouchPhase.Ended)
                    {
                        HandleDoupleTouch(touch);
                    }
                }
                else
                {
                    HandleSingleTouch(touch);
                }
            }
        }

        private void HandleSingleTouch(Touch touch)
        {
            switch (touch.phase)
            {
                case TouchPhase.Began: 
                {
                    swipeStartPosition = touch.position;
                    ResetLongTouch(); 
                    break; 
                }
                case TouchPhase.Stationary: 
                {
                    if (!isLongTouch)
                    {
                        touchDuration += Time.deltaTime;
                        if (touchDuration > LONG_TOUCH_THRESHOLD)
                        {
                            isLongTouch = true;
                        }
                    }
                    break;
                }
                case TouchPhase.Moved:
                {
                    if (LabelManagerBuildLesson.Instance.addLabelIcon != null)
                    {
                        break;
                    }

                    Vector2 deltaStart = touch.position - swipeStartPosition;
                    if (deltaStart.magnitude > MIN_SWIPE_MAGNITUDE) {
                        Rotate(touch);
                        break;
                    }
                    break;
                }
                case TouchPhase.Ended: 
                {
                    if (isLongTouch)
                    {
                        LabelManagerBuildLesson.Instance.DisplayPanelAddLabel(touch);
                        break;
                    }

                    SingleTouchOnLabel();
                    ResetLongTouch(); 
                    break;
                }
                case TouchPhase.Canceled: 
                {
                    ResetLongTouch(); 
                    break;
                }
            }
        }

        public void SingleTouchOnLabel()
        {
            GameObject labelTouched = Helper.IsTouchOnLabelEdit(touch.position, TagConfig.LABEL_TAG);
            if (labelTouched != null && labelTouched.tag == TagConfig.LABEL_TAG && !isRotation)
            {
                LabelManagerBuildLesson.Instance.ShowLabelEdit(labelTouched);
            }
            else if (LabelManagerBuildLesson.Instance.addLabelIcon && Helper.IsTouchWithoutUI())
            {
                LabelManagerBuildLesson.Instance.HidePanelAddLabel();
            }
            else if (LabelManagerBuildLesson.Instance.labelUpdatePanel.activeSelf && Helper.IsTouchWithoutUI() && !isRotation)
            {
                LabelManagerBuildLesson.Instance.HideLabelEdit();
            }

            return;
        }

        private void Rotate(Touch touch)
        {
            isRotation = true;
            ObjectManager.Instance.OriginObject.transform.Rotate(touch.deltaPosition.y * ROTATION_RATE, -touch.deltaPosition.x * ROTATION_RATE, 0, Space.World);
        }
        
        void ResetLongTouch()
        {
            touchDuration = 0f;
            isRotation = false;
            isLongTouch = false;
        }

        private void Drag(Touch touch, GameObject obj)
        {
            if (obj != null)
            {
                Vector3 centerObject = Helper.CalculateBounds(obj).center;
                obj.transform.localPosition += (Helper.GetTouchPositionAsWorldPoint(touch) - centerObject) / ObjectManager.Instance.FactorScaleInitial;
            }
        }
        private void HandleDoupleTouch(Touch touch)
        {
            GameObject selectedObject = Helper.GetChildOrganOnTouchByTag(touch.position);

            if (selectedObject == null || selectedObject == ObjectManager.Instance.OriginObject || ObjectManager.Instance.CurrentObject.transform.childCount < 1)
            {
                return;
            }
            onSelectChildObject?.Invoke(selectedObject);
        }

         private void HandleSimultaneousTouch(Touch touchZero, Touch touchOne)
        {
            if (touchZero.phase == TouchPhase.Began || touchOne.phase == TouchPhase.Began)
            {
                originDelta = Vector2.Distance(touchZero.position, touchOne.position);
                originScale = ObjectManager.Instance.OriginObject.transform.localScale;
            }
            else if (touchZero.phase == TouchPhase.Moved || touchOne.phase == TouchPhase.Moved)
            {
                currentDelta = Vector2.Distance(touchZero.position, touchOne.position);
                scaleFactor = currentDelta / originDelta;
                ObjectManager.Instance.OriginObject.transform.localScale = originScale * scaleFactor;
                foreach (LabelObjectInfo item in TagHandler.Instance.listLabelObjects)
                {
                    item.point.transform.localScale = ObjectManager.Instance.OriginScaleLabel / ((ObjectManager.Instance.OriginObject.transform.localScale.x / ObjectManager.Instance.OriginScale.x) * item.parentBelongTo.transform.localScale.x);
                }
            }
        }

        private void HandleSimultaneousThreeTouch(Touch touch)
        {
            Drag(touch, ObjectManager.Instance.OriginObject);
        }
    }
}

