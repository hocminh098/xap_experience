using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using System;
using UnityEngine.Networking;
using EasyUI.Toast;
using System.Threading.Tasks;
using BuildLesson;

namespace YoutubePlayer
{
    public class LoadVideoManager : MonoBehaviour
    {
        public GameObject mainViewVideo;
        YoutubePlayer youtubePlayer;
        VideoPlayer videoPlayer;
        public Button btnControlVideo;
        bool _IsPlayingVideo { get; set;} = false;
        public bool IsPlayingVideo { get; set; } = false;
        public GameObject panelLoading;

        void Start()
        {
            InitEvent();
        }

        void InitEvent()
        {
            btnControlVideo.onClick.AddListener(HandleControlVideo);
        }

        void HandleControlVideo()
        {
            IsPlayingVideo = !IsPlayingVideo;
            ControlVideo(IsPlayingVideo);
        }

        void Awake()
        {
            videoPlayer = mainViewVideo.GetComponent<VideoPlayer>();
            videoPlayer.prepareCompleted += VideoPlayerPreparedCompleted;
            youtubePlayer = mainViewVideo.GetComponent<YoutubePlayer>();
        }

        void Update()
        {
            if (videoPlayer != null)
            {
                
            }
        }

        public void ShowVideo()
        {
            btnControlVideo.interactable = false;
            Prepare();
        }

        void VideoPlayerPreparedCompleted(VideoPlayer source)
        {
            panelLoading.SetActive(!source.isPrepared);
            btnControlVideo.interactable = source.isPrepared;
        }

        public async void Prepare()
        {
            panelLoading.SetActive(true);
            Debug.Log("Loading video...");
            try
            {
                Exception exception = Network.CheckNetWorkToDisplayToast();
                if (exception != null)
                    throw exception;
                await youtubePlayer.PrepareVideoAsync();
                Debug.Log("Loading video success!");
            }
            catch(Exception exception)
            {
                videoPlayer = null;
                Toast.ShowCommonToast(exception.Message, APIUrlConfig.SERVER_ERROR_RESPONSE_CODE);
                Debug.Log("ERROR video " + exception);
            }
        }

        public void ControlVideo(bool _IsPlayingVideo)
        {        
            IsPlayingVideo = _IsPlayingVideo;
            if (videoPlayer != null)
            {
                if (IsPlayingVideo)
                {
                    PlayVideo();
                    btnControlVideo.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathConfig.AUDIO_PAUSE_IMAGE);
                }
                else
                {
                    PauseVideo();
                    btnControlVideo.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathConfig.AUDIO_START_PLAY_IMAGE);
                }
            }
        }

        public void PlayVideo()
        {
            videoPlayer.Play();
        }

        public void PauseVideo()
        {
            videoPlayer.Pause();
        }

        public void ResetVideo()
        {
            if (videoPlayer != null)
                videoPlayer.Stop();
            btnControlVideo.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathConfig.AUDIO_START_PLAY_IMAGE);
        }

        public void ExitVideo()
        {
            ResetVideo();
        }

        void OnDestroy()
        {
            videoPlayer.prepareCompleted -= VideoPlayerPreparedCompleted;
        }
    }
}
