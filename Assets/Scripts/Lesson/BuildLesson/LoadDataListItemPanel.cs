using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Net;
using UnityEngine.Networking;
using System.Text;
using System.IO;
using UnityEngine.UI;
using EasyUI.Toast;
using System.Threading.Tasks;
using TMPro;
using BuildLesson;

namespace YoutubePlayer 
{
    public class LoadDataListItemPanel : MonoBehaviour
    {
        private static LoadDataListItemPanel instance;
        public static LoadDataListItemPanel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<LoadDataListItemPanel>();
                }
                return instance; 
            }
        }
        public GameObject lessonInfoPanel;
        public GameObject showListItem;
        public GameObject txtShowListItem;
        public GameObject spinner;
        public Transform contentMedia;
        public int IndexMediaInContent = -1;
        private string jsonResponse;

        // panel PopUpDeleteActions
        public GameObject panelPopUpDeleteActions;
        public Button btnExitPopupDeleteActions;
        public Button btnDeleteActions;
        public Button btnCancelDeleteActions; 

        // panel PopUpDeleteActionsVideo
        public GameObject panelPopUpDeleteActionsVideo;
        public Button btnExitPopupDeleteActionsVideo;
        public Button btnDeleteActionsVideo;
        public Button btnCancelDeleteActionsVideo; 

        public GameObject ViewPort; 
        public GameObject ContentMedia;
        public GameObject ScrollView;

        void Update()
        {
            btnExitPopupDeleteActions.onClick.AddListener(HandlerExitPopupDeleteActions);
            btnCancelDeleteActions.onClick.AddListener(HandlerCancelDeleteActions);
            btnExitPopupDeleteActionsVideo.onClick.AddListener(HandlerExitPopupDeleteActionsVideo);
            btnCancelDeleteActionsVideo.onClick.AddListener(HandlerCancelDeleteActionsVideo);
            panelPopUpDeleteActions.transform.GetChild(0).transform.GetComponent<Button>().onClick.AddListener(HandlerExitPopupDeleteActions);
            panelPopUpDeleteActionsVideo.transform.GetChild(0).transform.GetComponent<Button>().onClick.AddListener(HandlerExitPopupDeleteActionsVideo);
        
            /// Purpose: Check the size of the Content Size filter and the View Port
            if (ViewPort.transform.GetComponent<RectTransform>().rect.height >= ContentMedia.transform.GetComponent<RectTransform>().rect.height)
            {
                /// Purpose: Disable the scroll view
                ScrollView.transform.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
                ScrollView.transform.GetComponent<ScrollRect>().enabled = false;
            }
            else 
            {
                /// Purpose: Enable the sroll view
                ScrollView.transform.GetComponent<ScrollRect>().enabled = true;
            }
        }
        
        void HandlerExitPopupDeleteActions()
        {
            panelPopUpDeleteActions.SetActive(false);
        }
        void HandlerCancelDeleteActions()
        {
            panelPopUpDeleteActions.SetActive(false);
        }
        void HandlerExitPopupDeleteActionsVideo()
        {
            panelPopUpDeleteActionsVideo.SetActive(false);
        }
        void HandlerCancelDeleteActionsVideo()
        {
            panelPopUpDeleteActionsVideo.SetActive(false); 
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: Update lesson info in Panel
        /// </summary>
        public async Task UpdateLessonInforPannel(int lessonId)
        {
            bool flag = true;
            try
            {
                APIResponse<List<LessonDetail>> lessonDetailResponse = await UnityHttpClient.CallAPI<List<LessonDetail>>(String.Format(APIUrlConfig.GET_LESSON_BY_ID, lessonId), UnityWebRequest.kHttpVerbGET); 
                if (lessonDetailResponse.code  == APIUrlConfig.SUCCESS_RESPONSE_CODE)
                {
                    // Use static class to store 
                    StaticLesson.SetValueForStaticLesson(lessonDetailResponse.data[0]);
                    // Refresh the item 
                    foreach (Transform child in lessonInfoPanel.transform)
                    {
                        GameObject.Destroy(child.gameObject);
                    }
                    // Load all infomation from the api result 
                    if (StaticLesson.Audio != "")
                    {
                        flag = false;
                        loadAudio(StaticLesson.Audio, lessonId); 
                    }
                    if (StaticLesson.Video != "")
                    {
                        flag = false;
                        StartCoroutine(loadVideo(StaticLesson.Video, lessonId));
                    } 
                    if (StaticLesson.ListLabel.Length > 0) 
                    {
                        foreach (Label label in StaticLesson.ListLabel)
                        {
                            if (label.audioLabel != "")
                            {
                                flag = false;
                                loadAudio(label.audioLabel, lessonId, label.labelName); 
                            }
                            if (label.videoLabel != "")
                            {
                                flag = false;
                                StartCoroutine(loadVideo(label.videoLabel, lessonId, label.labelName, label.labelId));
                            }
                        }
                    }
                    txtShowListItem.SetActive(flag);            
                }
                else 
                {
                    throw new Exception(lessonDetailResponse.message);
                }
            }
            catch (Exception e)
            {
                Debug.Log($"Update lesson info panel failed: {e.Message}");
            }
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: Load audio(s) in Panel
        /// </summary>
        private async Task loadAudio(string audioUrl, int lessonId, string title = "Intro", int labelId = -1)
        {
            GameObject audioComp = Instantiate(Resources.Load(PathConfig.ADD_AUDIO) as GameObject);
            GameObject btnShowPanelAudio = audioComp.transform.GetChild(0).GetChild(0).gameObject;
            GameObject btnHidePanelAudio = audioComp.transform.GetChild(0).GetChild(1).gameObject;
            Button btnDeleteAudio = audioComp.transform.GetChild(0).GetChild(2).GetComponent<Button>();
            Text txtNameAudioShow = audioComp.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>();
            Text txtNameAudioHide = audioComp.transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<Text>();

            // Event button 
            btnShowPanelAudio.GetComponent<Button>().onClick.AddListener(() => ShowAudio(audioUrl, labelId, audioComp));
            btnHidePanelAudio.GetComponent<Button>().onClick.AddListener(() => HideAudio(audioComp));

            if (title == "Intro" && labelId == -1)
            {
                btnDeleteAudio.onClick.AddListener(() => HandlerDeleteAudio(lessonId, title));
            }
            else
            {
                btnDeleteAudio.onClick.AddListener(() => HandlerDeleteAudioLabel(lessonId, Convert.ToInt32(PlayerPrefs.GetString("labelId")), title));
            }
            audioComp.name = String.Format("Audio: {0}", labelId);

            txtNameAudioShow.text = String.Format("Audio: {0}", Helper.ShortString(title, 15));
            txtNameAudioHide.text = String.Format("Audio: {0}", Helper.ShortString(title, 15));

            audioComp.transform.parent = lessonInfoPanel.transform;     
            audioComp.transform.localScale = new Vector3(1f, 1f, 1f);  
            AudioClip audioData = await UnityHttpClient.GetAudioClip(audioUrl); 
            audioComp.GetComponent<AudioSource>().clip = audioData;
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: Get index object in content Media
        /// </summary>
        public int GetIndexObjectInContentMedia(Transform currentTransform)
        {
            int index = -1;
            index = currentTransform.transform.GetSiblingIndex();
            return index;
        }
        
        /// <summary>
        /// Author: minhlh17
        /// Purpose: Load video(s) in Panel
        /// </summary>
        private IEnumerator loadVideo(string videoUrl, int lessonId, string title = "Intro", int labelId = -1)
        {
            GameObject videoComp = Instantiate(Resources.Load(PathConfig.ADD_VIDEO) as GameObject);
            GameObject btnShowPanelVideo = videoComp.transform.GetChild(0).GetChild(0).gameObject;
            GameObject btnHidePanelVideo = videoComp.transform.GetChild(0).GetChild(1).gameObject;
            Button btnDeleteVideo = videoComp.transform.GetChild(0).GetChild(2).GetComponent<Button>();
            Text txtNameVideoShow = videoComp.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<Text>();
            Text txtNameVideoHide = videoComp.transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<Text>();

            // Event button
            btnShowPanelVideo.GetComponent<Button>().onClick.AddListener(() => ShowVideo(videoUrl, labelId, videoComp));
            btnHidePanelVideo.GetComponent<Button>().onClick.AddListener(() => HideVideo(videoComp));

            YoutubePlayer youtubePlayer = videoComp.transform.GetChild(1).GetChild(0).GetChild(0).GetChild(0).GetComponent<YoutubePlayer>();
            
            if (title == "Intro" && labelId == -1)
            {
                btnDeleteVideo.onClick.AddListener(() => HandlerDeleteVideo(lessonId, title));
            }
            else
            {
                btnDeleteVideo.onClick.AddListener(() => HandlerDeleteVideoLabel(lessonId, Convert.ToInt32(PlayerPrefs.GetString("labelId")), title));
            }
            videoComp.name = String.Format("Video: {0}", labelId);
            txtNameVideoShow.text = String.Format("Video: {0}", Helper.ShortString(title, 15));
            txtNameVideoHide.text = String.Format("Video: {0}", Helper.ShortString(title, 15));
            // Get infor video
            UnityWebRequest webRequest = UnityWebRequest.Get(String.Format(APIUrlConfig.GetLinkVideo, videoUrl));
            videoComp.transform.parent = lessonInfoPanel.transform;
            videoComp.transform.localScale = new Vector3(1f, 1f, 1f);
            yield return webRequest.SendWebRequest();
            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                Toast.ShowCommonToast(webRequest.error, APIUrlConfig.SERVER_ERROR_RESPONSE_CODE); 
            }
            else
            {
                if (webRequest.isDone)
                {
                    InfoLinkVideo dataResp = JsonUtility.FromJson<InfoLinkVideo>(webRequest.downloadHandler.text); 
                    videoComp.transform.GetChild(1).GetChild(0).GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().text = dataResp.title.ToLower();
                    youtubePlayer.youtubeUrl = videoUrl;
                }
            }   
        }

        public void HideMedia()
        {
            if (PlayerPrefs.GetString("VideoId") != null)
            {
                foreach(Transform item in contentMedia)
                {
                    if(item.name == String.Format("Video: {0}", PlayerPrefs.GetString("VideoId")))
                    {
                        HideVideo(item.gameObject);
                    }
                }
            }

            if (PlayerPrefs.GetString("AudioId") != null)
            {
                foreach(Transform item in contentMedia)
                {
                    if(item.name == String.Format("Audio: {0}", PlayerPrefs.GetString("AudioId")))
                    {
                        HideAudio(item.gameObject);
                    }
                }
            }
        }

        public void ShowVideo(string videoUrl, int labelId, GameObject videoComp)
        {
            HideMedia();
            GameObject btnShowPanelVideo = videoComp.transform.GetChild(0).GetChild(0).gameObject;
            GameObject btnHidePanelVideo = videoComp.transform.GetChild(0).GetChild(1).gameObject;
            GameObject panelVideo = videoComp.transform.GetChild(1).gameObject;
            btnShowPanelVideo.SetActive(false);
            btnHidePanelVideo.SetActive(true);
            panelVideo.SetActive(true);
            PlayerPrefs.SetString("VideoId", labelId.ToString());

            // Video player
            panelVideo.GetComponent<LoadVideoManager>().ShowVideo();
        }

        public void HideVideo(GameObject videoComp)
        {
            GameObject btnShowPanelVideo = videoComp.transform.GetChild(0).GetChild(0).gameObject;
            GameObject btnHidePanelVideo = videoComp.transform.GetChild(0).GetChild(1).gameObject;
            GameObject panelVideo = videoComp.transform.GetChild(1).gameObject;
            btnShowPanelVideo.SetActive(true);
            panelVideo.SetActive(false);
            btnHidePanelVideo.SetActive(false);
        }

        public void ShowAudio(string audioUrl, int labelId, GameObject audioComp)
        {
            HideMedia();
            GameObject btnShowPanelAudio = audioComp.transform.GetChild(0).GetChild(0).gameObject;
            GameObject btnHidePanelAudio = audioComp.transform.GetChild(0).GetChild(1).gameObject;
            GameObject panelAudio = audioComp.transform.GetChild(1).gameObject;
            btnShowPanelAudio.SetActive(false);
            btnHidePanelAudio.SetActive(true);
            panelAudio.SetActive(true);
            PlayerPrefs.SetString("AudioId", labelId.ToString());

            // Audio display
            panelAudio.GetComponent<AudioManager1>().DisplayAudio(true, audioComp);
        }

        public void HideAudio(GameObject audioComp)
        {
            GameObject btnShowPanelAudio = audioComp.transform.GetChild(0).GetChild(0).gameObject;
            GameObject btnHidePanelAudio = audioComp.transform.GetChild(0).GetChild(1).gameObject;
            GameObject panelAudio = audioComp.transform.GetChild(1).gameObject;
            btnShowPanelAudio.SetActive(true);
            panelAudio.SetActive(false);
            btnHidePanelAudio.SetActive(false);
        }

        public InfoLinkVideo GetVideoInfo(string link)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(String.Format(APIUrlConfig.GetLinkVideo, link)); 
            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            jsonResponse = reader.ReadToEnd();
            return JsonUtility.FromJson<InfoLinkVideo>(jsonResponse); 
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: Handle delete audio(audioLesson, audioLabel) in Panel
        /// </summary>
        void HandlerDeleteAudio(int lessonId, string lessonTitle)
        {
            panelPopUpDeleteActions.transform.GetChild(1).GetChild(2).GetChild(0).GetComponent<Text>().text = Helper.ShortString(lessonTitle, 10) + "?";
            panelPopUpDeleteActions.SetActive(true);
            btnDeleteActions = GameObject.Find("BtnDeleteActions").GetComponent<Button>();
            btnDeleteActions.onClick.AddListener(() => DeleteAudioLesson(lessonId));
        }

        void HandlerDeleteAudioLabel(int lessonId, int labelId, string labelTitle)
        {
            panelPopUpDeleteActions.transform.GetChild(1).GetChild(2).GetChild(0).GetComponent<Text>().text = Helper.ShortString(labelTitle, 10) + "?";
            panelPopUpDeleteActions.SetActive(true);
            btnDeleteActions = GameObject.Find("BtnDeleteActions").GetComponent<Button>();
            btnDeleteActions.onClick.AddListener(() => DeleteAudioLabel(lessonId, labelId));
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: Handle delete video(videoLesson, videoLabel) in Panel
        /// </summary>
        void HandlerDeleteVideo(int lessonId, string lessonTitle)
        {
            panelPopUpDeleteActionsVideo.transform.GetChild(1).GetChild(2).GetChild(0).GetComponent<Text>().text = Helper.ShortString(lessonTitle, 10) + "?";
            panelPopUpDeleteActionsVideo.SetActive(true);
            btnDeleteActionsVideo = GameObject.Find("BtnDeleteActionsVideo").GetComponent<Button>();
            btnDeleteActionsVideo.onClick.AddListener(() => DeleteVideoLesson(lessonId));
        }

        void HandlerDeleteVideoLabel(int lessonId, int labelId, string labelTitle)
        {
            panelPopUpDeleteActionsVideo.transform.GetChild(1).GetChild(2).GetChild(0).GetComponent<Text>().text = Helper.ShortString(labelTitle, 10) + "?";
            panelPopUpDeleteActionsVideo.SetActive(true);
            btnDeleteActionsVideo = GameObject.Find("BtnDeleteActionsVideo").GetComponent<Button>();
            btnDeleteActionsVideo.onClick.AddListener(() => DeleteVideoLabel(lessonId, labelId));
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: Delete audio(audioLesson, audioLabel) in Panel
        /// </summary>
        public async void DeleteAudioLesson(int lessonId)
        {
            spinner.SetActive(true);
            try
            {
                panelPopUpDeleteActions.SetActive(false);
                string url = String.Format(APIUrlConfig.DELETE_AUDIO_LESSON, lessonId);
                APIResponse<string> deleteAudioLessonResponse = await UnityHttpClient.CallAPI<string>(url, UnityWebRequest.kHttpVerbDELETE);
                if (deleteAudioLessonResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
                {
                    UpdateLessonInforPannel(lessonId);
                    spinner.SetActive(false);
                    if (StaticLesson.Audio == "")
                    {
                        txtShowListItem.SetActive(true);
                    }
                }
            }
            catch (Exception e)
            {
                spinner.SetActive(false);
            }
        }

        public async void DeleteAudioLabel (int lessonId, int labelId)
        {
            spinner.SetActive(true);
            try
            {
                panelPopUpDeleteActions.SetActive(false);
                string url = String.Format(APIUrlConfig.DELETE_AUDIO_LABEL, labelId);
                APIResponse<string> deleteAudioLabelResponse = await UnityHttpClient.CallAPI<string>(url, UnityWebRequest.kHttpVerbDELETE);
                if (deleteAudioLabelResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
                {
                    UpdateLessonInforPannel(lessonId);
                    spinner.SetActive(false);
                }
            }
            catch (Exception e)
            {
                spinner.SetActive(false);
            }
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: Delete video(videoLesson, videoLabel) in Panel
        /// </summary>
        public async void DeleteVideoLesson(int lessonId)
        {
            spinner.SetActive(true);
            try
            {
                panelPopUpDeleteActions.SetActive(false);
                string url = String.Format(APIUrlConfig.DELETE_VIDEO_LESSON, lessonId);
                APIResponse<string> deleteVideoLessonResponse = await UnityHttpClient.CallAPI<string>(url, UnityWebRequest.kHttpVerbDELETE);
                if (deleteVideoLessonResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
                {
                    UpdateLessonInforPannel(lessonId);
                    spinner.SetActive(false);
                    panelPopUpDeleteActionsVideo.SetActive(false);
                    if (StaticLesson.Video == "")
                    {
                        txtShowListItem.SetActive(true);
                    }
                }
            }
            catch (Exception e)
            {
                spinner.SetActive(false);
            }
        }

        public async void DeleteVideoLabel(int lessonId, int labelId)
        {
            spinner.SetActive(true);
            try
            {
                panelPopUpDeleteActions.SetActive(false);
                string url = String.Format(APIUrlConfig.DELETE_VIDEO_LABEL, labelId);
                APIResponse<string> deleteVideoLabelResponse = await UnityHttpClient.CallAPI<string>(url, UnityWebRequest.kHttpVerbDELETE);
                if (deleteVideoLabelResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
                {
                    UpdateLessonInforPannel(lessonId);
                    spinner.SetActive(false);
                    panelPopUpDeleteActionsVideo.SetActive(false);
                }
            }
            catch (Exception e)
            {
                spinner.SetActive(false);
            }
        }
    }
}

