using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System; 

namespace BuildLesson
{
    public class InteractionUI : MonoBehaviour
    {
        private static InteractionUI instance; 
        public static InteractionUI Instance
        {
            get
            {
                if (instance = null)
                {
                    instance = FindObjectOfType<InteractionUI>(); 
                }
                return instance;
            }
        }
        public Button btnShowExitPopup; 
        public GameObject popupExit; 
        public GameObject btnExitPopup; 
        public GameObject btnExitLesson; 
        public GameObject btnCancelExitLesson; 
        public Button btnEditLeft;
        public Button btnAddMediaLeft;
        public Button btnDeleteLeft;
        public Button btnEditRight;
        public Button btnAddMediaRight;
        public Button btnDeleteRight;
        public TMP_InputField inputFieldLeft;
        public TMP_InputField inputFieldRight;
        public Button btnAddAudioLabel;
        public Button btnAddVideoLabel;
        public GameObject panelAddActivities;
        public GameObject panelPopUpDeleteLabel;
        public Button btnDeleteLabel;
        public Button btnExitPopupDeleteLabel;
        public Button btnCancelDeleteLabel;
        public Button btnCancel;
        void OnEnable()
        {
            BackOrLeaveApp.onBackPressed += HandlerShowExitPopup;
        }
        
        void OnDisable()
        {
            BackOrLeaveApp.onBackPressed -= HandlerShowExitPopup;
        }
        void Start()
        {
            SetActions(); 
            InitLayoutScreen();
        }

        void InitLayoutScreen()
        {
            Screen.orientation = ScreenOrientation.LandscapeLeft;
            StatusBarManager.statusBarState = StatusBarManager.States.Hidden;
            StatusBarManager.navigationBarState = StatusBarManager.States.Hidden;
        }

        void SetActions()
        {
            btnShowExitPopup.GetComponent<Button>().onClick.AddListener(HandlerShowExitPopup);
            btnExitPopup.GetComponent<Button>().onClick.AddListener(HandlerExitPopup);
            btnExitLesson.GetComponent<Button>().onClick.AddListener(HandlerExitLesson);
            btnCancelExitLesson.GetComponent<Button>().onClick.AddListener(HandlerExitPopup);
            btnEditLeft.onClick.AddListener(delegate { HandleEditNameLabel(inputFieldLeft); });
            btnEditRight.onClick.AddListener(delegate { HandleEditNameLabel(inputFieldRight); });
            btnAddMediaLeft.onClick.AddListener(delegate { ShowPanelAddMedia(true); });
            btnAddMediaRight.onClick.AddListener(delegate { ShowPanelAddMedia(true); });
            btnDeleteLeft.onClick.AddListener(ShowPopUpDeleteLabel);
            btnDeleteRight.onClick.AddListener(ShowPopUpDeleteLabel);
            btnDeleteLabel.onClick.AddListener(DeleteLabel); 
            btnExitPopupDeleteLabel.onClick.AddListener(ExitPopupDeleteLabel);
            btnCancelDeleteLabel.onClick.AddListener(ExitPopupDeleteLabel);
            btnCancel.onClick.AddListener(delegate { ShowPanelAddMedia(false); });
            btnAddAudioLabel.onClick.AddListener(AddAudioLabel);
            btnAddVideoLabel.onClick.AddListener(AddVideoLabel);
        }

        void HandlerShowExitPopup()
        {
            popupExit.SetActive(true);
        }

        void HandlerExitPopup()
        {
            popupExit.SetActive(false);
        }

        void HandlerExitLesson()
        {
            // BackOrLeaveApp.Instance.BackToPreviousScene(SceneManager.GetActiveScene().name);
            SceneManager.LoadScene(SceneConfig.lesson_edit);
        }

        public void HandleEditNameLabel(TMP_InputField inputField)
        {
            inputField.Select();
            LabelManagerBuildLesson.Instance.isTyping = true;
            inputField.onEndEdit.AddListener(delegate { LabelManagerBuildLesson.Instance.OnEndEditLabel(inputField); } ); 
        }
        public void ShowPanelAddMedia(bool isShowPanelMedia)
        {
            panelAddActivities.SetActive(isShowPanelMedia);
        }

        public void ShowPopUpDeleteLabel()
        {
            panelPopUpDeleteLabel.SetActive(true);
        }

        public void DeleteLabel()
        {
            panelPopUpDeleteLabel.SetActive(false);
            LabelManagerBuildLesson.Instance.ConfirmDeleteLabel();
        }

         void AddAudioLabel()
        {
            panelAddActivities.SetActive(false);
            ListItemsManager.Instance.panelAddAudio.SetActive(true);
            ListItemsManager.Instance.panelAddAudio.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = PlayerPrefs.GetString(StringConfig.labelName);
        }
        void AddVideoLabel()
        {
            panelAddActivities.SetActive(false);
            ListItemsManager.Instance.panelAddVideo.SetActive(true);
            ListItemsManager.Instance.panelAddVideo.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = PlayerPrefs.GetString(StringConfig.labelName);
        }

        void ExitPopupDeleteLabel()
        {
            panelPopUpDeleteLabel.SetActive(false);
        }
    }
}
