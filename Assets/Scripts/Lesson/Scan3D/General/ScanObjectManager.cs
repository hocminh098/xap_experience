using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanObjectManager : MonoBehaviour
{
    public static ScanObjectManager Instance;
    [SerializeField] private string taskUuid;
    public string TaskUuid 
    { 
        get 
        { 
            return taskUuid; 
        } 
        set 
        { 
            taskUuid = value; 
        } 
    }

    void Awake()
    {
        ScanObjectManager[] objs = FindObjectsOfType<ScanObjectManager>();
        if(Instance == null)
        {
            Instance = this;
        }

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }
}
