using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelController : MonoBehaviour
{
    #if UNITY_ANDROID
    //Start Rotation
    private float rotationSpeed = 0.5f;
    private Camera cam;
    private Vector2 camPos2D;
    //End Rotation

    //Start Scale
    private float initialDistance;
    private Vector3 initialScale;
    private float factor;
    //End Scale
#endif
#if UNITY_EDITOR
    private float horizontalSpeed = 2.0F;
    private float verticalSpeed = 2.0F;
    private float minScale = 0.1f;
    private float maxScale = 3f;
    private float tempScale;
#endif

    void Start()
    {
#if UNITY_ANDROID
        cam = Camera.main;
        camPos2D = new Vector2(cam.transform.position.x, cam.transform.position.y);
#endif
    }

    void Update()
    {
#if UNITY_ANDROID
        foreach (Touch touch in Input.touches)
        {
            if (Input.touchCount == 2)
            {
                var touchZero = Input.GetTouch(0);
                var touchOne = Input.GetTouch(1);

                if (touchZero.phase == TouchPhase.Ended || touchZero.phase == TouchPhase.Canceled ||
                    touchOne.phase == TouchPhase.Ended || touchOne.phase == TouchPhase.Canceled)
                {
                    return;
                }

                if (touchZero.phase == TouchPhase.Began || touchOne.phase == TouchPhase.Began)
                {
                    initialDistance = Vector2.Distance(touchZero.position, touchOne.position);
                    initialScale = gameObject.transform.localScale;
                }
                else
                {
                    var currentDistance = Vector2.Distance(touchZero.position, touchOne.position);

                    if (Mathf.Approximately(initialDistance, 0))
                    {
                        return;
                    }

                    factor = currentDistance / initialDistance;
                    gameObject.transform.localScale = initialScale * factor;
                }
            }
            else
            {
                if (Input.touchCount != 1)
                {
                    return;
                }
                if (touch.phase == TouchPhase.Began)
                {
                }
                else if (touch.phase == TouchPhase.Moved)
                {
                    gameObject.transform.Rotate(touch.deltaPosition.y * rotationSpeed, -touch.deltaPosition.x * rotationSpeed, 0, Space.World);
                }
                else if (touch.phase == TouchPhase.Ended)
                {
                }
            }

        }
#endif
#if UNITY_EDITOR
        //Rotate unity editor
        float h = horizontalSpeed * Input.GetAxis("Mouse X");
        float v = verticalSpeed * Input.GetAxis("Mouse Y");
        transform.Rotate(v, h, 0);
        //Scale unity editor
        tempScale = transform.localScale.x;
        tempScale += Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * 20f;
        tempScale = Mathf.Max(minScale, tempScale);
        tempScale = Mathf.Min(maxScale, tempScale);
        transform.localScale = new Vector3(tempScale, tempScale, tempScale);
#endif
    }
    
}
