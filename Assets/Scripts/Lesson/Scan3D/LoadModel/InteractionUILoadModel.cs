using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InteractionUILoadModel : MonoBehaviour
{
    public GameObject waitingScreen;
    public GameObject backToLessonDetailEdit;
    public GameObject panelConfirm;
    public GameObject btnExitPopUp;
    public GameObject btnDiscard;

    void Start()
    {
        InitUI();
        SetActions();
    }

    void InitUI()
    {
        waitingScreen.SetActive(false);
    }

    void SetActions()
    {
        waitingScreen.SetActive(false);
        backToLessonDetailEdit.GetComponent<Button>().onClick.AddListener(HandlerBackToLessonDetailEdit);
        btnExitPopUp.GetComponent<Button>().onClick.AddListener(HandlerExitPopUpConfirm);
        btnDiscard.GetComponent<Button>().onClick.AddListener(DiscardButton);
    }
    void HandlerBackToLessonDetailEdit()
    {
        panelConfirm.SetActive(true);
    }
    void HandlerExitPopUpConfirm()
    {
        panelConfirm.SetActive(false);
    }
    void DiscardButton()
    {
        SceneManager.LoadScene(SceneConfig.createLesson_main); 
    }
}
