using System.IO;
using UnityEngine;
using TriLibCore;
using TMPro;
using UnityEngine.UI;
using System;
using EasyUI.Toast;
using System.Threading.Tasks;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEditor;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using TriLibCore.Utils;
using TriLibCore.Mappers;
using TriLibCore.General;
using static UploadModel;

public class LoadModelController : MonoBehaviour
{
    private GameObject objectClone;
    public GameObject loadingScreen;
    [SerializeField] private GameObject loadingUI;
    [SerializeField] private Text loadingText;
    [SerializeField] private Image processImage;
    [SerializeField] private Text processTypeText;
    [SerializeField] private Button btnExitScanModel;
    [SerializeField] private Button btnSaveScanModel;
    private bool isCallAPI = false;
    public GameObject uiCoat;
    public GameObject panelConfirm;
    public Button btnSaveConfirmBtn;

    private string ModelURL;
    public static int idModel;

    public float downloadingModelProcess = 0;
    public bool isFinishDownloading = false;
    public bool isDownloadingSuccess = true;
    public float loadingFromLocalProcess = 0;
    private string modelPathInLocalSource;

    void Awake()
    {
        #if UNITY_ANDROID
            ModelURL = "https://reconstruction.xrcommunity.org/task/" + ScanObjectManager.Instance.TaskUuid + "/download/texture_model.zip";
        #endif
        // #if UNITY_EDITOR
        //     ModelURL = "https://reconstruction.xrcommunity.org/task/f5420296-382c-4280-a72c-f5a89ff959b4/download/texture_model.zip";
        // #endif
    }

    private void Start()
    {
        loadingScreen.SetActive(false);
        btnExitScanModel.onClick.AddListener(QuitApp);
        loadingUI.gameObject.SetActive(true);
        uiCoat.SetActive(false);
        Debug.Log("model url: " + ModelURL);
        LoadObjectAtRunTime(ModelURL);
        SetActions();
    }

    private void Update()
    {
        if (isCallAPI == true && UnityHttpClient.processAPI*2f*100f < 100)
        {
            loadingUI.SetActive(true);
            processImage.fillAmount = UnityHttpClient.processAPI * 2f;
            loadingText.text = $"{(processImage.fillAmount*100f):N0}%";
            processTypeText.gameObject.SetActive(false);
        }
    }
    
    void SetActions()
    {
        btnSaveScanModel.onClick.AddListener(SaveScanModel);
        btnSaveConfirmBtn.onClick.AddListener(SaveScanModel);
    }

    void SaveScanModel()
    {
        Debug.Log("Save button clicked: ");
        byte[] fileByte = ReadModelAsByte(modelPathInLocalSource);
        // UploadModelToServer(fileByte, objectURL.Substring(44, 36) + ".zip");
        panelConfirm.SetActive(false);
        UploadModelToServer(fileByte, "xxx.zip");
    }

    public async void LoadObjectAtRunTime(string objectURL)
    {
        btnSaveScanModel.gameObject.SetActive(false);
        Debug.Log("objectURL" + objectURL);
        try
        {
            downloadingModelProcess = 0;
            isFinishDownloading = false;
            isDownloadingSuccess = true;
            loadingFromLocalProcess = 0;
         
            // string modelName = objectURL.Substring(44, 36) + ".zip";
            string modelName = "xxx.zip";
            // int lastIndex = objectURL.LastIndexOf("/", StringComparison.Ordinal);
            // string modelName = objectURL.Remove(0, lastIndex + 1);

            modelPathInLocalSource = PathConfig.MEDIA_CACHE + modelName; // path to model fil
            string modelExtension = modelName.Remove(0, modelName.LastIndexOf(".", StringComparison.Ordinal) + 1).ToLower();
            Debug.Log("Model Extension: " + modelExtension);

            // Check if file with uuid.zip is exist
            if (!File.Exists(modelPathInLocalSource))
            {
                // If model was not downloaded (not exist in local), then download it
                await DownloadObjectFromLogicServer(objectURL, modelPathInLocalSource);
            }
            else
            {
                downloadingModelProcess = 1;
                isFinishDownloading = true;
                isDownloadingSuccess = true;
            }
            if (isDownloadingSuccess)
            {
                // Load file from local to unity application
                LoadObjectFromLocal(modelPathInLocalSource, modelExtension);
            }
        }
        catch (Exception exception)
        {
            Debug.Log($"error loading {exception.Message}");
        }
    }

    async Task DownloadObjectFromLogicServer(string objectURL, string modelPathInLocalSource)
    {
        bool isNetworkError = false;
        Debug.Log("objectURL 2:" + objectURL);
        try
        {
            Exception exception = Network.CheckNetWorkToDisplayToast();
            if (exception != null)
            {
                isNetworkError = true;
                throw exception;
            }
            UnityWebRequest www = new UnityWebRequest(objectURL);
            www.downloadHandler = new DownloadHandlerBuffer();
            var operation = www.SendWebRequest();

            while (!operation.isDone)
            {
                downloadingModelProcess = operation.progress;
                await Task.Yield();
            }
            isFinishDownloading = true;
            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                throw new Exception(www.error);
            }
            else
            {
                byte[] fileData = www.downloadHandler.data;
                if (!Directory.Exists(PathConfig.MEDIA_CACHE))
                {
                    Directory.CreateDirectory(PathConfig.MEDIA_CACHE);
                }
                using (var fs = new FileStream(modelPathInLocalSource, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(fileData, 0, fileData.Length);
                    fs.Close();
                }
            }
        }
        catch (Exception exception)
        {
            isFinishDownloading = true;
            isDownloadingSuccess = false;
            if (File.Exists(modelPathInLocalSource))
            {
                File.Delete(modelPathInLocalSource);
            }
            if (isNetworkError)
            {
                SceneNameManager.setPrevScene(SceneManager.GetActiveScene().name);
                Network.CheckNetWorkMoveScence();
            }
            throw exception;
        }
    }

    public void LoadObjectFromLocal(string modelPathInLocalSource, string modelExtension)
    {
        Debug.Log($"LoadObjectFromLocal {modelPathInLocalSource}");
        var assetLoaderOptions = AssetLoader.CreateDefaultLoaderOptions();
        if (modelExtension == ModelConfig.zipExtension)
        {
            AssetLoaderZip.LoadModelFromZipFile(modelPathInLocalSource, OnLoad, OnMaterialsLoad, OnProgress, OnError, null, assetLoaderOptions);
        }
        else
        {
            AssetLoader.LoadModelFromFile(modelPathInLocalSource, OnLoad, OnMaterialsLoad, OnProgress, OnError, null, assetLoaderOptions);
        }
    }

    public byte[] ReadModelAsByte(string modelPathInLocalSource)
    {
        return File.ReadAllBytes(modelPathInLocalSource);
    }

    // Code from UploadModel.cs
    public async Task UploadModelToServer(byte[] fileData, string fileName)
    {
        try 
        {
            btnSaveScanModel.interactable = false;
            isCallAPI = true;
            uiCoat.SetActive(true);
            var form = new WWWForm();
            form.AddBinaryData("model", fileData, fileName);
            APIResponse<ResData[]> importModelResponse = await UnityHttpClient.UploadFileAPI<ResData[]>(APIUrlConfig.Upload3DModel, UnityWebRequest.kHttpVerbPOST, form);
            isCallAPI = false;
            if (importModelResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
            {
                Debug.Log("3d scan: ");
                idModel = importModelResponse.data[0].file_id;
                UploadModel.idModel = idModel;
                ModelStoreManager.InitModelStore(idModel, importModelResponse.data[0].file_path);
                Debug.Log("iModel" + idModel);
                Exception exception = Network.CheckNetWorkToDisplayToast();
                Debug.Log("exception 3d scan:"+ exception);
                if (exception != null) throw exception;
                // BackOrLeaveApp.Instance.AddPreviousScene(SceneManager.GetActiveScene().name, SceneConfig.interactiveModel);
                await Task.Delay(1000);
                btnSaveScanModel.interactable = true;
                SceneManager.LoadScene(SceneConfig.interactiveModel);
            }
            else
            {
                throw new Exception(importModelResponse.message);
            }
        }
        catch (Exception exception)
        {
            Toast.ShowCommonToast(exception.Message, APIUrlConfig.BAD_REQUEST_RESPONSE_CODE); 
        }
    }

    private void OnLoad(AssetLoaderContext assetLoaderContext)
    {
        assetLoaderContext.RootGameObject.tag = "ModelClone";  
        DontDestroyOnLoad(assetLoaderContext.RootGameObject);   
    }

    private void OnMaterialsLoad(AssetLoaderContext assetLoaderContext)
    {

    }

    private void OnProgress(AssetLoaderContext assetLoaderContext, float progress)
    {
        processImage.fillAmount = progress;
        loadingText.text = $"{Mathf.Round(progress*100)} %";
        if (progress >= 1f)
        {
            objectClone = GameObject.Find("xxx");
            // objectClone.gameObject.tag = "Model";
            objectClone.AddComponent<ModelController>();
            objectClone.AddComponent<BoxCollider>();
            loadingUI.gameObject.SetActive(false);
            btnSaveScanModel.gameObject.SetActive(true);
        }
    }

    private void OnError(IContextualizedError obj)
    {
        Debug.Log($"An error occurred while loading your model: {obj.GetInnerException()}");
        loadingText.text = "Task was Fail, please take more image or check your connect !!!";
        btnExitScanModel.gameObject.SetActive(true);
    }
    
    private void QuitApp()
    {
        Application.Quit();
    }
}

