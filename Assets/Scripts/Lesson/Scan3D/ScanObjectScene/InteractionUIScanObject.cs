using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InteractionUIScanObject : MonoBehaviour
{
    public GameObject waitingScreen;
    public GameObject backToLessonDetailEdit;

    void Start()
    {
        InitUI();
        SetActions();
    }

    void InitUI()
    {
        waitingScreen.SetActive(false);
    }

    void SetActions()
    {
        waitingScreen.SetActive(false);
        backToLessonDetailEdit.GetComponent<Button>().onClick.AddListener(HandlerBackToLessonDetailEdit);
    }

    void HandlerBackToLessonDetailEdit()
    {
        SceneManager.LoadScene(SceneConfig.createLesson_main); 
    }
}
