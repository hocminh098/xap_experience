using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class PlacementIndicator : MonoBehaviour
{
    [SerializeField] private ARRaycastManager raycastManager;
    [SerializeField] private GameObject visual;
    private bool visualIsActive;
    public bool VisuaIsActive { get { return visualIsActive; } }

    void Start()
    {
        visual.gameObject.SetActive(false);
        visualIsActive = false;
    }
    
    void Update()
    {
        List<ARRaycastHit> hits = new List<ARRaycastHit>();
        raycastManager.Raycast(new Vector2(Screen.width / 2, Screen.height / 2), hits, TrackableType.PlaneWithinPolygon);
        if (hits.Count > 0)
        {
            transform.position = hits[0].pose.position;
            transform.rotation = hits[0].pose.rotation;
            if (!visual.activeInHierarchy)
            {
                visual.SetActive(true);
                visualIsActive = true;
            }
        }
        else
        {
            if (visual.activeInHierarchy)
            {
                visual.SetActive(false);
                visualIsActive = false;
            }
        }
    }
}
