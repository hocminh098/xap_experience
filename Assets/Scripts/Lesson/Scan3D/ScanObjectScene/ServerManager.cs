using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Threading.Tasks;

public class ServerManager : Singleton<ServerManager>
{
    public delegate void CallBack();
    private TaskNewInit taskInit;
    public TaskNewInit TaskInit { get { return taskInit; } set { taskInit = value; } }
    // Init new Task
    public IEnumerator TaskNewInit(string url, CallBack callBack)
    {
        WWWForm form = new WWWForm();
        using (UnityWebRequest request = UnityWebRequest.Post(url, form))
        {
            yield return request.SendWebRequest();
            request.timeout = 2;
            switch (request.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    Debug.Log("Failed");
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.Log("Failed");
                    break;
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.Log("Failed");
                    break;
                case UnityWebRequest.Result.Success:
                    Debug.Log("Success");
                    callBack();
                    break;
            }
        }
    }
    // Upload image
    public IEnumerator TaskNewUpload(string url, TaskNewInit task, Texture2D texture, CallBack callBack)
    {
        string newURL = url + task.uuid;
        byte[] encodeToPNG;
        encodeToPNG = texture.EncodeToPNG();
        WWWForm form = new WWWForm();
        form.AddField("uuid", task.uuid);
        form.AddBinaryData("images", encodeToPNG);
        using (UnityWebRequest request = UnityWebRequest.Post(newURL, form))
        {
            yield return request.SendWebRequest();
            switch (request.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    break;
                case UnityWebRequest.Result.DataProcessingError:
                    break;
                case UnityWebRequest.Result.Success:
                    callBack();
                    break;
            }
        }
    }
    // Commit Task
    public IEnumerator TaskNewCommit(string url, TaskNewInit task, CallBack callBack)
    {
        string newURL = url + task.uuid;
        WWWForm form = new WWWForm();
        form.AddField("uuid", task.uuid);
        using (UnityWebRequest request = UnityWebRequest.Post(newURL, form))
        {
            yield return request.SendWebRequest();
            switch (request.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    break;
                case UnityWebRequest.Result.DataProcessingError:
                    break;
                case UnityWebRequest.Result.Success:
                    callBack();
                    break;
            }
        }
    }
    // Get task info
    public IEnumerator TaskInfo(string url)
    {
        string newURl = url + taskInit.uuid + "/info";
        using (UnityWebRequest request = UnityWebRequest.Get(newURl))
        {
            yield return request.SendWebRequest();
            switch (request.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    break;
                case UnityWebRequest.Result.DataProcessingError:
                    break;
                case UnityWebRequest.Result.Success:
                    taskInit = JsonUtility.FromJson<TaskNewInit>(request.downloadHandler.text);
                    break;
            }
        }
    }
    // Init task with setting
    public IEnumerator TaskNewInitWithSetting(string URL, string json, CallBack callBack)
    {
        using (UnityWebRequest request = UnityWebRequest.Post(URL, json))
        {
            byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);
            request.uploadHandler = new UploadHandlerRaw(jsonToSend);
            request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/json");

            yield return request.SendWebRequest();

            switch (request.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    break;
                case UnityWebRequest.Result.DataProcessingError:
                    break;
                case UnityWebRequest.Result.Success:
                    taskInit = JsonUtility.FromJson<TaskNewInit>(request.downloadHandler.text);
                    ScanObjectManager.Instance.TaskUuid = taskInit.uuid;
                    callBack();
                    break;
            }
        }
    }
    // Post multiple Image
    public IEnumerator TaskNewUploadMultipleImage(string url, TaskNewInit task, List<ImageWasTaken> listImages, CallBack callBack)
    {
        string newURL = url + task.uuid;
        WWWForm form = new WWWForm();
        form.AddField("uuid", task.uuid);
        foreach (ImageWasTaken image in listImages)
        {
            byte[] imageData = image.visualImage.EncodeToJPG();
            string name = image.imageName;
            form.AddBinaryData("images", imageData, name, "image/png");
        }
        UnityWebRequest request = UnityWebRequest.Post(newURL, form);
        yield return request.SendWebRequest();
        if (request.result == UnityWebRequest.Result.Success)
        {
            callBack();
        }
        else
        {
            
        }
    }  
}

public class ServerURL
{
    public const string InitNewTaskURL = "https://reconstruction.xrcommunity.org/task/new/init";
    public const string TaskNewUpLoad = "https://reconstruction.xrcommunity.org/task/new/upload/";
    public const string TaskNewCommit = "https://reconstruction.xrcommunity.org/task/new/commit/";
    public const string TaskInfo = "https://reconstruction.xrcommunity.org/task/";
    public const string Info = "https://reconstruction.xrcommunity.org/info";
}

[System.Serializable]
public class TaskNewInit
{
    public string uuid;
    public string name;
    public int progress;
}