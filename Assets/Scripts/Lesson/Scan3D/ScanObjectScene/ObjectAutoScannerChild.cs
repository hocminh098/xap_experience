using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectAutoScannerChild : MonoBehaviour
{
    private bool hadTakenPhoto;
    public bool HadTakenPhoto { get { return hadTakenPhoto; } set { hadTakenPhoto = value;} }
    private Vector3 rotationCube;
    public Vector3 RotationCube { get { return rotationCube; } }
    
    private void Start()
    {
        rotationCube = gameObject.transform.eulerAngles;
        hadTakenPhoto = false;
    }
}
