using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;

public class ScanObjectController : MonoBehaviour
{
    [SerializeField] private TextAsset jsonSetting;
    [SerializeField] private GameObject beginTaskUI;
    [SerializeField] private GameObject placeBoxTaskUI;
    [SerializeField] private GameObject takeImageTaskUI;
    [SerializeField] private GameObject handCursor;
    [SerializeField] private ButtonPress btnDeleteBox;
    [SerializeField] private ButtonPress btnNextStep;
    [SerializeField] private ButtonPress btnResetTask;
    [SerializeField] private ButtonPress btnStartModeling;
    [SerializeField] private GameObject modelToInstance;
    [SerializeField] private PlacementIndicator placementIndicator;
    [SerializeField] private List<ImageWasTaken> listImages = new List<ImageWasTaken>();

    [SerializeField] private TMP_Text textPlaceBoxTaskUI;
    [SerializeField] private TMP_Text textTakeImageTaskUI;
    [SerializeField] private float cornerView;
    [SerializeField] private GameObject loadingUI;
    [SerializeField] private Text textPercent;
    [SerializeField] private Image processImage;
    [SerializeField] private Text processTypeText;
    public float downloadingModelProcess = 0;
    
    private bool beginTaskDone = false;
    private bool placeBoxTaskDone = false;
    private bool takeImageTasDone = false;
    private GameObject model;
    private Camera cameraMain;
    private Vector3 rotationCameraMain;
    // Scale object
    private float initialDistance;
    private Vector3 initialScale;
    private bool commited;
    private float delayTime = 3f;
    private float time;
    public ARSession arSession;

    private void Start()
    {
        arSession.Reset();
        loadingUI.gameObject.SetActive(false);
        commited = false;
        placementIndicator = FindObjectOfType<PlacementIndicator>();
        FirstlyOpenScene();
        btnDeleteBox.GetComponent<Button>().onClick.AddListener(DeleteBox);
        btnNextStep.GetComponent<Button>().onClick.AddListener(NextStep);
        btnResetTask.GetComponent<Button>().onClick.AddListener(ResetTask);
        btnStartModeling.GetComponent<Button>().onClick.AddListener(StartModeling);
        cameraMain = Camera.main;
    }

    private void Update()
    {
        if (commited == true)
        {
            time += Time.deltaTime;
            while (time >= delayTime)
            {
                StartCoroutine(ServerManager.Instance.TaskInfo(ServerURL.TaskInfo));
                if (ServerManager.Instance.TaskInit.progress < 100)
                {
                    // textPercent.text = $"Loading: {ServerManager.Instance.TaskInit.progress} %";
                    textPercent.text = $"{ServerManager.Instance.TaskInit.progress} %";
                    // processImage.fillAmount = downloadingModelProcess;
                    processImage.fillAmount = ServerManager.Instance.TaskInit.progress;
                }
                else if (ServerManager.Instance.TaskInit.progress == 100)
                {
                    commited = false;
                    LoadModel();
                }
                time -= delayTime;
            }
        }
        if (model == null)
        {
            switch (placementIndicator.VisuaIsActive)
            {
                case true:
                    textPlaceBoxTaskUI.text = "Touch to place box";
                    break;
                case false:
                    textPlaceBoxTaskUI.text = "On tracking your plane";
                    break;
            }
            foreach (Touch touch in Input.touches)
            {
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        // if (beginTaskDone == false && placeBoxTaskDone == true && takeImageTasDone == false)
                        // {
                        //     InitTask();
                        // }
                        // else if (beginTaskDone == false && placeBoxTaskDone == false && takeImageTasDone == false)
                        if (beginTaskDone == false && placeBoxTaskDone == false && takeImageTasDone == false)
                        {
                            if (btnDeleteBox.ButtonPressed || btnNextStep.ButtonPressed
                            || btnResetTask.ButtonPressed || btnStartModeling.ButtonPressed)
                            {
                                return;
                            }
                            else
                            {
                                TouchToPlaceBox();
                            }
                        }
                        break;
                    case TouchPhase.Moved:
                        break;
                    case TouchPhase.Ended:
                        break;
                }
            }
        }
        else if (model != null)
        {
            if (beginTaskDone == false && placeBoxTaskDone == true && takeImageTasDone == false)
            {
                foreach (Touch touch in Input.touches)
                {
                    switch (touch.phase)
                    {
                        case TouchPhase.Began:
                            InitTask();
                            break;
                        case TouchPhase.Moved:
                            break;
                        case TouchPhase.Ended:
                            break;
                    }

                }
            }
            else if (beginTaskDone == true && placeBoxTaskDone == true && takeImageTasDone == false)
            {
                textPlaceBoxTaskUI.text = "Press next step to take photo";
                Vector3 rayOrigin = new Vector3(0.5f, 0.5f, 0f);
                float rayLength = 500f;
                Ray ray = cameraMain.ViewportPointToRay(rayOrigin);
                Debug.DrawRay(ray.origin, ray.direction * rayLength, Color.red);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit)
            && beginTaskDone == true
            && placeBoxTaskDone == true
            && takeImageTasDone == false)
                {
                    if (hit.transform.GetComponent<ObjectAutoScannerChild>().HadTakenPhoto == false)
                    {
                        rotationCameraMain = cameraMain.transform.eulerAngles;
                        if (rotationCameraMain.x >= hit.transform.GetComponent<ObjectAutoScannerChild>().RotationCube.x - cornerView
                                && rotationCameraMain.x <= hit.transform.GetComponent<ObjectAutoScannerChild>().RotationCube.x + cornerView
                                && rotationCameraMain.y >= hit.transform.GetComponent<ObjectAutoScannerChild>().RotationCube.y - cornerView
                                && rotationCameraMain.y <= hit.transform.GetComponent<ObjectAutoScannerChild>().RotationCube.y + cornerView)
                        {
                            textTakeImageTaskUI.text = "Touch to take image";
                            hit.transform.GetComponent<Renderer>().material.color = new Color(255f, 0f, 0f, 0.2f);
                            foreach (Touch touch in Input.touches)
                            {
                                switch (touch.phase)
                                {
                                    case TouchPhase.Began:
                                        if (beginTaskDone == true && placeBoxTaskDone == true && takeImageTasDone == false)
                                        {
                                            if (btnDeleteBox.ButtonPressed || btnNextStep.ButtonPressed
                                            || btnResetTask.ButtonPressed || btnStartModeling.ButtonPressed)
                                            {
                                                return;
                                            }
                                            else
                                            {
                                                TakeImage();
                                                hit.transform.GetComponent<ObjectAutoScannerChild>().HadTakenPhoto = true;
                                            }
                                        }
                                        break;
                                    case TouchPhase.Moved:
                                        break;
                                    case TouchPhase.Ended:
                                        break;
                                }

                            }
                        }
                    }
                }
                else
                {
                    textTakeImageTaskUI.text = "Target cursor to plates on screen";
                    ObjectAutoScanner.Instance.ResetMaterialChild();
                }
            }

            if (beginTaskDone == false && placeBoxTaskDone == false && takeImageTasDone == false)
            {
                if (Input.touchCount == 2)
                {
                    var touchZero = Input.GetTouch(0);
                    var touchOne = Input.GetTouch(1);
                    if (touchZero.phase == TouchPhase.Ended || touchZero.phase == TouchPhase.Canceled ||
                        touchOne.phase == TouchPhase.Ended || touchOne.phase == TouchPhase.Canceled)
                    {
                        return;
                    }

                    if (touchZero.phase == TouchPhase.Began || touchOne.phase == TouchPhase.Began)
                    {
                        initialDistance = Vector2.Distance(touchZero.position, touchOne.position);
                        initialScale = model.transform.localScale;
                    }
                    else
                    {
                        var currentDistance = Vector2.Distance(touchZero.position, touchOne.position);

                        if (Mathf.Approximately(initialDistance, 0))
                        {
                            return;
                        }

                        var factor = currentDistance / initialDistance;
                        model.transform.localScale = initialScale * factor;
                    }
                }
            }
        }
    }
    
    // UI controller start
    private void FirstlyOpenScene()
    {
        beginTaskUI.gameObject.SetActive(false);
        placeBoxTaskUI.gameObject.SetActive(true);
        takeImageTaskUI.gameObject.SetActive(false);
        handCursor.gameObject.SetActive(false);
        beginTaskDone = false;
        placeBoxTaskDone = false;
        takeImageTasDone = false;
        btnNextStep.gameObject.SetActive(false);
        btnDeleteBox.gameObject.SetActive(false);
        if (model != null)
        {
            Destroy(model.gameObject);
        }
        if (listImages.Count != 0)
        {
            listImages.Clear();
        }
    }

    private void BeginTask()
    {
        beginTaskUI.gameObject.SetActive(false);
        placeBoxTaskUI.gameObject.SetActive(false);
        takeImageTaskUI.gameObject.SetActive(true);
        handCursor.gameObject.SetActive(true);
        beginTaskDone = true;
        placeBoxTaskDone = true;
        takeImageTasDone = false;
    }

    private void PlaceBoxConfirmTask()
    {
        beginTaskUI.gameObject.SetActive(true);
        placeBoxTaskUI.gameObject.SetActive(false);
        takeImageTaskUI.gameObject.SetActive(false);
        handCursor.gameObject.SetActive(false);
        beginTaskDone = false;
        placeBoxTaskDone = true;
        takeImageTasDone = false;
    }

    private void TakingImageTask()
    {
        beginTaskUI.gameObject.SetActive(false);
        placeBoxTaskUI.gameObject.SetActive(false);
        takeImageTaskUI.gameObject.SetActive(false);
        handCursor.gameObject.SetActive(false);
        if (model != null)
        {
            model.gameObject.SetActive(false);
        }
    }

    private void HadTakenImageTask()
    {
        beginTaskUI.gameObject.SetActive(false);
        placeBoxTaskUI.gameObject.SetActive(false);
        takeImageTaskUI.gameObject.SetActive(true);
        handCursor.gameObject.SetActive(true);
        if (model != null)
        {
            model.gameObject.SetActive(true);
        }
    }

    // UI Controller end
    private void TouchToPlaceBox()
    {
        if (placementIndicator.VisuaIsActive == true)
        {
            model = Instantiate(modelToInstance, placementIndicator.transform.position, placementIndicator.transform.rotation);
        }
        placementIndicator.gameObject.SetActive(false);
        btnNextStep.gameObject.SetActive(true);
        btnDeleteBox.gameObject.SetActive(true);
    }

    private void DeleteBox()
    {
        if (model != null)
        {
            Destroy(model.gameObject);
        }
        else
        {
            return;
        }
        placementIndicator.gameObject.SetActive(true);
        btnNextStep.gameObject.SetActive(false);
        btnDeleteBox.gameObject.SetActive(false);
    }

    private void NextStep()
    {
        if (model == null)
        {
            return;
        }
        PlaceBoxConfirmTask();
        btnNextStep.gameObject.SetActive(false);
        btnDeleteBox.gameObject.SetActive(false);
    }

    private void ResetTask()
    {
        if (model == null)
        {
            return;
        }
        FirstlyOpenScene();
        placementIndicator.gameObject.SetActive(true);
    }

    private void StartModeling()
    {
        if (listImages.Count == 0)
        {
            return;
        }
        loadingUI.gameObject.SetActive(true);
        textPercent.text = $"{ServerManager.Instance.TaskInit.progress} %";
        processImage.fillAmount = ServerManager.Instance.TaskInit.progress;
        // processImage.gameObject.SetActive(true);
        handCursor.gameObject.SetActive(false);
        btnResetTask.gameObject.GetComponent<Button>().interactable = false;
        btnStartModeling.gameObject.GetComponent<Button>().interactable = false;
        StartCoroutine(ServerManager.Instance.TaskNewUploadMultipleImage(ServerURL.TaskNewUpLoad, ServerManager.Instance.TaskInit, listImages, CommitTask));
    }

    private void TakeImage()
    {
        TakingImageTask();
        StartCoroutine(ScreenShot());
    }

    private IEnumerator ScreenShot()
    {
        yield return new WaitForEndOfFrame();
        Texture2D screenTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        Rect regionToRead = new Rect(0f, 0f, Screen.width, Screen.height);
        screenTexture.ReadPixels(regionToRead, 0, 0, false);
        screenTexture.Apply();
        string name = "ScreenShot_ScanApp" + System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".png";
        ImageWasTaken newImage = new ImageWasTaken();
        newImage.imageName = name;
        newImage.visualImage = screenTexture;
        listImages.Add(newImage);
        HadTakenImageTask();
    }

    private void CheckStateUI()
    {
        // Do something
    }
    private void InitTask()
    {
        StartCoroutine(ServerManager.Instance.TaskNewInitWithSetting(ServerURL.InitNewTaskURL, jsonSetting.text, BeginTask));
    }
    private void CommitTask()
    {
        StartCoroutine(ServerManager.Instance.TaskNewCommit(ServerURL.TaskNewCommit, ServerManager.Instance.TaskInit, IsCommited));
    }
    private void IsCommited()
    {
        loadingUI.gameObject.SetActive(true);
        commited = true;
    }
    private void LoadModel()
    {
        SceneManager.LoadScene(SceneConfig.load3DModel);
    }
}

[System.Serializable]
public class ImageWasTaken
{
    public string imageName;
    public Texture2D visualImage;
}

