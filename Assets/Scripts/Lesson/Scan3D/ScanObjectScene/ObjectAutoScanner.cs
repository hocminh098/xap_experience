using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectAutoScanner : MonoBehaviour
{
    public static ObjectAutoScanner Instance;
    private bool hadTakenPhoto;
    public bool HadTakenPhoto { get { return hadTakenPhoto; } set { hadTakenPhoto = value; } }
    [SerializeField] private Material cubeChildMaterialClassic;
    [SerializeField] List<ObjectAutoScannerChild> cubeChild;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
    
    public void ResetMaterialChild()
    {
        foreach (ObjectAutoScannerChild obj in cubeChild)
        {
            if (obj.HadTakenPhoto == false)
            {
                obj.GetComponent<Renderer>().material = cubeChildMaterialClassic;
            }
            else
            {
                continue;
            }
        }
    }
}
