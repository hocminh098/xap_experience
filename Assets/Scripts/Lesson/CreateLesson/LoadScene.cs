using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using System; 
using UnityEngine.SceneManagement; 
using UnityEngine.Networking; 
using System.Text;
using EasyUI.Toast;
using System.Threading.Tasks;
using AdvancedInputFieldPlugin;

namespace CreateLesson
{
    public class LoadScene : MonoBehaviour
    {
        public GameObject spinner;
        public List3DModel[] myData;
        public List3DModel currentModel; 
        public GameObject createLesssonFormComponent;
        public AdvancedInputField lessonTitleInputField;
        public GameObject txtLessonTitleNotification;
        public AdvancedInputField lessonObjectiveInputField;
        public GameObject txtLessonObjectiveNotification;
        public Button buildLessonBtn;
        public List<Button> resetInputFieldBtns;
        private ListOrgans listOrgans;
        public Dropdown dropdown;
        public GameObject categoryComponent;
        public GameObject txtCategoryNotification;
        private List<Dropdown.OptionData> option_ = new List<Dropdown.OptionData>();
        private int indexItemDropdown = 1;
        public GameObject panelConfirm;
        public Button saveConfirmBtn;
        private bool isValidLessonTitle = true;
        private bool isValidLessonObjective = true;

        void Start()
        {
            StartCoroutine(UpdateModelStoreManager());
            createLesssonFormComponent.transform.GetChild(0).GetChild(1).GetComponent<AdvancedInputField>().Text = ModelStoreManager.modelName;
            InitScreen(); 
            InitUI();
            InitDropDown();
            InitEvents();
        }
        void Update()
        {
            if (lessonTitleInputField.Selected == true && lessonTitleInputField.Text != "" && isValidLessonTitle == false)
            {
                lessonTitleInputField.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldWarning);
            }
            else if (lessonTitleInputField.Selected == true && lessonTitleInputField.Text == "" && isValidLessonTitle == true)
            {
                lessonTitleInputField.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldBorder);
            }
            else if (lessonTitleInputField.Selected == false && lessonTitleInputField.Text != "" && isValidLessonTitle == true)
            {
                lessonTitleInputField.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldNormal);
            }

            if (lessonObjectiveInputField.Selected == true && lessonObjectiveInputField.Text != "" && isValidLessonObjective == false)
            {
                lessonObjectiveInputField.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.border_red);
            }
            else if (lessonObjectiveInputField.Selected == true && lessonObjectiveInputField.Text == "" && isValidLessonObjective == true)
            {
                lessonObjectiveInputField.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.border_grey);
            }
            else if (lessonObjectiveInputField.Selected == false && lessonObjectiveInputField.Text != "" && isValidLessonObjective == true)
            {
                lessonObjectiveInputField.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.border_normal);
            }
        }

        void OnEnable()
        {
            NativeKeyboardManager.AddKeyboardHeightChangedListener(OnKeyboardHeightChanged); 
        }

        void OnDisable()
        {
            NativeKeyboardManager.RemoveKeyboardHeightChangedListener(OnKeyboardHeightChanged); 
        }

        void InitScreen()
        {
            Screen.orientation = ScreenOrientation.Portrait;
            StatusBarManager.statusBarState = StatusBarManager.States.TranslucentOverContent;
            StatusBarManager.navigationBarState = StatusBarManager.States.Hidden;
        }

        void InitUI()
        {
            HideAllNotifications();
            spinner.SetActive(false);
        }

        void HideAllNotifications()
        {
            txtLessonTitleNotification.SetActive(false);
            txtLessonObjectiveNotification.SetActive(false);
            txtCategoryNotification.SetActive(false);
        }

        void InitEvents()
        { 
            lessonTitleInputField.OnValueChanged.AddListener(CheckLessonTitle);
            lessonObjectiveInputField.OnValueChanged.AddListener(CheckLessonObjective);
            buildLessonBtn.onClick.AddListener(CreateLessonInfo);
            saveConfirmBtn.onClick.AddListener(CreateLessonInfo);
            lessonTitleInputField.OnBeginEdit.AddListener(OnLessonTitleInputFieldBeginEdit);
            lessonObjectiveInputField.OnBeginEdit.AddListener(OnLessonObjectiveInputFieldBeginEdit);
            lessonObjectiveInputField.OnEndEdit.AddListener(OnLessonObjectiveInputFieldEndEdit);
            // categoryComponent.OnEndEdit.AddListener(OnCategoryComponentBeginEdit);
            dropdown.onValueChanged.AddListener(delegate {
                IsValidCategoryCreateLesson(dropdown);
            });
        }

        public void OnLessonObjectiveInputFieldEndEdit(string arg0, EndEditReason reason)
        {
            if (reason == EndEditReason.KEYBOARD_DONE)
            {
                CreateLessonInfo();
            }
        }

        void CheckLessonTitle(string data)
        {
            IsValidLessonTitle(data);
        }
        void CheckLessonObjective(string data)
        {
            IsValidLessonObjective(data);
        }
       
        bool IsValidLessonTitle(string data)
        {
            if(string.IsNullOrEmpty(data))
            {
                txtLessonTitleNotification.GetComponent<Text>().text = AuthenConfig.emptyValue;
                isValidLessonTitle = false;
                ChangeUIStatus(lessonTitleInputField, txtLessonTitleNotification, true);
                lessonTitleInputField.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldWarning);
                buildLessonBtn.interactable = false;
                return false;
            }
            else
            {
                buildLessonBtn.interactable = true;
                isValidLessonTitle = true;
                ChangeUIStatus(lessonTitleInputField, txtLessonTitleNotification, false);
                return true;
            }
        }
        bool IsValidLessonObjective(string data)
        {
            if(string.IsNullOrEmpty(data))
            {
                txtLessonObjectiveNotification.GetComponent<Text>().text = AuthenConfig.emptyValue;
                isValidLessonObjective = false;
                ChangeUIStatusObjective(lessonObjectiveInputField, txtLessonObjectiveNotification, true);
                RefreshLayoutGroupsForLongNotifications(createLesssonFormComponent);
                buildLessonBtn.interactable = false;
                return false;
            }
            else
            {
                buildLessonBtn.interactable = true;
                isValidLessonObjective = true;
                ChangeUIStatusObjective(lessonObjectiveInputField, txtLessonObjectiveNotification, false);
                RefreshLayoutGroupsForLongNotifications(createLesssonFormComponent);
                return true;
            }
        }

        bool IsValidCategoryCreateLesson(Dropdown dropdown)
        {
            if (dropdown.value == 0)
            {
                Debug.Log("Not allow"); 
                txtCategoryNotification.GetComponent<Text>().text = AuthenConfig.emptyValue;
                ChangeUINotification(dropdown, txtCategoryNotification, true);
                buildLessonBtn.interactable = false;
                return false;
            }
            else
            {
                buildLessonBtn.interactable = true;
                ChangeUINotification(dropdown, txtCategoryNotification, false);
                return true;
            }
        }

        public void RefreshLayoutGroupsForLongNotifications(GameObject root)
        {
            foreach (var layoutGroup in root.GetComponentsInChildren<LayoutGroup>())
            {
                LayoutRebuilder.ForceRebuildLayoutImmediate(layoutGroup.GetComponent<RectTransform>());
            }
        }

        void OnLessonTitleInputFieldBeginEdit(BeginEditReason reason)
        {
            TransformCreateLessonForm(true);
        }

        void OnCategoryComponentBeginEdit(BeginEditReason reason)
        {
            TransformCreateLessonForm(true);
            lessonTitleInputField.transform.parent.gameObject.SetActive(false);
        }

        void OnLessonObjectiveInputFieldBeginEdit(BeginEditReason reason)
        {
            TransformCreateLessonForm(true);
            lessonTitleInputField.transform.parent.gameObject.SetActive(false);
            // categoryComponent.transform.parent.gameObject.SetActive(false);
        }

        private void ChangeUIStatus(AdvancedInputField input, GameObject warning, bool status)
        {
            warning.SetActive(status); 
            if(status)
            {
                input.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldWarning);
            }
            else
            {
                input.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldBorder);
            }
        }

        private void ChangeUIStatusObjective(AdvancedInputField input, GameObject warning, bool status)
        {
            warning.SetActive(status); 
            if(status)
            {
                input.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.border_red);
            }
            else
            {
                input.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.border_grey);
            }
        }

        private void ChangeUINotification(Dropdown dropdown, GameObject warning, bool status)
        {
            warning.SetActive(status); 
            if(status)
            {
                dropdown.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldWarning);
            }
            else
            {
                dropdown.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldNormal);
            }
        }

        public void OnKeyboardHeightChanged(int keyboardHeight)
        {
            if (keyboardHeight == 0)
            {
                if (lessonTitleInputField.Selected || lessonObjectiveInputField.Selected)
                {
                    return;
                }
                TransformCreateLessonForm(false);
            }
        }
        void TransformCreateLessonForm(bool isUp)
        {
            lessonTitleInputField.transform.parent.gameObject.SetActive(true);
            // categoryComponent.transform.parent.gameObject.SetActive(true);
        }

        IEnumerator UpdateModelStoreManager()
        {
            // Call API to get the file path 
            using (var uwr = UnityWebRequest.Get(APIUrlConfig.BASE_URL + $"models/get3DModelDetail/{ModelStoreManager.modelId}"))
                {
                    uwr.SetRequestHeader("Authorization", PlayerPrefs.GetString("user_token"));

                    var operation = uwr.SendWebRequest();

                    while (!operation.isDone)
                    {
                        yield return null;
                    }

                    if (uwr.downloadHandler.text == "Unauthorized")
                    {
                        Debug.Log(uwr.downloadHandler.text);
                        yield break;
                    }
                    string str = System.Text.Encoding.UTF8.GetString(uwr.downloadHandler.data);
                    Debug.Log(str);
                    Response response = JsonUtility.FromJson<Response>(str);

                    ModelData data = response.data[0];
                    Debug.Log("Here :" + data.modelFile);
                    ModelStoreManager.InitModelStore(ModelStoreManager.modelId, data.modelName, data.modelFile);
            }
        }
        
        void InitDropDown()
        {           
            listOrgans = LoadData.Instance.getListOrgans();
            if (listOrgans.data.Length > 0)
            {
                foreach (ListOrganLesson organ in listOrgans.data)
                {
                    option_.Add(new Dropdown.OptionData(organ.organsName));
                }
                dropdown.AddOptions(option_);
            }
            else
            {
                // dropdown.GetComponent<Dropdown>().options.Add(new Dropdown.OptionData() { text = "--Choose--" });
                indexItemDropdown = 1;
                dropdown.value = indexItemDropdown;
            }
            ClickItemDropdown(dropdown);
            dropdown.onValueChanged.AddListener(delegate { ClickItemDropdown(dropdown); });
        }

        public void ClickItemDropdown(Dropdown dropdown)
        {
            Debug.Log("minhlh17: " + dropdown.value);
            indexItemDropdown = dropdown.value;
        }

        void FreezeUI(bool isFreezed)
        {
            buildLessonBtn.interactable = !isFreezed;
            foreach (Button button in resetInputFieldBtns)
            {
                button.interactable = !isFreezed;
            }
            lessonTitleInputField.interactable = !isFreezed;
            lessonObjectiveInputField.interactable = !isFreezed;
            dropdown.interactable = !isFreezed;
            spinner.SetActive(isFreezed);
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: save create lesson info
        /// </summary>
        private void CreateLessonInfo() 
        {   
            string lessonTitle = lessonTitleInputField.Text;
            string lessonObjective = lessonObjectiveInputField.Text;
            Dropdown categoryLesson = dropdown;
            bool isValidInfo = false;

            if (!IsValidLessonTitle(lessonTitle))
            {
                isValidInfo = true;
            }
            if (!IsValidLessonObjective(lessonObjective))
            {
                isValidInfo = true;
            }
            if (!IsValidCategoryCreateLesson(categoryLesson))
            {
                isValidInfo = true;
            }
            if (!isValidInfo)
            {
                PublicLesson newLesson = new PublicLesson();
                newLesson.modelId = ModelStoreManager.modelId;
                newLesson.lessonTitle = createLesssonFormComponent.transform.GetChild(0).GetChild(1).GetComponent<AdvancedInputField>().Text;
                newLesson.organId = listOrgans.data[dropdown.value - 1].organsId;
                newLesson.lessonObjectives = createLesssonFormComponent.transform.GetChild(2).GetChild(1).GetComponent<AdvancedInputField>().Text;
                newLesson.publicLesson = createLesssonFormComponent.transform.GetChild(3).GetChild(0).GetComponent<Toggle>().isOn ? 1 : 0;
                spinner.SetActive(false);

                StaticLesson.LessonTitle = createLesssonFormComponent.transform.GetChild(0).GetChild(1).GetComponent<AdvancedInputField>().Text;
                StaticLesson.ModelId = ModelStoreManager.modelId;
                StaticLesson.ModelFile = ModelStoreManager.modelFile;
                StaticLesson.LessonObjectives = createLesssonFormComponent.transform.GetChild(2).GetChild(1).GetComponent<AdvancedInputField>().Text;
                // StaticLesson.PublicLesson = createLesssonFormComponent.transform.GetChild(3).GetChild(0).GetComponent<Toggle>().isOn ? 1 : 0;
                BuildLesson(newLesson);
                panelConfirm.SetActive(false);
            }
            if (isValidInfo)
            {
                panelConfirm.SetActive(false);
            }
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: Call API POST create lesson info
        /// </summary>
        public async Task BuildLesson(PublicLesson newLesson)
        {
            try
            {
                FreezeUI(true);
                TransformCreateLessonForm(false);
                APIResponse<DataCreateLesson[]> createLessonInfoResponse = await UnityHttpClient.CallAPI<DataCreateLesson[]>(APIUrlConfig.POST_CREATE_LESSON_INFO, UnityWebRequest.kHttpVerbPOST, newLesson);
                if (createLessonInfoResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
                {
                    SaveLesson.SaveLessonId(createLessonInfoResponse.data[0].lessonId);
                    Debug.Log("lessonId: " + createLessonInfoResponse.data[0].lessonId);
                    LessonManager.InitLesson(createLessonInfoResponse.data[0].lessonId);
                    await Task.Delay(1000);
                    StartCoroutine(Helper.LoadAsynchronously(SceneConfig.buildLesson));
                }
                else
                {
                    throw new Exception(createLessonInfoResponse.message);
                }
            }
            catch (Exception e)
            {
                FreezeUI(false);
                Toast.ShowCommonToast(e.Message, APIUrlConfig.SERVER_ERROR_RESPONSE_CODE);
                spinner.SetActive(false);
                return;
            }
        }
    }

    [System.Serializable]
    class Response 
    {
        public long code;
        public string message;
        public ModelData[] data;
    }

    [System.Serializable]
    class ModelData 
    {
        public long modelId;
        public string modelName;
        public long modelFileId;
        public long thumbnailFileId;
        public string modelThumbnail;
        public string modelFile;
        public int type;
    }
}
