using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using UnityEngine.SceneManagement; 
using UnityEngine.EventSystems; 

namespace CreateLesson
{
    public class InteractionUI : MonoBehaviour
    {
        private GameObject backToBack;
        private GameObject cancelBtn;
        public GameObject panelConfirm;
        public GameObject btnExitPopUp; 
        public GameObject btnSave;
        public GameObject btnDiscard; 

        private static InteractionUI instance; 
        public static InteractionUI Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<InteractionUI>();
                }
                return instance; 
            }
        }
        void Start()
        {
            InitUI(); 
            SetActions(); 
        }
        void InitUI()
        {
            backToBack = GameObject.Find("BackBtn"); 
            cancelBtn = GameObject.Find("CancelBtn");
        }
        void SetActions()
        {
            backToBack.GetComponent<Button>().onClick.AddListener(HandlerBackTo3DStore);
            cancelBtn.GetComponent<Button>().onClick.AddListener(HandlerBackTo3DStore);
            btnExitPopUp.GetComponent<Button>().onClick.AddListener(HandlerExitPopUpConfirm);
            btnDiscard.GetComponent<Button>().onClick.AddListener(CancelButtonToBack3DStore); 
        }

        void HandlerBackTo3DStore()
        {
            panelConfirm.SetActive(true);
        }
        void HandlerExitPopUpConfirm()
        {
            panelConfirm.SetActive(false);
        }
        
        void CancelButtonToBack3DStore()
        {
            if(ScenePrevious.scenePrevious == SceneConfig.interactiveModel)
            {
                BackOrLeaveApp.Instance.BackToPreviousScene(SceneManager.GetActiveScene().name);
                SceneManager.LoadScene(SceneConfig.uploadModel);
            }
            else
            {
                if(ScenePrevious.scenePrevious == SceneConfig.storeModel)
                {
                    BackOrLeaveApp.Instance.BackToPreviousScene(SceneManager.GetActiveScene().name);
                    SceneManager.LoadScene(SceneConfig.storeModel);
                }
            }
        }
    }
}
