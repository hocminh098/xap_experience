using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using System; 
using UnityEngine.Networking;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;
using System.Text;
using EasyUI.Toast;
using System.Threading.Tasks;
using AdvancedInputFieldPlugin;

namespace UpdateLessonObjectives
{
    public class LoadScene : MonoBehaviour
    {
        public GameObject spinner;
        public LessonDetail[] myData;
        public LessonDetail currentLesson; 
        public GameObject updateLesssonFormComponent;
        public AdvancedInputField lessonTitleInputField;
        public GameObject txtLessonTitleNotification;
        public AdvancedInputField lessonObjectiveInputField;
        public GameObject txtLessonObjectiveNotification;
        public Button updateBtn;
        public List<Button> resetInputFieldBtns;
        private ListOrgans listOrgans;
        public GameObject dropdownObj; 
        private Dropdown dropdown;
        public GameObject txtCategoryNotification;
        private List<Dropdown.OptionData> options = new List<Dropdown.OptionData>();
        private int indexItemDropdown = 0;
        public GameObject panelConfirm;
        public Button saveConfirmBtn;
        private bool isValidLessonTitle = true;
        private bool isValidLessonObjective = true;

        void Start()
        {
            dropdown = dropdownObj.GetComponent<Dropdown>();
            myData = LoadData.Instance.GetLessonByID(LessonManager.lessonId.ToString()).data;
            currentLesson = myData[0];
            UpdateDropDown();
            StartCoroutine(LoadCurrentLesson(currentLesson));
            InitScreen();
            InitUI();
            InitEvents();
        }

        void Update()
        {
            if (lessonTitleInputField.Selected == true && lessonTitleInputField.Text != "" && isValidLessonTitle == false)
            {
                lessonTitleInputField.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldWarning);
            }
            else if (lessonTitleInputField.Selected == true && lessonTitleInputField.Text == "" && isValidLessonTitle == true)
            {
                lessonTitleInputField.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldBorder);
            }
            else if (lessonTitleInputField.Selected == false && lessonTitleInputField.Text != "" && isValidLessonTitle == true)
            {
                lessonTitleInputField.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldNormal);
            }

            if (lessonObjectiveInputField.Selected == true && lessonObjectiveInputField.Text != "" && isValidLessonObjective == false)
            {
                lessonObjectiveInputField.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.border_red);
            }
            else if (lessonObjectiveInputField.Selected == true && lessonObjectiveInputField.Text == "" && isValidLessonObjective == true)
            {
                lessonObjectiveInputField.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.border_grey);
            }
            else if (lessonObjectiveInputField.Selected == false && lessonObjectiveInputField.Text != "" && isValidLessonObjective == true)
            {
                lessonObjectiveInputField.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.border_normal);
            }
        }

        void OnEnable()
        {
            NativeKeyboardManager.AddKeyboardHeightChangedListener(OnKeyboardHeightChanged); 
        }

        void OnDisable()
        {
            NativeKeyboardManager.RemoveKeyboardHeightChangedListener(OnKeyboardHeightChanged); 
        }

        void InitScreen()
        {
            Screen.orientation = ScreenOrientation.Portrait;
            StatusBarManager.statusBarState = StatusBarManager.States.TranslucentOverContent;
            StatusBarManager.navigationBarState = StatusBarManager.States.Hidden;
        }

        void InitUI()
        {
            HideAllNotifications();
            spinner.SetActive(false);
        }

        void HideAllNotifications()
        {
            txtLessonTitleNotification.SetActive(false);
            txtLessonObjectiveNotification.SetActive(false);
            txtCategoryNotification.SetActive(false);
        }

        void InitEvents()
        {
            lessonTitleInputField.OnValueChanged.AddListener(CheckLessonTitle);
            lessonObjectiveInputField.OnValueChanged.AddListener(CheckLessonObjective);
            updateBtn.onClick.AddListener(UpdateLessonObjective);
            saveConfirmBtn.onClick.AddListener(UpdateLessonObjective);
            lessonTitleInputField.OnBeginEdit.AddListener(OnLessonTitleInputFieldBeginEdit);
            lessonObjectiveInputField.OnBeginEdit.AddListener(OnLessonObjectiveInputFieldBeginEdit);
            lessonObjectiveInputField.OnEndEdit.AddListener(OnLessonObjectiveInputFieldEndEdit);
            dropdown.onValueChanged.AddListener(delegate {
                IsValidCategoryCreateLesson(dropdown);
            });
        }

        public void OnLessonObjectiveInputFieldEndEdit(string arg0, EndEditReason reason)
        {
            if (reason == EndEditReason.KEYBOARD_DONE)
            {
                UpdateLessonObjective();
            }
        }

        void CheckLessonTitle(string data)
        {
            IsValidLessonTitle(data);
        }
        void CheckLessonObjective(string data)
        {
            IsValidLessonObjective(data);
        }

        bool IsValidLessonTitle(string data)
        {
            if(string.IsNullOrEmpty(data))
            {
                txtLessonTitleNotification.GetComponent<Text>().text = AuthenConfig.emptyValue;
                isValidLessonTitle = false;
                ChangeUIStatus(lessonTitleInputField, txtLessonTitleNotification, true);
                updateBtn.interactable = false;
                return false;
            }
            else
            {
                updateBtn.interactable = true;
                isValidLessonTitle = true;
                ChangeUIStatus(lessonTitleInputField, txtLessonTitleNotification, false);
                return true;
            }
        }

        bool IsValidLessonObjective(string data)
        {
            if(string.IsNullOrEmpty(data))
            {
                txtLessonObjectiveNotification.GetComponent<Text>().text = AuthenConfig.emptyValue;
                isValidLessonObjective = false;
                ChangeUIStatusObjective(lessonObjectiveInputField, txtLessonObjectiveNotification, true);
                updateBtn.interactable = false;
                return false;
            }
            else
            {
                updateBtn.interactable = true;
                isValidLessonObjective = true;
                ChangeUIStatusObjective(lessonObjectiveInputField, txtLessonObjectiveNotification, false);
                return true;
            }
        }

        bool IsValidCategoryCreateLesson(Dropdown dropdown)
        {
            if (dropdown.value == 0)
            {
                txtCategoryNotification.GetComponent<Text>().text = AuthenConfig.emptyValue;
                ChangeUINotification(dropdown, txtCategoryNotification, true);
                updateBtn.interactable = false;
                return false;
            }
            else
            {
                updateBtn.interactable = true;
                ChangeUINotification(dropdown, txtCategoryNotification, false);
                return true;
            }
        }

        void OnLessonTitleInputFieldBeginEdit(BeginEditReason reason)
        {
            TransformUpdateLessonForm(true);
        }

        void OnLessonObjectiveInputFieldBeginEdit(BeginEditReason reason)
        {
            TransformUpdateLessonForm(true);
            lessonTitleInputField.transform.parent.gameObject.SetActive(false);
        }

        private void ChangeUIStatus(AdvancedInputField input, GameObject warning, bool status)
        {
            warning.SetActive(status); 
            if(status)
            {
                input.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldWarning);
            }
            else
            {
                input.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldBorder);
            }
        }

        private void ChangeUIStatusObjective(AdvancedInputField input, GameObject warning, bool status)
        {
            warning.SetActive(status); 
            if(status)
            {
                input.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.border_red);
            }
            else
            {
                input.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.border_grey);
            }
        }

        private void ChangeUINotification(Dropdown dropdown, GameObject warning, bool status)
        {
            warning.SetActive(status); 
            if(status)
            {
                dropdown.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldWarning);
            }
            else
            {
                dropdown.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldNormal);
            }
        }

        public void OnKeyboardHeightChanged(int keyboardHeight)
        {
            if (keyboardHeight == 0)
            {
                if (lessonTitleInputField.Selected || lessonObjectiveInputField.Selected)
                {
                    return;
                }
                TransformUpdateLessonForm(false);
            }
        }

        void TransformUpdateLessonForm(bool isUp)
        {
            lessonTitleInputField.transform.parent.gameObject.SetActive(true);
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: Load current lesson
        /// </summary>
        IEnumerator LoadCurrentLesson(LessonDetail currentLesson)
        {
            string imageUri = String.Format(APIUrlConfig.LoadLesson, currentLesson.lessonThumbnail);
            updateLesssonFormComponent.transform.GetChild(0).GetChild(1).GetComponent<AdvancedInputField>().Text = currentLesson.lessonTitle; 
            updateLesssonFormComponent.transform.GetChild(2).GetChild(1).GetComponent<AdvancedInputField>().Text = currentLesson.lessonObjectives;
            updateLesssonFormComponent.transform.GetChild(3).GetChild(0).GetComponent<Toggle>().isOn = currentLesson.isPublic == 1 ? true:false;
            /// Purpose: Loop to find the mapping index of organId on the dropDown 
            foreach (ListOrganLesson x in listOrgans.data)
            {
                Debug.Log("Current lesson: " + x.organsId);
            }
            ListOrganLesson organ = Array.Find<ListOrganLesson>(listOrgans.data, item => item.organsId == currentLesson.organId);
            int organIndex = dropdown.options.FindIndex((i) => { return i.text.Equals(organ.organsName); });
            dropdown.value = organIndex;

            ModelStoreManager.InitModelStore(currentLesson.modelId, ModelStoreManager.modelName);
            UnityWebRequest request = UnityWebRequestTexture.GetTexture(imageUri);
            yield return request.SendWebRequest(); 
            if (request.isNetworkError || request.isHttpError)
            {

            }
            if (request.isDone)
            {
                Texture2D tex = ((DownloadHandlerTexture) request.downloadHandler).texture;
                Sprite sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(tex.width / 2, tex.height / 2));
            }
        }

        void UpdateDropDown()
        {
            listOrgans = LoadData.Instance.getListOrgans();
            if (listOrgans.data.Length > 0)
            {
                foreach (ListOrganLesson organ in listOrgans.data)
                {
                    options.Add(new Dropdown.OptionData(organ.organsName));
                }
                dropdown.AddOptions(options);
            }
            else
            {
                // dropdown.GetComponent<Dropdown>().options.Add(new Dropdown.OptionData() { text = "--Choose--" });
                indexItemDropdown = 0;
                dropdown.value = indexItemDropdown;
            }
            ClickItemDropdown(dropdown);
            dropdown.onValueChanged.AddListener(delegate { ClickItemDropdown(dropdown); });
        }

        public void ClickItemDropdown(Dropdown dropdown)
        {
            indexItemDropdown = dropdown.value;
        }

        void FreezeUI(bool isFreezed)
        {
            updateBtn.interactable = !isFreezed;
            foreach (Button button in resetInputFieldBtns)
            {
                button.interactable = !isFreezed;
            }
            lessonTitleInputField.interactable = !isFreezed;
            lessonObjectiveInputField.interactable = !isFreezed;
            dropdown.interactable = !isFreezed;
            spinner.SetActive(isFreezed);
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: save update lesson info
        /// </summary>
        void UpdateLessonObjective()
        {
            string lessonTitle = lessonTitleInputField.Text;
            string lessonObjective = lessonObjectiveInputField.Text;
            Dropdown categoryLesson = dropdown;
            bool isValidInfo = false;

            if (!IsValidLessonTitle(lessonTitle))
            {
                isValidInfo = true;
            }
            if (!IsValidLessonObjective(lessonObjective))
            {
                isValidInfo = true;
            }
            if (!IsValidCategoryCreateLesson(categoryLesson))
            {
                isValidInfo = true;
            }
            if (!isValidInfo)
            {
                PublicLesson newLesson = new PublicLesson();
                newLesson.modelId = ModelStoreManager.modelId;
                newLesson.lessonTitle = updateLesssonFormComponent.transform.GetChild(0).GetChild(1).GetComponent<AdvancedInputField>().Text;
                newLesson.organId = listOrgans.data[dropdown.value - 1].organsId;
                newLesson.lessonObjectives = updateLesssonFormComponent.transform.GetChild(2).GetChild(1).GetComponent<AdvancedInputField>().Text;
                newLesson.publicLesson = updateLesssonFormComponent.transform.GetChild(3).GetChild(0).GetComponent<Toggle>().isOn ? 1 : 0;
                spinner.SetActive(false);

                Submit(LessonManager.lessonId, newLesson);
                panelConfirm.SetActive(false);
            }
            if (isValidInfo)
            {
                panelConfirm.SetActive(false);
            }
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: Call API PATCH update lesson info
        /// </summary>
        public async Task Submit(int lessonId, PublicLesson newLesson)
        {
            try
            {
                FreezeUI(true);
                TransformUpdateLessonForm(false);
                string url = String.Format(APIUrlConfig.PATCH_UPDATE_LESSON_INFO, lessonId);
                APIResponse<string> updateLessonResponse = await UnityHttpClient.CallAPI<string>(url, APIUrlConfig.PATCH_METHOD, newLesson);
                if (updateLessonResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
                {
                    spinner.SetActive(false);
                    panelConfirm.SetActive(false);
                    Toast.ShowCommonToast(updateLessonResponse.message, APIUrlConfig.SUCCESS_RESPONSE_CODE);
                    await Task.Delay(1000);
                    StartCoroutine(Helper.LoadAsynchronously(SceneConfig.lesson_edit));
                }
                else
                {
                    throw new Exception(updateLessonResponse.message);
                }
                Debug.Log($"lhminh17 {updateLessonResponse.code}");
            }
            catch (Exception e)
            {
                FreezeUI(false);
                Toast.ShowCommonToast(e.Message, APIUrlConfig.SERVER_ERROR_RESPONSE_CODE);
                spinner.SetActive(false);
                return;
            }
        }
    }
}
