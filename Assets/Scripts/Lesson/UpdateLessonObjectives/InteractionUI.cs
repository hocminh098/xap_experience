using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace UpdateLessonObjectives
{
    public class InteractionUI : MonoBehaviour
    {
        private GameObject backToLessonDetailEdit; 
        private GameObject cancelBtn;
        public GameObject panelConfirm;
        public GameObject btnExitPopUp; 
        public GameObject btnSave;
        public GameObject btnDiscard;

        private static InteractionUI instance; 
        public static InteractionUI Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<InteractionUI>(); 
                }
                return instance; 
            }
        }
        void Start()
        {
            InitUI();
            SetActions();
        }
        void InitUI()
        {
            backToLessonDetailEdit = GameObject.Find("BackBtn");
            cancelBtn = GameObject.Find("CancelBtn");
        }
        void SetActions()
        {
            backToLessonDetailEdit.GetComponent<Button>().onClick.AddListener(HandlerBackToLessonDetailEdit);
            cancelBtn.GetComponent<Button>().onClick.AddListener(HandlerBackToLessonDetailEdit);
            btnExitPopUp.GetComponent<Button>().onClick.AddListener(HandlerExitPopUpConfirm);
            btnDiscard.GetComponent<Button>().onClick.AddListener(CancelButton); 
        }

        void HandlerBackToLessonDetailEdit()
        {
            panelConfirm.SetActive(true);
        }
        void HandlerExitPopUpConfirm()
        {
            panelConfirm.SetActive(false);
        }
        
        void CancelButton()
        {
            BackOrLeaveApp.Instance.BackToPreviousScene(SceneManager.GetActiveScene().name);
        }
    }
}
