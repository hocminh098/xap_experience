using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System;
using EasyUI.Toast;
using System.Threading.Tasks;

namespace LessonDetail_Edit 
{
    public class InteractionUI : MonoBehaviour
    {
        public GameObject loadingScreen;
        private GameObject startLessonBtn;
        private GameObject startMeetingBtn;
        public GameObject backToHomeBtn;
        private GameObject buildLessonBtn;
        private GameObject lessonObjectivesBtn; 
        private static InteractionUI instance; 
        public static InteractionUI Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<InteractionUI>();
                }
                return instance; 
            }
        }
        private string hightLightBtnColor = "#C8C8C8";
        
        void Start()
        {
            loadingScreen.SetActive(false);
            InitUI(); 
            SetActions();
        }
        void InitUI()
        {
            startLessonBtn = GameObject.Find("StartLessonBtn");
            if (PlayerPrefs.GetString("user_email") != "") 
            {
                startMeetingBtn = GameObject.Find("StartMeetingBtn"); 
            }
            buildLessonBtn = GameObject.Find("BtnBuildLesson");
            lessonObjectivesBtn = GameObject.Find("BtnLessonObjectives");
        }
        void SetActions()
        {
            backToHomeBtn.GetComponent<Button>().onClick.AddListener(BackToMyLesson);
            startLessonBtn.GetComponent<Button>().onClick.AddListener(StartExperience);
            if (PlayerPrefs.GetString(PlayerPrefConfig.userToken) != "")
            {
                startMeetingBtn.GetComponent<Button>().onClick.AddListener(HandleSwitchStartMeeting);
            }
            buildLessonBtn.GetComponent<Button>().onClick.AddListener(HandleSwitchBuildLesson);
            lessonObjectivesBtn.GetComponent<Button>().onClick.AddListener(UpdateLessonObjectives);
        }
        void BackToMyLesson()
        {
            BackOrLeaveApp.Instance.BackToPreviousScene(SceneManager.GetActiveScene().name);
        }

        public async void HandleSwitchStartMeeting()
        {
            await StartMeeting();
        }
        public async void HandleSwitchBuildLesson()
        {
            await BuildLesson();
        }

        void StartExperience()
        {
            startLessonBtn.GetComponent<Button>().interactable = false;
            BackOrLeaveApp.Instance.AddPreviousScene(SceneManager.GetActiveScene().name, SceneConfig.experience);
            loadingScreen.SetActive(true);
            StartCoroutine(Helper.LoadAsynchronously(SceneConfig.experience));
        }

        public async Task StartMeeting()
        {
            loadingScreen.SetActive(true);
            try
            {
                startMeetingBtn.GetComponent<Button>().interactable = false;
                BackOrLeaveApp.Instance.AddPreviousScene(SceneManager.GetActiveScene().name, SceneConfig.meetingStarting); 
                Exception exception = Network.CheckNetWorkToDisplayToast();
                if (exception != null) throw exception;
                StartCoroutine(Helper.LoadAsynchronously(SceneConfig.meetingStarting));
            }
            catch (Exception exception)
            {
                loadingScreen.SetActive(false);
                startMeetingBtn.GetComponent<Button>().interactable = true;
                Toast.ShowCommonToast(exception.Message, APIUrlConfig.SERVER_ERROR_RESPONSE_CODE);
                return;
            }
        }

        public async Task BuildLesson()
        {
            loadingScreen.SetActive(true);
            try
            {
                BackOrLeaveApp.Instance.AddPreviousScene(SceneManager.GetActiveScene().name, SceneConfig.buildLesson);
                Exception exception = Network.CheckNetWorkToDisplayToast();
                if (exception != null) throw exception;
                StopAllCoroutines();
                await Helper.GetInforDetaiLesson(System.Convert.ToInt32(PlayerPrefs.GetString("lessonId")));
                SceneNameManager.setPrevScene(SceneManager.GetActiveScene().name); 
                StartCoroutine(Helper.LoadAsynchronously(SceneConfig.buildLesson));
            }
            catch (Exception exception)
            {
                loadingScreen.SetActive(false);
                Toast.ShowCommonToast(exception.Message, APIUrlConfig.SERVER_ERROR_RESPONSE_CODE);
                return;
            }
        }

        public async void UpdateLessonObjectives()
        {
            loadingScreen.SetActive(true);
            try
            {
                BackOrLeaveApp.Instance.AddPreviousScene(SceneManager.GetActiveScene().name, SceneConfig.update_lesson);
                Exception exception = Network.CheckNetWorkToDisplayToast();
                if (exception != null) throw exception;
                SceneNameManager.setPrevScene(SceneManager.GetActiveScene().name); 
                StartCoroutine(Helper.LoadAsynchronously(SceneConfig.update_lesson));
            }
            catch (Exception exception)
            {
                loadingScreen.SetActive(false);
                Toast.ShowCommonToast(exception.Message, APIUrlConfig.SERVER_ERROR_RESPONSE_CODE);
                return;
            }
        }
    }
}
