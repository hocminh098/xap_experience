using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using UnityEngine.Networking;
using System.IO;
using System; 
using System.Threading.Tasks; 

namespace MyLesson
{
    public class LoadData : MonoBehaviour
    {
        private string jsonResponse;
        private static LoadData instance; 
        public static LoadData Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = FindObjectOfType<LoadData>();
                }
                return instance; 
            }
        }
        
        public async Task<Lesson[]> GetListMyLesson(string searchValue, int offset, int limit)
        {
            try
            {
                APIResponse<Lesson[]> getListMyLessonResponse = await UnityHttpClient.CallAPI<Lesson[]>(String.Format(APIUrlConfig.GET_MY_LESSONS), UnityWebRequest.kHttpVerbGET);
                if (getListMyLessonResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
                {
                    return getListMyLessonResponse.data;
                }
                else
                {
                    throw new Exception(getListMyLessonResponse.message);
                }
            }
            catch (Exception e)
            {
                throw e;
                Debug.Log("failed" + e.Message);
            }
        }
    }
}
