using System.Collections;
using System.Collections.Generic;
using UnityEngine; 
using UnityEngine.UI; 
using System; 
using UnityEngine.Networking;
using System.Threading.Tasks;
using EasyUI.Toast;

namespace LessonDescription
{
    public class LoadScene : MonoBehaviour
    {
        private static LoadScene instance; 
        public static LoadScene Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<LoadScene>(); 
                }
                return instance; 
            }
        }
        public GameObject bodyObject;
        [SerializeField]
        public GameObject lessonTitle; 
        public GameObject loadingImage;
        public Button startMeetingBtn;
        public Button startExperienceBtn;

        public GameObject lessonObjectivesComponent;
        public GameObject scrollView;
        public GameObject viewPort;
        public GameObject contentLessonDetail;
        public GameObject txtLessonDetail;

        void Start()
        {
            InitScreen(); 
            InitUI();   
            LoadLessonDetail(LessonManager.lessonId);
            txtLessonDetail = GameObject.Find("TxtLessonDetail");
        }

        void Update()
        {
            /// Purpose: Check the size of the Content Size filter and the View Port
            if (viewPort.transform.GetComponent<RectTransform>().rect.height >= contentLessonDetail.transform.GetComponent<RectTransform>().rect.height)
            {
                /// Purpose: Disable the scroll view
                scrollView.transform.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
                scrollView.transform.GetComponent<ScrollRect>().enabled = false;
            }
            else 
            {
                /// Purpose: Enable the sroll view
                // scrollView.transform.GetComponent<ScrollRect>().verticalNormalizedPosition = 0f;
                scrollView.transform.GetComponent<ScrollRect>().enabled = true;
            }
        }

        void InitUI()
        {
            if (startMeetingBtn != null)
            {
                startMeetingBtn.interactable = false;
            }
            if (startExperienceBtn != null)
            {
                startExperienceBtn.interactable = false;
            }
        }

        void InitScreen()
        {
            Screen.orientation = ScreenOrientation.Portrait; 
            StatusBarManager.statusBarState = StatusBarManager.States.TranslucentOverContent;
            StatusBarManager.navigationBarState = StatusBarManager.States.Hidden;
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: Call API Get lesson by Id
        /// </summary>
        public async void LoadLessonDetail(int lessonId)
        {
            try
            {
                APIResponse<LessonDetail[]> lessonDetailResponse = await UnityHttpClient.CallAPI<LessonDetail[]>(String.Format(APIUrlConfig.GET_LESSON_BY_ID, lessonId), UnityWebRequest.kHttpVerbGET);
                if (lessonDetailResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
                {
                    StaticLesson.SetValueForStaticLesson(lessonDetailResponse.data[0]);
                    LoadDataIntoUI();
                }
                else
                {
                    throw new Exception(lessonDetailResponse.message);
                }
            }
            catch (Exception ex)
            {
                Toast.ShowCommonToast(ex.Message, APIUrlConfig.SERVER_ERROR_RESPONSE_CODE);
            }
        }
        
        /// <summary>
        /// Author: minhlh17
        /// Purpose: Load data into UI
        /// </summary>
        void LoadDataIntoUI()
        {
            if (startMeetingBtn != null)
            {
                startMeetingBtn.interactable = true;
            }
            if (startExperienceBtn != null)
            {
                startExperienceBtn.interactable = true;
            }
            try
            {
                string imageURL = APIUrlConfig.DOMAIN_SERVER + StaticLesson.LessonThumbnail;
                RawImage targetImage = bodyObject.transform.GetChild(0).GetChild(0).GetComponent<RawImage>();
                StartCoroutine(UnityHttpClient.LoadRawImageAsync(imageURL, targetImage, (isSuccess) => {
                    if (isSuccess)
                    {
                        bodyObject.transform.GetChild(0).GetChild(0).GetChild(0).gameObject.SetActive(false);
                    }
                }));
                lessonTitle.gameObject.GetComponent<Text>().text = Helper.ShortString(StaticLesson.LessonTitle.ToLower(), 30);
                txtLessonDetail.GetComponent<Text>().text = StaticLesson.LessonObjectives;
                // bodyObject.transform.GetChild(3).GetChild(0).GetChild(1).GetComponent<Text>().text = StaticLesson.LessonObjectives;
                bodyObject.transform.GetChild(2).GetChild(0).GetChild(1).GetChild(0).GetComponent<Text>().text = Helper.ShortString(StaticLesson.AuthorName, 18);
                bodyObject.transform.GetChild(2).GetChild(0).GetChild(1).GetChild(1).GetComponent<Text>().text = DateTime.Parse(StaticLesson.CreatedDate).ToString("dd/MM/yyyy HH:mm:ss");
                bodyObject.transform.GetChild(2).GetChild(1).GetChild(0).GetComponent<Text>().text = "#" + StaticLesson.LessonId.ToString();
                bodyObject.transform.GetChild(2).GetChild(1).GetChild(1).GetComponent<Text>().text = StaticLesson.Viewed.ToString() + " Views";
                loadingImage.SetActive(false);
            }
            catch(Exception e)
            {
                Debug.Log(e.Message);
            }
        }
    }
}
