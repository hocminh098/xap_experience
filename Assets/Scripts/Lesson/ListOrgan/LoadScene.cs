using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using System; 
using System.Text.RegularExpressions;
using UnityEngine.EventSystems; 
using UnityEngine.Networking;
using System.Threading.Tasks;
using System.Linq;
using EasyUI.Toast;

namespace ListOrgan 
{
    public class LoadScene : MonoBehaviour
    {
        private int timeDelayForSearch = 400;
        private char[] charsToTrim = { '*', '.', ' '};
        public int calculatedSize = 35; 
        private string searchValueString; 
        private int offset = 0;
        private int limit = 8;
        public GameObject lessonPanelObject;
        public Text totalFoundLessonText;
        public GameObject lessonObjectResource;
        public InputField searchInputField; 
        public GameObject resetSearchBoxBtn; 
        public CustomScrollRect lessonPanelScrollRect;
        public GameObject loadingItem;
        public Text organHeaderText;
        [SerializeField] private UIRefreshControl m_UIRefreshControl;
        [SerializeField] private UILoadmoreControl m_UILoadmoreControl;
        private bool isLoadmoreActive = true;
        public GameObject noDataComponent;

        async void Start()
        {
            InitScreen();
            InitUI();
            InitEvents(); 
            await UpdateLessonsData();
            noDataComponent.SetActive(false);
        }

        void InitScreen()
        {
            Screen.orientation = ScreenOrientation.Portrait; 
            StatusBarManager.statusBarState = StatusBarManager.States.TranslucentOverContent;
            StatusBarManager.navigationBarState = StatusBarManager.States.Hidden;
        }
        void InitEvents()
        {
            resetSearchBoxBtn.transform.GetComponent<Button>().onClick.AddListener(ResetSearchBox); 
            searchInputField.onValueChanged.AddListener(SearchLessons); 
            m_UIRefreshControl.OnRefresh.AddListener(RefreshData);
            m_UILoadmoreControl.OnLoadmore.AddListener(LoadmoreData);
        }

        async void RefreshData()
        {
            await UpdateLessonsData();
            m_UIRefreshControl.EndRefreshing();
        }

        async void LoadmoreData()
        {
            await LoadMoreLessons();
            m_UILoadmoreControl.EndLoadingmore();
        }

        void InitUI()
        {
            organHeaderText.text = OrganManager.organName;
        }
        void ResetSearchBox()
        {
            searchInputField.text = "";
            searchInputField.Select();
        }

        async void SearchLessons(string value)
        {
            searchValueString = Regex.Replace(value, @"\s+", " ").ToLower().Trim(charsToTrim); 
            resetSearchBoxBtn.SetActive(!string.IsNullOrEmpty(searchValueString)); 
            if (string.IsNullOrEmpty(searchValueString))
            {
                await UpdateLessonsData();
            }
            else
            {
                await CheckForSearchingLessons(value);
            }
        }

        async Task CheckForSearchingLessons(string value)
        {
            await Task.Delay(timeDelayForSearch);
            if (value == searchValueString)
            {
                await UpdateLessonsData();
            }
        }

        async Task UpdateLessonsData()
        {
            isLoadmoreActive = true;
            offset = 0;
            await UpdateLessonsPanel(true);
        }

        async Task LoadMoreLessons()
        {
            offset++;
            await UpdateLessonsPanel(false);
        }

        /// <summary>
        /// Author: minhlh17
        /// Purpose: UpdateLessonPanel and call API GET_SEARCH_LESSONS_BY_ORGAN
        /// </summary>
        async Task UpdateLessonsPanel(bool isRenewLessonPanel)
        {
            try
            {
                if (!isRenewLessonPanel && !isLoadmoreActive)
                {
                    return;
                }
                if (string.IsNullOrEmpty(searchValueString))
                {
                    searchValueString = null;
                }
                if (isRenewLessonPanel)
                {
                    DestroyAllLessons();
                    totalFoundLessonText.text = "";
                }
                ResetLoading(true);
                noDataComponent.SetActive(false);

                string URL = String.Format(APIUrlConfig.GET_SEARCH_LESSONS_BY_ORGAN, searchValueString, offset, limit, OrganManager.organId);            
                APIResponse<List<Lesson>> lessonResponse = await UnityHttpClient.CallAPI<List<Lesson>>(URL, UnityWebRequest.kHttpVerbGET);
                noDataComponent.SetActive(false);
                ResetLoading(false);              
                if (isRenewLessonPanel)
                {
                    DestroyAllLessons();
                    totalFoundLessonText.text = "";
                }

                if (lessonResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
                {
                    offset = lessonResponse.meta.page;
                    isLoadmoreActive = (lessonResponse.meta.totalPage > lessonResponse.meta.page + 1);
                    StartCoroutine(LoadLessonsIntoUI(lessonResponse.data, lessonResponse.meta.totalElements, isRenewLessonPanel));
                }
            } 
            catch (Exception e)
            {
                Debug.Log(e.Message);
                ResetLoading(false);
                noDataComponent.SetActive(false);  
            }
        }

        void ResetLoading(bool isActive)
        {
            if (loadingItem != null)
            {
                loadingItem.SetActive(isActive);  
                loadingItem.transform.SetAsLastSibling();
            }
        }

        IEnumerator LoadLessonsIntoUI(List<Lesson> lessons, int totalFoundLesson, bool isRenewLessonPanel)
        {
            if (isRenewLessonPanel)
            {
                DestroyAllLessons();
                // totalFoundLessonText.text = (lessons.Count) > 0 ? totalFoundLesson.ToString() + ItemConfig.foundLessonNumberIntro : ItemConfig.noDataNotification;
                totalFoundLessonText.text = totalFoundLesson.ToString() + ItemConfig.foundLessonNumberIntro;
            }
            if (lessons.Count < 1)
            {
                if (isRenewLessonPanel)
                {
                    totalFoundLessonText.text = "";
                    noDataComponent.SetActive(true);
                }
                yield break;
            }
            noDataComponent.SetActive(false);  
            foreach (Lesson lesson in lessons)
            {
                InstantiateLesson(lesson, lessonPanelObject.transform);
            }
            ResetLoading(false); 
        }

        void DestroyAllLessons()
        {
            foreach (Transform child in lessonPanelObject.transform) 
            {
                if (child.gameObject.tag != TagConfig.loadingItemTag)
                {
                    GameObject.Destroy(child.gameObject);
                }
            }
        }

        void InstantiateLesson(Lesson lesson, Transform parentTransform)
        {
            try
            {
                GameObject lessonObject = Instantiate(lessonObjectResource) as GameObject; 
                lessonObject.transform.SetParent(parentTransform, false);

                string imageURL = APIUrlConfig.DOMAIN_SERVER + lesson.lessonThumbnail;
                RawImage targetImage = lessonObject.transform.GetChild(0).GetChild(0).gameObject.GetComponent<RawImage>();
                StartCoroutine(UnityHttpClient.LoadRawImageAsync(imageURL, targetImage, (isSuccess) => {
                    if (isSuccess)
                    {
                        lessonObject.transform.GetChild(0).GetChild(1).gameObject.SetActive(false);
                    }
                }));

                lessonObject.name = lesson.lessonId.ToString(); 
                lessonObject.transform.GetChild(1).GetChild(0).gameObject.GetComponent<Text>().text = Helper.FormatString(lesson.lessonTitle, calculatedSize);
                lessonObject.transform.GetChild(0).gameObject.GetComponent<Button>().onClick.AddListener(() => InteractionUI.Instance.onClickItemLesson(lesson.lessonId));            
            }
            catch (Exception e)
            {
                Debug.Log($"error InstantiateLesson {e.Message}");
            }
        }
    }
}
