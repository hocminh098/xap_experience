using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement; 

public class InteractionUISignUp : MonoBehaviour
{
    public GameObject waitingScreen; 
    public GameObject switchToHomeNoSignIn;
    public GameObject switchToSignInBtn; 
    public GameObject panelConfirm;
    public GameObject btnExitPopUp; 
    public GameObject btnSave;
    public GameObject btnDiscard; 
    
    void Start()
    {
        InitUI(); 
        SetActions(); 
    }
    void InitUI()
    {
        waitingScreen.SetActive(false); 
    }   
    void SetActions()
    {
        waitingScreen.SetActive(false); 
        switchToHomeNoSignIn.GetComponent<Button>().onClick.AddListener(HanlderSwitchToHomeNoSignIn); 
        btnExitPopUp.GetComponent<Button>().onClick.AddListener(HandlerExitPopUpConfirm);
        switchToSignInBtn.GetComponent<Button>().onClick.AddListener(SwitchToSignIn); 
        btnSave.GetComponent<Button>().onClick.AddListener(SwitchToHomeNoSignIn); 
        btnDiscard.GetComponent<Button>().onClick.AddListener(DiscardButton); 
    }
    void HanlderSwitchToHomeNoSignIn()
    {
        panelConfirm.SetActive(true);
    }
    void HandlerExitPopUpConfirm()
    {
        panelConfirm.SetActive(false);
    }

    void SwitchToHomeNoSignIn()
    {
        // BackOrLeaveApp.Instance.BackToPreviousScene(SceneManager.GetActiveScene().name); 
        // SignUpManager.CallAPISignUp(email, fullName, password, passwordConfirmation);
    }
    void DiscardButton()
    {
        BackOrLeaveApp.Instance.BackToPreviousScene(SceneManager.GetActiveScene().name); 
    }
    void SwitchToSignIn()
    {
        SceneManager.LoadScene(SceneConfig.signIn); 
    }
}
