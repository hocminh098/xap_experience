using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClearInputField : MonoBehaviour
{
    public GameObject xButton;
    public InputField inputField;
    
    void Start()
    {
        xButton.SetActive(false);
        xButton.transform.GetComponent<Button>().onClick.AddListener(ClearInput);
        inputField.onValueChanged.AddListener(HandleInput);
    }

    void ClearInput()
    {
        inputField.SetTextWithoutNotify("");
        xButton.SetActive(false);
    }

    void HandleInput(string data)
    {
        if (data == "")
        {
            xButton.SetActive(false);
        }
        else
        {
            xButton.SetActive(true);
        }
    }
}
