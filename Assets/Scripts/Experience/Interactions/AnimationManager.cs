using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class AnimationManager : MonoBehaviour
{
    public Button btnShortListAnimation;
    public Button btnFullListAnimation;
    public GameObject listAnimationFull;
    public GameObject listAnimationShort;
    public Button btnControlFull;
    public Button btnControlShort;
    public Button btnNext;
    public Button btnPrev;
    public Button animationToggleBtn;
    public GameObject txtNameAnimation;
    public Transform animationListTransform;
    public GameObject sliderAnimationFull;
    public GameObject sliderAnimationShort;
    private AnimationState currentAnimationState;
    public static event Action<Text, AnimationState> onAnimationStateChanged;
    private static AnimationManager instance;
    Animation currentAnimation;
    List<AnimationState> animationStates = new List<AnimationState>();
    public static AnimationManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<AnimationManager>();
            }
            return instance;
        }
    }

    private bool isShowingAnimation;
    private bool IsPlayingAnimation { get; set; }
    public bool IsShowingAnimation
    {
        get
        {
            return isShowingAnimation;
        }
        set
        {
            isShowingAnimation = value;
            if (animationToggleBtn != null)
            {
                animationToggleBtn.GetComponent<Image>().sprite = isShowingAnimation ? Resources.Load<Sprite>(PathConfig.ANIMATION_CLICKED_IMAGE) : Resources.Load<Sprite>(PathConfig.ANIMATION_UNCLICK_IMAGE);
                listAnimationFull.SetActive(isShowingAnimation);
                if (!isShowingAnimation && currentAnimationState != null)
                {
                    StopAnimation(currentAnimationState);
                    UnSelectAllAnimatiton();
                    ResetAnimation();
                }
                if (listAnimationShort.activeSelf)
                {
                    listAnimationShort.SetActive(false);
                }
            }
        }
    }

    void OnEnable()
    {
        ObjectManager.onChangeCurrentObject += SetupAnimations;
    }

    void OnDisable()
    {
        ObjectManager.onChangeCurrentObject -= SetupAnimations;
    }

    // Start is called before the first frame update
    void Start()
    {
        InitUI();
        InitEvents();
    }

    void Update()
    {
        OnChangeTimeAnimtion();
        EnableControlSwitchAnimation();
    }

    private void InitUI()
    {
        isShowingAnimation = false;
    }

    private void InitEvents()
    {
        btnShortListAnimation.onClick.AddListener(ShortAnimationPanel);
        btnFullListAnimation.onClick.AddListener(FullAnimationPanel);
        btnControlFull.onClick.AddListener(ControlAnimationCurrent);
        btnControlShort.onClick.AddListener(ControlAnimationCurrent);
        btnNext.onClick.AddListener(delegate { HandleSwitchAnimation(true);});
        btnPrev.onClick.AddListener(delegate { HandleSwitchAnimation(false);});
    }

    private void ShortAnimationPanel()
    {
        listAnimationFull.SetActive(false);
        listAnimationShort.SetActive(true);
    }

    private void FullAnimationPanel()
    {
        listAnimationShort.SetActive(false);
        listAnimationFull.SetActive(true);
    }

    public void ControlAnimationCurrent()
    {
        IsPlayingAnimation = !IsPlayingAnimation;
        ControlAnimation(IsPlayingAnimation);
    }

    private void ControlAnimation(bool _IsPlayingAnimation)
    {
        IsPlayingAnimation = _IsPlayingAnimation;
        if (currentAnimation != null && currentAnimationState != null)
        {
            if (IsPlayingAnimation)
            {
                ResumeAnimation();
                btnControlFull.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathConfig.AUDIO_PAUSE_IMAGE);
                btnControlShort.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathConfig.AUDIO_PAUSE_IMAGE);
            }
            else
            {
                PauseAnimation();
                btnControlFull.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathConfig.AUDIO_PLAY_IMAGE);
                btnControlShort.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathConfig.AUDIO_PLAY_IMAGE);
            }
        }
    }

    public void HandleSwitchAnimation(bool isNextAnimation)
    {
        int indexAnimation = animationStates.IndexOf(currentAnimationState);
        indexAnimation = isNextAnimation ? indexAnimation + 1 : indexAnimation - 1;
        HandleAnimationStateChanged(animationListTransform.GetChild(indexAnimation).transform.GetChild(0).GetComponent<Text>(), animationStates[indexAnimation]);
    }

    public void EnableControlSwitchAnimation()
    {
        if (currentAnimation != null && currentAnimationState != null)
        {
            int indexAnimation = animationStates.IndexOf(currentAnimationState);
            btnPrev.interactable = (indexAnimation != 0);
            btnNext.interactable = (indexAnimation != animationStates.Count - 1);
        }
        else
        {
            btnPrev.interactable = false;
            btnNext.interactable = false;
            btnControlShort.interactable = false;
            btnControlFull.interactable = false;
        }
    }

    public void InitAnimationList()
    {
        animationStates.Clear();
        animationStates = Helper.GetAnimations(ObjectManager.Instance.CurrentObject);
        foreach (AnimationState animationState in animationStates)
        {
            GameObject animationItem = Instantiate(Resources.Load(PathConfig.ANIMATION_ITEM)) as GameObject;
            Text animationText = animationItem.transform.GetChild(0).GetComponent<Text>();
            animationText.text = animationState.name;
            animationItem.GetComponent<Button>().onClick.AddListener(delegate { OnChangeAnimation(animationText, animationState); });
            animationItem.transform.SetParent(animationListTransform, false);
        }
    }

    public void OnChangeAnimation(Text animationText, AnimationState newAnimationState)
    {
        onAnimationStateChanged(animationText, newAnimationState);
    }

    public void PlayAnimation(AnimationState currentAnimationState)
    {
        currentAnimation.Play(currentAnimationState.name);
        Time.timeScale = 1;
    }

    public void StopAnimation(AnimationState currentAnimationState)
    {
        currentAnimation.Stop(currentAnimationState.name);
        Time.timeScale = 1;
    }

    public void PauseAnimation()
    {
        Time.timeScale = 0;
    }

    public void ResumeAnimation()
    {
        Time.timeScale = 1;
    }

    public void HandleAnimationStateChanged(Text newAnimationText, AnimationState newAnimationState)
    {
        UnSelectAllAnimatiton();
        currentAnimation = ObjectManager.Instance.CurrentObject.GetComponent<Animation>();
        currentAnimation.enabled = true;
        if (currentAnimation != null)
        {
            if (currentAnimationState != newAnimationState)
            {
                PlayAnimation(newAnimationState);
                newAnimationText.fontStyle = FontStyle.Bold;
                currentAnimationState = newAnimationState;
                currentAnimationState.wrapMode = WrapMode.Loop;

                sliderAnimationFull.GetComponent<Slider>().maxValue = (float)currentAnimationState.length;
                sliderAnimationShort.GetComponent<Slider>().maxValue = (float)currentAnimationState.length;
                txtNameAnimation.GetComponent<TMPro.TextMeshProUGUI>().text = currentAnimationState.name;

                btnControlFull.interactable = true;
                btnControlShort.interactable = true;

                btnControlFull.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathConfig.AUDIO_PAUSE_IMAGE);
                btnControlShort.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathConfig.AUDIO_PAUSE_IMAGE);
                IsPlayingAnimation = true;
            }
            else
            {
                StopAnimation(newAnimationState);
                ResetAnimation();
            }
        }
    }

    void OnChangeTimeAnimtion()
    {
        if (currentAnimation != null && currentAnimationState != null)
        {
            if ((float)currentAnimationState.time > (float)currentAnimationState.length)
            {
                sliderAnimationFull.GetComponent<Slider>().value = 0f;
                sliderAnimationShort.GetComponent<Slider>().value = 0f;
                currentAnimationState.time = 0f;
            }
            else
            {
                sliderAnimationFull.GetComponent<Slider>().value = (float)currentAnimationState.time;
                sliderAnimationShort.GetComponent<Slider>().value = (float)currentAnimationState.time;
            }
        }
    }

    public void UnSelectAllAnimatiton()
    {
        foreach(Transform child in animationListTransform)
        {
            child.GetChild(0).GetComponent<Text>().fontStyle = FontStyle.Normal;
        }
    }
    public void SetupAnimations(string currentObjectName)
    {
        Animation currentAnimation = ObjectManager.Instance.CurrentObject.GetComponent<Animation>();
        if (currentAnimation != null)
        {
            currentAnimation.enabled = false;
            DestroyAllAnimation();
            InitAnimationList();
        }
    }
    public void DestroyAllAnimation()
    {
        foreach (Transform child in animationListTransform.transform) 
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    public void ResetAnimation()
    {
        sliderAnimationFull.GetComponent<Slider>().value = 0f;
        sliderAnimationShort.GetComponent<Slider>().value = 0f;
        currentAnimationState = null;
        btnControlFull.interactable = false;
        btnControlShort.interactable = false;

        btnControlFull.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathConfig.AUDIO_PLAY_IMAGE);
        btnControlShort.GetComponent<Image>().sprite = Resources.Load<Sprite>(PathConfig.AUDIO_PLAY_IMAGE);
        IsPlayingAnimation = false;

    }
}
