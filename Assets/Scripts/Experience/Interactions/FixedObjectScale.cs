using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedObjectScale : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        FixedScale();
    }

    void FixedScale()
    {
        if (ObjectManager.Instance.OriginObject != null)
        {
            transform.localScale = ObjectManager.Instance.OriginScaleLabel / ((ObjectManager.Instance.OriginObject.transform.localScale.x / ObjectManager.Instance.OriginScale.x) * transform.parent.localScale.x);
        }
    }
}
