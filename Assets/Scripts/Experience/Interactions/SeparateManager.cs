using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SeparateManager : MonoBehaviour
{
    private static SeparateManager instance;
    public static SeparateManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<SeparateManager>();
            return instance;
        }
    }

    private float r = 0.18f;
    // constantly
    private const float RADIUS = 8f;
    private const float DISTANCE_FACTOR = 1.5f;

    // variable
    private int childCount;
    private Vector3 centerPosition;
    private Vector3 centerPosCurrentObject;
    private Vector3 centerPosChildObject;

    private Vector3 targetPosition;
    private float angle;
    private float maxDegree = 360f;
    public Button btnSeparate;
    private bool isSeparating;
    private bool isDisplayLabel;
    public bool IsSeparating
    {
        get
        {
            return isSeparating;
        }
        set
        {
            isSeparating = value;
            btnSeparate.GetComponent<Image>().sprite = isSeparating ? Resources.Load<Sprite>(PathConfig.SEPARATE_CLICKED_IMAGE) : Resources.Load<Sprite>(PathConfig.SEPARATE_UNCLICK_IMAGE);
        }
    }

    public void HandleSeparate(bool isSeparating)
    {
        IsSeparating = isSeparating;
        if (IsSeparating)
        {
            btnSeparate.interactable = false;
            SeparateOrganModel();
            btnSeparate.interactable = true;
        }
        else
        {
            btnSeparate.interactable = false;
            BackToPositionOrgan();
            btnSeparate.interactable = true;
        }
    }

    // public void SeparateOrganModel()
    // {
    //     childCount = ObjectManager.Instance.CurrentObject.transform.childCount;
    //     centerPosCurrentObject = ObjectManager.Instance.CurrentObject.transform.localPosition;
    //     int i = 0;
    //     foreach (Transform childTransform in ObjectManager.Instance.CurrentObject.transform)
    //     {
    //         if (childTransform.tag != TagConfig.LABEL_TAG)
    //         {
    //             centerPosChildObject = Helper.CalculateBounds(childTransform.gameObject).center;
    //             targetPosition = ComputeTargetPosition(centerPosCurrentObject, centerPosChildObject);
    //             StartCoroutine(MoveObjectWithLocalPosition(childTransform.gameObject, targetPosition));
    //             i++;
    //         }
    //     }
    // }
    // public Vector3 ComputeTargetPosition(Vector3 center, Vector3 currentPosition)
    // {
    //     Vector3 dir = currentPosition - center;
    //     return dir.normalized * DISTANCE_FACTOR / ObjectManager.Instance.FactorScaleInitial;
    // }

    GameObject InstantiateItem(int n, int i)
    {
        float angle = (360 / n ) * i * Mathf.Deg2Rad;
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.position = new Vector3(r  * Mathf.Sin(angle), r * Mathf.Cos(angle), 0);
        return cube;
    }

    void PreventOverlap(GameObject obj, Bounds currentObjectBounds)
    {
        Bounds boundObject = Helper.CalculateBounds(obj);
        float factorX = (boundObject.center.x < currentObjectBounds.center.x) ? -1 : 1;
        float factorY = (boundObject.center.y < currentObjectBounds.center.y) ? -1 : 1;
        if (isEqualToZero(boundObject.center.x - currentObjectBounds.center.x))
        {
            obj.transform.localPosition += new Vector3(0, factorY * boundObject.extents.y, 0) / ObjectManager.Instance.FactorScaleInitial;
        }
        else if (isEqualToZero(boundObject.center.y - currentObjectBounds.center.y))
        {
            obj.transform.localPosition += new Vector3(factorX * boundObject.extents.x, 0, 0) / ObjectManager.Instance.FactorScaleInitial;
        }
        else
        {
            obj.transform.localPosition += new Vector3(factorX * boundObject.extents.x, factorY * boundObject.extents.y, 0) / ObjectManager.Instance.FactorScaleInitial;
        }
    }

    bool isEqualToZero(float x)
    {
        return Mathf.Abs(x) < Helper.MIN;
    }

    public void SeparateOrganModel()
    {
        // if (LabelManager.Instance.IsShowingLabel)
        // {
        //     LabelManager.Instance.IsShowingLabel = !LabelManager.Instance.IsShowingLabel;
        //     LabelManager.Instance.HandleLabelView(LabelManager.Instance.IsShowingLabel);
        //     isDisplayLabel = true;
        // }
        // else
        // {
        //     isDisplayLabel = false;
        // }
        int childCount = ObjectManager.Instance.CurrentObject.transform.childCount;
        if (childCount < 1)
        {
            return;
        }
        BackToPositionOrgan();

        // reset origin object
        
        ObjectManager.Instance.ResetStateOfOriginObject();
        
        int i = 0;
        float angle;
        Bounds currentObjectBounds = Helper.CalculateBounds(ObjectManager.Instance.CurrentObject);
        Bounds objectBound;

        //  GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        // sphere.transform.position = currentObjectBounds.center;
        // sphere.GetComponent<SphereCollider>().radius = r;
        // sphere.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);


        GameObject childObject;
        Dictionary<int, Bounds> listOfSeparatingPositionOrders = Helper.GetListOfSeparatingPositionOrders(ObjectManager.Instance.CurrentObject, currentObjectBounds);
        foreach (KeyValuePair<int, Bounds> item in listOfSeparatingPositionOrders)
        {
            angle = (maxDegree / childCount) * i * Mathf.Deg2Rad;
            objectBound = item.Value;
            childObject = ObjectManager.Instance.CurrentObject.transform.GetChild(item.Key).gameObject;
            childObject.transform.localPosition += (currentObjectBounds.center + new Vector3(-r  * Mathf.Cos(angle), r * Mathf.Sin(angle), 0) - objectBound.center) / ObjectManager.Instance.FactorScaleInitial;
            // TestCircle(currentObjectBounds.center + new Vector3(-r  * Mathf.Cos(angle), r * Mathf.Sin(angle), 0));
            // prevent overlap
            PreventOverlap(childObject, currentObjectBounds);
            i++;
        }
        
        // if (isDisplayLabel)
        // {
        //     LabelManager.Instance.IsShowingLabel = !LabelManager.Instance.IsShowingLabel;
        //     LabelManager.Instance.HandleLabelView(LabelManager.Instance.IsShowingLabel);
        // }
        
    }

    void TestCircle(Vector3 position)
    {
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.position = position;
        cube.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
    }

    // public void SeparateOrganModel()
    // {
    //     childCount = ObjectManager.Instance.CurrentObject.transform.childCount;
    //     if (childCount <= 0) return;
    //     // centerPosition = CalculateCentroid();
    //     centerPosition = ObjectManager.Instance.CenterPivot;
    //     int i = 0;
    //     foreach (Transform child in ObjectManager.Instance.CurrentObject.transform)
    //     {
    //         if (child.gameObject.tag != TagConfig.LABEL_TAG)
    //         {
    //             // targetPosition = ComputeTargetPosition(centerPosition, ObjectManager.Instance.ListchildrenOfOriginPosition[i]);
    //             targetPosition = ComputeTargetPosition(centerPosition, ObjectManager.Instance.ListchildrenOfPivotPosition[i]);
    //             StartCoroutine(MoveObjectWithLocalPosition(child.gameObject, targetPosition));
    //             i++;
    //         }
    //     }
    // }

    private Vector3 CalculateCentroid()
    {
        Vector3 centroid = new Vector3(0, 0, 0);

        foreach (Vector3 localPosition in ObjectManager.Instance.ListchildrenOfOriginPosition)
        {
            centroid += localPosition;
        }
        centroid /= ObjectManager.Instance.ListchildrenOfOriginPosition.Count;
        return centroid;
    }

    public Vector3 ComputeTargetPosition(Vector3 origin, Vector3 target)
    {
        Vector3 dir = target - origin;
        return dir.normalized * DISTANCE_FACTOR / ObjectManager.Instance.FactorScaleInitial;
    }

    public IEnumerator MoveObjectWithLocalPosition(GameObject moveObject, Vector3 targetPosition)
    {
        float timeSinceStarted = 0f;
        while (true)
        {
            timeSinceStarted += Time.deltaTime;
            moveObject.transform.localPosition = Vector3.Lerp(moveObject.transform.localPosition, targetPosition, timeSinceStarted);
            if (moveObject.transform.localPosition == targetPosition)
            {
                yield break;
            }
            yield return null;
        }
    }
    public void BackToPositionOrgan()
    {
        // if (LabelManager.Instance.IsShowingLabel)
        // {
        //     LabelManager.Instance.IsShowingLabel = !LabelManager.Instance.IsShowingLabel;
        //     LabelManager.Instance.HandleLabelView(LabelManager.Instance.IsShowingLabel);
        //     isDisplayLabel = true;
        // }
        // else
        // {
        //     isDisplayLabel = false;
        // }


        int childCount = ObjectManager.Instance.ListchildrenOfOriginPosition.Count;
        if (childCount < 1)
        {
            return;
        }
        for (int i = 0; i < childCount; i++)
        {   
            {
                targetPosition = ObjectManager.Instance.ListchildrenOfOriginPosition[i];
                // StartCoroutine(MoveObjectWithLocalPosition(ObjectManager.Instance.CurrentObject.transform.GetChild(i).gameObject, targetPosition));
                ObjectManager.Instance.CurrentObject.transform.GetChild(i).gameObject.transform.localPosition = targetPosition;
            }
        }

        // if (isDisplayLabel)
        // {
        //     LabelManager.Instance.IsShowingLabel = !LabelManager.Instance.IsShowingLabel;
        //     LabelManager.Instance.HandleLabelView(LabelManager.Instance.IsShowingLabel);
        // }
    }
}