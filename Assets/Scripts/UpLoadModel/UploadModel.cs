using System.Collections;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using TriLibCore;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
using System.Collections.Generic;
using System;
using EasyUI.Toast;
using System.Threading.Tasks;

public class UploadModel : MonoBehaviour
{
    public Image imgLoadingFill;
    public GameObject uiBFill;
    public Text txtPercent;
    public Button btnUploadModel3D;
    public Button btnBack;
    public GameObject uiCoat;
    public Text warningFileFormat;
    public Text warningFileSize;
    public static int idModel;
    private bool isCallAPI = false;
    private string[] arrFormatFile = {"FBX", "OBJ", "GLB", "ZIP"};  
    private string contentType;
    public GameObject waitingScreen;

    private static UploadModel instance;
    public static UploadModel Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<UploadModel>();
            }
            return instance; 
        }
    }

    private void Start()
    {
        
        Screen.orientation = ScreenOrientation.Portrait; 
        StatusBarManager.statusBarState = StatusBarManager.States.TranslucentOverContent;
        StatusBarManager.navigationBarState = StatusBarManager.States.Hidden;
        waitingScreen.SetActive(false);
        SetEventUI();
    }

    private void Update()
    {
        if(isCallAPI == true && UnityHttpClient.processAPI*2f*100f < 100)
        {
            imgLoadingFill.fillAmount = UnityHttpClient.processAPI * 2f;    
            txtPercent.text=$"{(imgLoadingFill.fillAmount*100f):N0} %";
        }
    }

    public void SetEventUI()
    {
        btnUploadModel3D.onClick.AddListener(Test);
        btnBack.onClick.AddListener(() => BackOrLeaveApp.Instance.BackToPreviousScene(SceneManager.GetActiveScene().name));
    }

    public string getModelName(string path)
    {
        string fullModelName = path.Substring(path.LastIndexOf("/") + 1);
        string formatFile = fullModelName.Substring(fullModelName.LastIndexOf(".") + 1);
        string[] modelName = fullModelName.Split('.');
        return modelName[0];
    }
    public void Test()
    {
        btnUploadModel3D.interactable = false;
        StartCoroutine(HandlerUploadModel());
    }

    public IEnumerator HandlerUploadModel()
    {
        yield return new WaitForSeconds(0.01f);
        DestroyModel();
        imgLoadingFill.fillAmount = 0f;

        var assetLoaderOptions = AssetLoader.CreateDefaultLoaderOptions();
        AssetLoaderFilePicker.Create()
            .LoadModelFromFilePickerAsync("load model", 
                x =>
                {
                    string path = ModelManager.modelFilepath;
                    var fileInfo = new System.IO.FileInfo(path);
                    var lengthFile = fileInfo.Length/1000000;
                    var modelName = getModelName(path);
                    Debug.Log("lengthFile: " + lengthFile);
                    string formatFile = ModelManager.modelExtension.ToUpper();
                    if (Array.IndexOf(arrFormatFile, formatFile) < 0) 
                    {
                        ReStore();
                        warningFileSize.enabled = false;     
                        warningFileFormat.enabled = true;     
                    }
                    else 
                    {
                        if(lengthFile > 100)    
                        {
                            ReStore();
                            warningFileSize.enabled = true; 
                            warningFileFormat.enabled = false;    
                        }
                        else
                        {
                            x.RootGameObject.tag = "ModelClone";
                            x.RootGameObject.name = modelName;
                            warningFileFormat.enabled = false;
                            warningFileSize.enabled = false;   
                            UploadModelToServer(File.ReadAllBytes(path), path);
                            DontDestroyOnLoad(x.RootGameObject);   
                        }
                    }   
                },
                x => { 

                },
                (x, y) => { 
                   
                },
                x => { 
                        btnUploadModel3D.interactable = true;
                        if(x == true)
                        {
                            uiCoat.SetActive(true);
                            uiBFill.SetActive(true);
                            // txtPercent.text="0%";
                        }
                        else 
                        {
                            // txtPercent.text="";
                            ReStore();
                        }
                    },
                x => { 
                    btnUploadModel3D.interactable = true;
                    ReStore();
                    warningFileFormat.enabled = true;
                    warningFileSize.enabled = false;  
                },
                null,
                assetLoaderOptions);
    }
    

    public async Task UploadModelToServer(byte[] fileData, string fileName)
    {
        try 
        {
            Debug.Log("UploadModelToServer");
            isCallAPI = true;
            var form = new WWWForm();
            form.AddBinaryData("model", fileData, fileName);

            APIResponse<ResData[]> importModelResponse = await UnityHttpClient.UploadFileAPI<ResData[]>(APIUrlConfig.Upload3DModel, UnityWebRequest.kHttpVerbPOST, form);
            isCallAPI = false;
            if (importModelResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
            {
                idModel = importModelResponse.data[0].file_id;
                ModelStoreManager.InitModelStore(idModel, importModelResponse.data[0].file_path);
                Debug.Log("iModel" + idModel);
                ReStore();
                Exception exception = Network.CheckNetWorkToDisplayToast();
                if (exception != null) throw exception;
                BackOrLeaveApp.Instance.AddPreviousScene(SceneManager.GetActiveScene().name, SceneConfig.interactiveModel);
                await Task.Delay(1000);
                SceneManager.LoadScene(SceneConfig.interactiveModel);
            }
            else
            {
                txtPercent.text="";
                throw new Exception(importModelResponse.message);
            }
        }
        catch (Exception exception)
        {
            ReStore();
            txtPercent.text="";
            Toast.ShowCommonToast(exception.Message, APIUrlConfig.BAD_REQUEST_RESPONSE_CODE); 
        }
    }

    private void DestroyModel()
    {
        GameObject modelClone = GameObject.FindWithTag("ModelClone");
        if (modelClone != null)
        {
            Destroy(modelClone);
        }
    }

    public void ReStore() 
    {
        uiCoat.SetActive(false);
        uiBFill.SetActive(false);
        // imgLoadingFill.fillAmount = 0; 
        txtPercent.text = "";
    }
}

[System.Serializable]
class ResData 
{
    public int type;
    public string extention;
    public double size;
    public string file_name;
    public string file_path;
    public int created_by;
    public string created_date;
    public int file_id;
}

