using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using BuildLesson;
using EasyUI.Toast;
using System.Threading.Tasks;
using AdvancedInputFieldPlugin;
using System;

public class InteractiveModel : MonoBehaviour
{
    public Button btnCreateThumbnail;
    public Button btnCompleteConversion;
    public GameObject btnSave;
    public GameObject panelConfirm;
    public Image imgScreenShot;
    public Camera camera;
    public Transform parent2;
	public Vector3 offset;
    public AdvancedInputField iModelName;
    public GameObject uiCoat;
    public GameObject NameModelWarning;
    public GameObject PictureWarning;
    public GameObject waitingScreen;
    private static Canvas canvas;
    private int idModel;
    public static string modelName;
    private byte[] thumbnail = null;
    public int resWidth = 350; 
    public int resHeight = 350;
    private bool isValidIModelName = true;
 
    public void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait; 
        StatusBarManager.statusBarState = StatusBarManager.States.TranslucentOverContent;
        StatusBarManager.navigationBarState = StatusBarManager.States.Hidden;

        GameObject modelClone = GameObject.FindWithTag("ModelClone");
        modelClone.transform.position = new Vector3(0, 1.4f, 0);
        modelClone.transform.Rotate(-3f, 0, 0, Space.World);

        NameModelWarning.SetActive(false);
        PictureWarning.SetActive(false);
        if (modelClone != null) 
        {
            iModelName.Text = modelClone.name;
            ObjectModel.Instance.InitGameObject(modelClone);
        }
        idModel = UploadModel.idModel;

        InitEvents();   
    }

    void Update()
    {
        TouchModel.Instance.HandleTouchInteraction();
        if (iModelName.Selected == true && iModelName.Text != "" && isValidIModelName == false)
        {
            iModelName.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldWarning);
        }
        else if (iModelName.Selected == true && iModelName.Text == "" && isValidIModelName == true)
        {
            iModelName.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldBorder);
        }
        else if (iModelName.Selected == false && iModelName.Text != "" && isValidIModelName == true)
        {
            iModelName.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldNormal);
        }
    }

    void OnValidate()
    {
        btnCreateThumbnail = GameObject.Find("BtnCreateThumbnail").GetComponent<Button>();
        imgScreenShot = GameObject.Find("ImgScreenShot").GetComponent<Image>();
        btnCompleteConversion = GameObject.Find("BtnComplete").GetComponent<Button>();
    }

    void InitEvents()
    {
        btnCreateThumbnail.onClick.AddListener(HandleCreateThumbnail);
        btnCompleteConversion.onClick.AddListener(HandleCompleteConversion);
        iModelName.OnValueChanged.AddListener(checkNameModelValid);
        btnSave.GetComponent<Button>().onClick.AddListener(HandleCompleteConversion);
    }

    private void checkNameModelValid(string data)
    {
        if(data != "")
        {
            ChangeUIStatus(iModelName, NameModelWarning, false);
        }
    }

    private void ChangeUIStatus(AdvancedInputField input, GameObject warning, bool status)
    {
        warning.SetActive(status);
        if(status)
        {
            input.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldWarning);
        }
        else
        {
            input.GetComponent<Image>().sprite = Resources.Load<Sprite>(SpriteConfig.imageInputFieldBorder);
        }
    }

    private void HandleCreateThumbnail()
    {
        LateUpdates();
    }

    void LateUpdates() 
    {
        ObjectModel.Instance.OriginObject.transform.position = new Vector3(0, 0, 0);
        Vector3 oldPositonCam = Camera.main.transform.localPosition;
        Camera.main.transform.localPosition = new Vector3(oldPositonCam.x, -1.65f, -3.5f);

        RenderTexture rt = new RenderTexture(resWidth, resHeight, 32);
        Camera.main.targetTexture = rt;
        Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.ARGB32, false);
        Camera.main.Render();
        RenderTexture.active = rt;
        screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
        Camera.main.targetTexture = null;
        RenderTexture.active = null; // JC: added to avoid errors
        screenShot.Apply();
        imgScreenShot.sprite = Sprite.Create(screenShot, new Rect(0, 0, resWidth, resHeight), new Vector2(0, 0));

        ObjectModel.Instance.OriginObject.transform.position = new Vector3(0, 1.4f, 0);
        Camera.main.transform.localPosition = oldPositonCam;
        thumbnail = screenShot.EncodeToPNG();
        Destroy(rt);
    }

    public void HandleCompleteConversion()
    {
        HandleCompleteConversionAPI();
        panelConfirm.SetActive(false);
    }

    public async Task HandleCompleteConversionAPI()
    {
        bool check = true;
        string modelName = iModelName.Text;
        Debug.Log(modelName);
        if(thumbnail == null)
        {
            check = false;
            PictureWarning.SetActive(true);
            uiCoat.SetActive(false);
        }
        else 
        {
            PictureWarning.SetActive(false);
        }

        if(modelName == "")
        {
            check = false;
            uiCoat.SetActive(false);
            isValidIModelName = false;
            ChangeUIStatus(iModelName, NameModelWarning, true);
        }

        if(check == false) 
        {
            panelConfirm.SetActive(false);
        }
        else 
        {
            uiCoat.SetActive(true);
            waitingScreen.SetActive(true);
            try 
            {
                var form = new WWWForm();
                form.AddField("modelFileId", idModel);
                form.AddField("modelName", iModelName.Text.ToString());
                form.AddBinaryData("thumbnail", thumbnail,$"{modelName}.jpg","image/jpg");

                APIResponse<ResDataImportModel[]> interactiveModelResponse = await UnityHttpClient.UploadFileAPI<ResDataImportModel[]>(APIUrlConfig.Import3DModel, UnityWebRequest.kHttpVerbPOST, form);
                if (interactiveModelResponse.code == APIUrlConfig.SUCCESS_RESPONSE_CODE)
                {
                    GameObject modelOrgan = GameObject.FindWithTag("Organ");
                    if (modelOrgan != null)
                    {
                        Destroy(modelOrgan);
                    }
                    // BackOrLeaveApp.Instance.AddPreviousScene(SceneManager.GetActiveScene().name, SceneConfig.createLesson);
                    ModelStoreManager.InitModelStore(interactiveModelResponse.data[0].modelId, interactiveModelResponse.data[0].modelName);
                    // ScenePrevious.scenePrevious = SceneConfig.interactiveModel; 
                    await Task.Delay(1000);    
                    SceneManager.LoadScene(SceneConfig.createLesson); 
                }
                else
                {
                    throw new Exception(interactiveModelResponse.message);
                }
            }
            catch (Exception exception)
            {
                ReStore();
                Toast.ShowCommonToast(exception.Message, APIUrlConfig.BAD_REQUEST_RESPONSE_CODE); 
            }
        }        
    }

    IEnumerator waiter()
    {    
        yield return new WaitForSeconds(1);  
    }

    private void ReStore() 
    {
        uiCoat.SetActive(false);
        waitingScreen.SetActive(false);
        GameObject modelOrgan = GameObject.FindWithTag("Organ");
        if (modelOrgan != null)
        {
            Destroy(modelOrgan);
        }
    }
}

[System.Serializable]
class ResDataImportModel 
{
    public string modelName;
    public string modelFileId;
    public int thumnailFileId;
    public int createBy;
    public string createDate;
    public int modelId;
}