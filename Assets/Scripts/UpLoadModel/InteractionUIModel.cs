using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement; 

public class InteractionUIModel : MonoBehaviour
{
    public GameObject btnBack;
    public GameObject panelConfirm;
    public GameObject btnExitPopUp; 
    public GameObject btnDiscard; 

    void Start()
    {
        SetActions(); 
    }   
    void SetActions()
    {
        btnBack.GetComponent<Button>().onClick.AddListener(HandlerBack); 
        btnExitPopUp.GetComponent<Button>().onClick.AddListener(HandlerExitPopUpConfirm);
        btnDiscard.GetComponent<Button>().onClick.AddListener(DiscardlButton); 
    }
    void HandlerBack()
    {
        panelConfirm.SetActive(true);
    }
    void HandlerExitPopUpConfirm()
    {
        panelConfirm.SetActive(false);
    }
    void DiscardlButton()
    {
        BackOrLeaveApp.Instance.BackToPreviousScene(SceneManager.GetActiveScene().name);
        GameObject modelClone = GameObject.FindWithTag("Organ");
        Destroy(modelClone);
    }
}
